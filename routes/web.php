<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\SocialiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->away(config('app.front_app_url'));
});

Route::get('/auth/password/reset/{token}', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');

Route::group(['prefix' => 'auth/login'], function () {
    Route::get('{provider}', [SocialiteController::class, 'redirectToProvider']);
    Route::get('{provider}/callback', [SocialiteController::class, 'handleProviderCallback']);
});
