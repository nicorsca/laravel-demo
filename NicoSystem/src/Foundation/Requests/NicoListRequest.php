<?php


namespace NicoSystem\Foundation\Requests;


use NicoSystem\Requests\NicoRequest;

class NicoListRequest extends NicoRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'page' => 'nullable|integer|min:1|max:99999999',
            'sort_by' => 'nullable|string|max:255',
            'sort_order' => 'nullable|string|in:asc,desc',
            'per_page' => 'nullable|integer|min:4|max:500',
            'all' => 'nullable|in:1,0',
            'keyword' => 'nullable|string',
        ];
    }
}
