<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 12/31/2016
 * Time: 12:23 AM
 */

namespace NicoSystem\Foundation;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Trait NicoResponseTraits
 * @package NicoSystem\Foundation
 */
trait NicoResponseTraits
{
    protected bool $api = true;

    /**
     * @param $body
     * @param string $codeText
     * @param string $messageCode
     * @param array $headers
     * @return JsonResponse
     */
    public function responseOk($body, string $codeText = 'ok', string $messageCode = 'ok', array $headers = []): JsonResponse
    {
        return $this->nicoResponse($body, Response::HTTP_OK, $codeText, $messageCode, $headers);
    }

    /**
     * @param $body
     * @param int $status
     * @param string $codeText
     * @param string $messageCode
     * @param array $headers
     * @return JsonResponse
     */
    public function nicoResponse($body, int $status = Response::HTTP_OK, string $codeText = 'OK', string $messageCode = 'ok', array $headers = []): JsonResponse
    {
        if ($body instanceof Collection) {
            $body = new Paginator($body->all(), $body->count());
        }
        $status = $this->validateStatusCode($status);

        return response()->json($body, $status)->withHeaders($headers);
    }

    /**
     * @param int $status
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseError(int $status = Response::HTTP_INTERNAL_SERVER_ERROR, string $codeText = 'server error', string $code = 'server_error', array $headers = []): JsonResponse
    {
        $status = $this->validateStatusCode($status);

        return response()->json([
            "message" => trans("appconstant." . $code),
            'code' => $code,
            'code_text' => $codeText,
        ], $status)->withHeaders($headers);
    }

    /**
     * @param int $code
     * @return int
     */
    public function validateStatusCode(int $code = 0): int
    {
        $status_code = array(100, 101, 102, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226,
            300, 301, 302, 303, 304, 305, 306, 307, 308
        , 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 425, 426, 428, 429, 431, 451
        , 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511);
        if (in_array($code, $status_code)) {
            return $code;
        } else {
            return 500;
        }
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseServerError(string $codeText = 'internal server error occurred', string $code = 'internal_server_error', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_INTERNAL_SERVER_ERROR, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseUnAuthorize(string $codeText = 'unauthorized', string $code = 'unauthorized', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_UNAUTHORIZED, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseForbidden(string $codeText = 'forbidden', string $code = 'forbidden', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_FORBIDDEN, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseNotFound(string $codeText = 'not found', string $code = 'not_found', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_NOT_FOUND, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseBadRequest(string $codeText = 'bad request', string $code = 'bad_request', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_BAD_REQUEST, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responsePreConditionFailed(string $codeText = 'precondition failed', string $code = 'precondition_failed', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_PRECONDITION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseConflict( string $codeText = 'conflict', string $code = 'conflict', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_CONFLICT, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseExpectationFailed(string $codeText = 'expectation failed', string $code = 'expectation_failed', array $headers = []): JsonResponse
    {
        return $this->responseError(Response::HTTP_EXPECTATION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param null $body
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseValidationError($body = null, string $codeText = 'form validation failed', string $code = 'form_validation_error', array $headers = []): JsonResponse
    {
        return $this->nicoResponse($body, Response::HTTP_EXPECTATION_FAILED, $codeText, $code, $headers);
    }

    /**
     * @param string $codeText
     * @param string $code
     * @param array $headers
     * @return JsonResponse
     */
    public function responseTooManyAttempts(string $codeText = 'too many request', string $code = 'too_many_request', array $headers = []): JsonResponse
    {
        return $this->responseError( Response::HTTP_TOO_MANY_REQUESTS, $codeText, $code, $headers);
    }

}
