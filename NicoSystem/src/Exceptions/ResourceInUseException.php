<?php
/**
 * Created by PhpStorm.
 * User: amar
 * Date: 10/29/18
 * Time: 2:01 PM
 */

namespace NicoSystem\Exceptions;

/**
 * Class ResourceInUseException
 * @package NicoSystem\Exceptions
 */
class ResourceInUseException extends NicoException
{
    protected $code = 428;

    protected string $respCode = "err_resource_in_use";

    protected $message = "Resource in use";
}
