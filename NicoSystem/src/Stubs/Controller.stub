<?php

namespace App\{moduleContainer}\{{module}}\Controllers;

use App\{moduleContainer}\{{module}}\Interfaces\{module}Interface;
use App\{moduleContainer}\{{module}}\Requests\{module}CreateRequest;
use App\{moduleContainer}\{{module}}\Requests\{module}UpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class {{module}}Controller
 * @package App\{moduleContainer}\{{module}}\Controllers
 */
class {{module}}Controller extends BaseController
{
    /**
     * {{module}}Controller constructor.
     * @param {module}Interface $repository
     */
    public function __construct(private {module}Interface $repository)
    {
    }

    /**
     * @param NicoRequest $request
     * @return JsonResponse
     */
    public function index(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param {module}CreateRequest $request
     * @return JsonResponse
     */
    public function store({module}CreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->all()));
    }

    /**
     * @param {module}UpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update({module}UpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id,$request->all()));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show (NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->toggleStatus($id));
    }
}
