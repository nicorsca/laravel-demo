<?php

namespace Database\Factories;

use App\System\Employee\Database\Models\AllocatedLeave;
use App\System\Setting\Database\Models\Setting;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class AllocatedLeaveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AllocatedLeave::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $type = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        return [
            'title' => $this->faker->text(50),
            'days' => $this->faker->randomDigitNotZero(),
            'start_date' => Carbon::now()->format('Y-m-d'),
            'end_date' => Carbon::now()->addYear()->format('Y-m-d'),
            'type' => $this->faker->randomElement($type->value),
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
