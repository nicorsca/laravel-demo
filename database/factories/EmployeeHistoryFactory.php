<?php

namespace Database\Factories;

use App\System\Employee\Database\Models\History;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeHistoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = History::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'date' => Carbon::now()->format('Y-m-d'),
            'status' => $this->faker->unique(true)->randomElement([0,1]),
            'comment' => $this->faker->sentence,
        ];
    }
}
