<?php

namespace Database\Factories;

use App\System\Employee\Database\Models\SocialSecurity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class SocialSecurityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SocialSecurity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->unique()->company,
            'number' => $this->faker->unique()->creditCardNumber,
            'start_date' => Carbon::yesterday(),
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
