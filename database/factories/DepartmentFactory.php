<?php

namespace Database\Factories;

use App\System\Department\Database\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class DepartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Department::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->unique()->jobTitle,
            'description' => $this->faker->text,
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
