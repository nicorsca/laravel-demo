<?php

namespace Database\Factories;

use App\System\Common\Database\Models\Document;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class DocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Document::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->title,
            'url' => $this->faker->url,
            'thumbnail' => $this->faker->url,
            'status' => Status::STATUS_PUBLISHED,
            'type' => 'image/png',
        ];
    }
}
