<?php

namespace Database\Factories;

use App\System\Project\Database\Models\ProjectRole;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectRoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectRole::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->unique()->jobTitle,
        ];
    }
}
