<?php

namespace Database\Factories;

use App\System\User\Database\Models\Provider;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProviderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Provider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'provider' => $this->faker->randomElement(['google', 'microsoft']),
            'provider_id' => $this->faker->unique()->randomNumber(),
            'avatar' => $this->faker->imageUrl(),
        ];
    }
}
