<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateVwAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('drop view if exists vw_attendances');

        if (config('database.default') == 'sqlite') {
            $attendDate = "strftime('%Y-%m-%d', datetime(attendances.attend_at / 1000, 'unixepoch'))";
            $checkIn = "MAX(CASE WHEN TYPE = 'check_in' THEN attend_at END)";
            $checkOut = "MAX(CASE WHEN TYPE = 'check_out' THEN attend_at END)";
            $leaveIn = "MAX(CASE WHEN TYPE = 'leave_in' THEN attend_at END)";
            $leaveOut = "MAX(CASE WHEN TYPE = 'leave_out' THEN attend_at END)";
        } else {
            $attendDate = "DATE(attendances.attend_at)";
            $checkIn = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'check_in' THEN attend_at END))";
            $checkOut = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'check_out' THEN attend_at END))";
            $leaveIn = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'leave_in' THEN attend_at END))";
            $leaveOut = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'leave_out' THEN attend_at END))";
        }
        $sumOfBreaks = "SUM(break_time)";
        $checkTimeDiff = "{$checkOut} - {$checkIn}";

        $break_hours = DB::table('vw_attendance_breaks')
            ->select('attend_date',
                DB::raw("${sumOfBreaks} as break_time"),
                'employee_id'
            )->groupBy('attend_date', 'employee_id');

        $sqlQuery = DB::table('attendances')
            ->select(
                'attendances.employee_id',
                DB::raw(
                    "{$attendDate} AS attend_date,
                        {$checkIn}           AS check_in,
                        {$checkOut}          AS check_out,
                        {$leaveIn}           AS leave_in,
                        {$leaveOut}         AS leave_out,
                        IFNULL({$checkTimeDiff}, 0) as office_time,
                        IFNULL({$checkTimeDiff}, 0) - b.break_time as work_time,
                        b.break_time,
                        IFNULL({$leaveOut} - {$leaveIn}, 0) as leave_time"
                ))
            ->join(DB::raw("({$break_hours->toSql()}) b"), function ($join) use ($attendDate) {
                $join->on('b.attend_date', '=', DB::raw($attendDate))
                    ->on('b.employee_id', '=', 'attendances.employee_id');
            })->whereNull('attendances.deleted_at')
            ->whereRaw('attendances.status = 2')
            ->groupBy('attendances.employee_id', DB::raw($attendDate));

        $sqlQuery = array_reduce($sqlQuery->getBindings(), function ($sql, $binding) {
            return preg_replace('/\?/', is_numeric($binding) ? $binding : "'" . $binding . "'", $sql, 1);
        }, $sqlQuery->toSql());

        DB::statement('Create view vw_attendances as ' . $sqlQuery);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists vw_attendances');
    }
}
