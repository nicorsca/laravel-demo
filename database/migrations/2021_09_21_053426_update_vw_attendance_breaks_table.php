<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateVwAttendanceBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('drop view if exists vw_attendance_breaks');

        if (config('database.default') == 'sqlite') {
            $breakOut = "MAX(CASE WHEN TYPE = 'break_out' THEN attend_at END)";
            $breakIn = "MAX(CASE WHEN TYPE = 'break_in' THEN attend_at END)";
            $attendDate = "strftime('%Y-%m-%d', datetime(attend_at / 1000, 'unixepoch'))";
        } else {
            $breakOut = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'break_out' THEN attend_at END))";
            $breakIn = "UNIX_TIMESTAMP(MAX(CASE WHEN TYPE = 'break_in' THEN attend_at END))";
            $attendDate = "DATE(attend_at)";
        }
        $sqlQuery = DB::table('attendances')
            ->select(
                DB::raw("{$attendDate} AS attend_date,
                {$breakOut} AS break_out,
                 {$breakIn} AS break_in,
                IFNULL({$breakIn} - {$breakOut}, 0) AS break_time
                "),
                'employee_id',
                'punch_count'
            )
            ->whereNull('deleted_at')
            ->whereRaw('status = 2')
            ->groupBy('employee_id', 'punch_count', DB::raw($attendDate));

        $sqlQuery = array_reduce($sqlQuery->getBindings(), function ($sql, $binding) {
            return preg_replace('/\?/', is_numeric($binding) ? $binding : "'" . $binding . "'", $sql, 1);
        }, $sqlQuery->toSql());

        DB::statement('Create view vw_attendance_breaks as ' . $sqlQuery);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists vw_attendance_breaks');
    }
}
