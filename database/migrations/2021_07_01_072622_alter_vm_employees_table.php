<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterVmEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('drop view if exists vw_employees');
        if (config('database.default') == 'sqlite') {
            $name = "users.first_name || ' ' ||users.middle_name || ' ' || users.last_name AS name";
        } else {
            $name = "CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) AS name";
        }
        $sqlQuery = DB::table('employees')
            ->join('users', 'employees.user_id', '=', 'users.id')
            ->join('departments', 'employees.department_id', '=', 'departments.id')
            ->select(DB::raw($name), 'users.first_name', 'users.last_name', 'users.middle_name', 'users.email', 'users.email_verified_at',
                'users.status', 'users.is_locked', 'users.avatar', 'employees.*', 'departments.title as department');

        $sqlQuery = array_reduce($sqlQuery->getBindings(), function ($sql, $binding) {
            return preg_replace('/\?/', is_numeric($binding) ? $binding : "'" . $binding . "'", $sql, 1);
        }, $sqlQuery->toSql());

        DB::statement('create view vw_employees as ' . $sqlQuery);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists vw_employees');
    }
}
