<?php

namespace Database\Seeders;

use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\System\Employee\Database\Models\Employee;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use App\System\Setting\Foundation\SettingDefaultValue;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LeaveSeeder extends Seeder
{
    use FiscalYearTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $employee = Employee::setActivityRecord(false)->factory()->forUser()->forDepartment()->create();
        $manager = Employee::setActivityRecord(false)->factory()->forUser()->forDepartment()->create();
        $leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first()?->value ?? SettingDefaultValue::LEAVES_TYPE;
        $fiscalDates = $this->getStartAndEndFiscalDate(true);
        AllocatedLeave::setActivityRecord(false)->factory()->create([
            'employee_id' => $employee->id,
            'start_date' => $fiscalDates['start_date'],
            'end_date' => $fiscalDates['end_date'],
            'type' => $leaveType[0],
            'days' => 30
        ]);

        $startAt = Carbon::parse($fiscalDates['start_date']);

        for($i = 1; $i<15; $i++) {
            Leave::setActivityRecord(false)->factory()->create([
                'requested_to' => $manager->id,
                'employee_id' => $employee->id,
                'type' => $leaveType[0],
                'start_at' => $startAt,
                'end_at' => $startAt->addDays(1)->subSecond(),
                'status' => LeaveStatus::PENDING,
                'days' => 1
            ]);
            $startAt = $startAt->addDays(2);
        }
    }
}
