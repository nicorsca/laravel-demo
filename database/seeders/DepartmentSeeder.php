<?php

namespace Database\Seeders;

use App\System\Department\Database\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Department::setActivityRecord(false)->factory()->count(30)->create();
    }
}
