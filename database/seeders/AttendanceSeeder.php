<?php

namespace Database\Seeders;

use App\System\Attendance\Database\Models\Attendance;
use App\System\Employee\Database\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run(): void
    {
        $employees = Employee::paginate(15);
        $employees->each(function ($employee){
            for ($days = 1; $days <= 30; $days++) {
                if (!in_array(Carbon::today()->subDays($days)->dayOfWeek, [0,6], false)) {
                    Attendance::setActivityRecord(false)->factory()->create([
                        'employee_id' => $employee->id,
                        'type'        => 'check_in',
                        'attend_at'   => Carbon::today()->subDays($days)->addHours(3)->addMinutes(random_int(1, 60)),
                    ]);
                    Attendance::setActivityRecord(false)->factory()->create([
                        'employee_id' => $employee->id,
                        'type'        => 'break_out',
                        'attend_at'   => Carbon::today()->subDays($days)->addHours(7)->addMinutes(random_int(1, 60)),
                    ]);
                    Attendance::setActivityRecord(false)->factory()->create([
                        'employee_id' => $employee->id,
                        'type'        => 'break_in',
                        'attend_at'   => Carbon::today()->subDays($days)->addHours(8)->addMinutes(random_int(1, 60)),
                    ]);
                    Attendance::setActivityRecord(false)->factory()->create([
                        'employee_id' => $employee->id,
                        'type'        => 'check_out',
                        'attend_at'   => Carbon::today()->subDays($days)->addHours(12)->addMinutes(random_int(1, 60)),
                    ]);
                }
            }
        });
    }
}
