<?php

namespace Database\Seeders;

use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $department = Department::setActivityRecord(false)->factory()->count(20)->create();
        $deptd = $department->pluck('id');

        for($i=0;$i<=1;$i++)
        {
            Employee::setActivityRecord(false)->factory([
                'department_id' => $deptd->random(),
            ])->forUser()->create();
        }
    }
}
