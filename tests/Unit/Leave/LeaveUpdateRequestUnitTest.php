<?php


namespace Leave;


use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Mails\LeaveRequestUpdateMail;
use App\System\Employee\Database\Models\Employee;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class LeaveUpdateRequestUnitTest extends TestCase
{

    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private string $employeeAccessToken;

    public function testEmployeeCanUpdateLeaveRequestForLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::tomorrow()->addDay()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[1],
            'days' => 5,
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $employeeAccessToken = 'Bearer ' . $employee->user->createToken('test')->accessToken;
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $employeeAccessToken]);
        $response->assertStatus(403);
        $response->assertJson([
            'code' => 'forbidden',
            'code_text' => trans('auth.forbidden')
        ]);
    }

    public function testEmployeeCannotUpdateLeaveRequestOfOtherEmployees(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['status'] = LeaveStatus::PENDING;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotUpdateLeaveRequestIfLeaveStatusIsNotPending(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::APPROVED,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(422);
        $response->assertJson([
            'code' => 'err_invalid_leave_status_exception',
            'code_text' => trans('responses.leave.status_not_pending'),
        ]);
    }

    public function testEmployeeCannotUpdateLeaveRequestForPaidLeaveIfAlreadyLeaveIsRequestedInThatDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 5
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(11)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCannotUpdateLeaveRequestForPaidLeaveIfAlreadyLeaveIsRequestedInThatPartialDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(11)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(6)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);

        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCannotUpdateLeaveRequestForPaidLeaveIfAllocatedLeaveIsAbsent(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(404);
        $response->assertJson([
            'code' => 'allocated_leave_not_found',
            'code_text' => trans('responses.leave.empty_allocated_leave'),
        ]);
    }

    public function testEmployeeCanUpdateLeaveRequestForPaidLeaveForLimitedAllocatedLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['status'] = LeaveStatus::PENDING;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotUpdateLeaveRequestForPaidLeaveIfLeavesAreRequestedMoreThanAllocatedLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(2)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 4
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->subMonth()->addDay()->toDateTimeString(),
            'end_at' => Carbon::now()->subMonth()->addDays(2)->subSecond()->toDateTimeString(),
            'days' => 2
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        //update 2 days -> 3 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(11)->subSecond()->format('Y-m-d H:i:s'),
        ]);

        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);

        $response->assertStatus(400);
        $response->assertJson([
            'code' => 'paid_leave_exceed_exception',
            'code_text' => trans('responses.leave.paid_leave_exceed'),
        ]);
    }

    public function testLeaveRequestShouldSplitIfUpdatingLeaveIsRequestedAtStartAndEndOfFiscalYear(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonths(5)->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(4)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->subDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //leave request for 2+3 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //allocate leave for next fiscal year
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->addMonths(7)->toDateString(),
            'end_date' => Carbon::now()->addMonths(12)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $data['status'] = LeaveStatus::PENDING;
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first();
        $fiscalDateStart = Carbon::now()->year . '-' . $fiscalDateStartSetting->value;
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        $endAt = $data['end_at'];
        $data['end_at'] = $fiscalDateEnd . ' 23:59:59';
        $this->assertDatabaseHas('leaves', $data);

        $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
        $data['start_at'] = $fiscalDateStart . ' 00:00:00';
        $data['end_at'] = $endAt;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testLeaveRequestShouldSplitIfLeaveIsAfterFiscalYearAndUpdatingLeaveRequestedAtStartAndEndOfFiscalYear(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonths(5)->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->addDays(1)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(3)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //leave request for 2+3 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //allocate leave for next fiscal year
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->addMonths(7)->toDateString(),
            'end_date' => Carbon::now()->addMonths(12)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $data['status'] = LeaveStatus::PENDING;
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first();
        $fiscalDateStart = Carbon::now()->year . '-' . $fiscalDateStartSetting->value;
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        $endAt = $data['end_at'];
        $data['end_at'] = $fiscalDateEnd . ' 23:59:59';
        $this->assertDatabaseHas('leaves', $data);

        $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
        $data['start_at'] = $fiscalDateStart . ' 00:00:00';
        $data['end_at'] = $endAt;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanUpdateLeaveRequestForUnpaidLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->subMonth()->addDay()->toDateTimeString(),
            'end_at' => Carbon::now()->subMonth()->addDays(2)->subSecond()->toDateTimeString(),
            'days' => 2
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);

        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanUpdateLeaveRequestForUnpaidLeaveForEndAndStartOfFiscalYear(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->employee->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(4)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->subDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //leave request for 2+3 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(2)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $data['status'] = LeaveStatus::PENDING;
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first();
        $fiscalDateStart = Carbon::now()->year . '-' . $fiscalDateStartSetting->value;
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        $endAt = $data['end_at'];
        $data['end_at'] = $fiscalDateEnd . ' 23:59:59';
        $this->assertDatabaseHas('leaves', $data);

        $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
        $data['start_at'] = $fiscalDateStart . ' 00:00:00';
        $data['end_at'] = $endAt;
        $data['days'] = 3;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotUpdateLeaveRequestForUnpaidLeaveIfLeaveIsAlreadyRequestInThatDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(12)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCanUpdateRequestCancellationLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCanUpdateRequestCancellationLeaveWithinLeaveRequestedDate(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(200);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['days'] = 1;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotUpdateRequestCancellationLeaveForExistingCancellationLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::APPROVED
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(409);
        $response->assertJson([
            'code' => 'leave_exists_exception',
            'code_text' => trans('responses.leave.requested_already'),
        ]);
    }

    public function testEmployeeCannotRequestIfCancellationLeaveIsNotInRangeOfLeaveTakenDay(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'status' => LeaveStatus::APPROVED,
            'start_at' => Carbon::tomorrow()->addDay()->format('Y-m-d H:i:s'),
            'end_at' => Carbon::tomorrow()->addDays(3)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::tomorrow()->addDay()->format('Y-m-d H:i:s'),
            'end_at' => Carbon::tomorrow()->addDays(4)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(404);
        $response->assertJson([
            'code' => 'not_found',
            'code_text' => 'not found',
        ]);
    }

    public function testCancellationLeaveShouldNotBeRequestedIfLeaveIsNotApprovedAndStatusIsUnpaidOrPaidLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::tomorrow()->addDays(11)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::tomorrow()->addDays(13)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(404);
        $response->assertJson([
            'code' => 'not_found',
            'code_text' => 'not found',
        ]);
    }

    public function testPaidLeaveCannotBeUpdatedToCancellationLeave(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE,
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $response->assertStatus(404);
        $response->assertJson([
            'code' => 'not_found',
            'code_text' => 'not found',
        ]);
    }

    public function testCancellationLeaveCanBeUpdatedToPaidLeaveInDifferentDateRange(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2 days
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::CANCELLATION_LEAVE
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(12)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $response = $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['status'] = LeaveStatus::PENDING;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testMailShouldBeSentDuringLeaveRequestUpdate(): void
    {
        Mail::fake();
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 2
        ]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::tomorrow()->addDay()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[1],
            'days' => 5,
        ]);
        //leave request for 2 days
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(8)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(10)->subSecond()->format('Y-m-d H:i:s'),
        ]);
        $this->json('PUT', 'api/leaves/' . $leave->id, $data, ['authorization' => $this->employeeAccessToken]);
        Mail::assertQueued(LeaveRequestUpdateMail::class);
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->employeeAccessToken = 'Bearer ' . $this->employee->user->createToken('test')->accessToken;;
        $this->manager = Employee::factory()->forUser()->forDepartment()->create();
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);
    }

}
