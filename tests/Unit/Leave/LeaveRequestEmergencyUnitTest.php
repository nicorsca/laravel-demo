<?php


namespace Leave;


use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Mails\LeaveRequestMail;
use App\System\Attendance\Database\Models\Attendance;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class LeaveRequestEmergencyUnitTest extends TestCase
{
    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private string $managerAccessToken;

    public function testEmergencyLeaveRequestCanBeDoneByManager(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        $response->assertStatus(200);
        $this->assertInstanceOf(Leave::class, $response->getOriginalContent());
        $data['status'] = LeaveStatus::APPROVED;
        $data['requested_to'] = $this->manager->id;
        $data['is_emergency'] = true;
        $this->assertDatabaseHas('leaves', $data);
    }

    public function testEmployeeCannotRequestForLeaveIfEmployeeIdAndRequestedIdIsSame(): void
    {
        $data = Leave::factory()->raw([
            'employee_id' => $this->manager->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        $response->assertStatus(400);
        $response->assertJson([
            'code' => 'err_cannot_leave_request_self',
            'code_text' => trans('responses.employee.cannot_request_self'),
        ]);
    }

    public function testEmployeeCannotRequestForLeaveForNonExistingEmployee(): void
    {
        $data = Leave::factory()->raw([
            'employee_id' => random_int(1000, 2000),
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        $response->assertStatus(417);
        $response->assertJson([
            'employee_id' => ['The selected employee id is invalid.']
        ]);
    }

    public function testEmergencyLeaveRequestCanBeDoneByManagerAndInsertInAttendance(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $response = $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(8)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);

        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addDays(9)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
    }

    public function testLeaveRequestShouldSplitIfLeaveIsRequestedAtStartAndEndOfFiscalYearAndInsertInAttendances(): void
    {
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonths(5)->toDateString(),
            'end_date' => Carbon::now()->addMonths(5)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2+2 days
        $data = Leave::factory()->raw([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addMonths(6)->subDays(2)->toDateString() . ' 00:00:00',
            'end_at' => Carbon::now()->addMonths(6)->addDays(1)->subSecond()->toDateString() . ' 23:59:59',
        ]);
        //allocate leave for next fiscal year
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->addMonths(7)->toDateString(),
            'end_date' => Carbon::now()->addMonths(12)->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        $response = $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        $data['status'] = LeaveStatus::APPROVED;
        $response->assertStatus(200);
        $response->assertJsonCount(2);
        $attendances = Attendance::all();
        $this->assertCount(8, $attendances);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->subDays(2)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->subDays(1)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->subDays(1)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);

        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
        $this->assertDatabaseHas('attendances', [
            'attend_at' => Carbon::today()->addMonths(6)->addDay()->subSecond()->format('Y-m-d H:i:s'),
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'employee_id' => $this->employee->id,
        ]);
    }

    public function testMailShouldNotBeSentDuringEmergencyLeave(): void
    {
        Mail::fake();
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 10
        ]);
        //leave request for 2 days
        $data = Leave::factory()->raw([
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0]
        ]);
        $this->json('POST', 'api/leaves/emergency', $data, ['authorization' => $this->managerAccessToken]);
        Mail::assertNotQueued(LeaveRequestMail::class);
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->manager = Employee::factory()->forUser()->forDepartment()->create();
        $this->managerAccessToken = 'Bearer ' . $this->manager->user->createToken('test')->accessToken;;
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();
        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);
    }

}
