<?php

namespace Holiday;

use App\Exceptions\ResourceExistsException;
use App\Modules\Holidays\Database\Models\Holiday;
use App\Modules\Holidays\Repositories\HolidayRepository;
use Carbon\Carbon;
use Tests\TestCase;

class HolidayCreateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Holidays\Repositories\HolidayRepository
     */
    private HolidayRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HolidayRepository(new Holiday());
    }

    public function testHolidayShouldCreate(): void
    {
        $data = Holiday::factory()->raw();

        $result = $this->repository->create($data);
        $this->assertInstanceOf(Holiday::class, $result);
        $this->assertDatabaseHas('holidays', $data);
    }

    public function testMultipleHolidayShouldCreate(): void
    {
        $data = [
            'holidays' => [
                Holiday::factory()->raw(['date' => Carbon::tomorrow()->toDateString()]),
                Holiday::factory()->raw(['date' => Carbon::tomorrow()->addMonth()->toDateString()]),
                Holiday::factory()->raw(['date' => Carbon::tomorrow()->addMonths(4)->toDateString()]),
            ]
        ];

        $result = $this->repository->createMultiple($data);
        $this->assertCount(3, $result);
        foreach ($data['holidays'] as $d) {
            $this->assertDatabaseHas('holidays', $d);
        }
    }

}
