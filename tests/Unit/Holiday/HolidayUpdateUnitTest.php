<?php

namespace Holiday;

use App\Modules\Holidays\Database\Models\Holiday;
use App\Modules\Holidays\Repositories\HolidayRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class HolidayUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Holidays\Repositories\HolidayRepository
     */
    private HolidayRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HolidayRepository(new Holiday());
    }

    public function testHolidayShouldUpdate(): void
    {
        $holiday = Holiday::factory()->create(['date' => Carbon::tomorrow()->toDateString()]);

        $data = Holiday::factory()->raw();
        $result = $this->repository->update($holiday->id, $data);
        $this->assertInstanceOf(Holiday::class, $result);
        $this->assertEquals($holiday->id, $result->id);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['date'], $result->date);
        $this->assertEquals($holiday['remarks'], $result->remarks);
    }

    public function testHolidayShouldNotUpdateForPassedHolidays(): void
    {
        $holiday = Holiday::factory()->create(['date' => Carbon::yesterday()->toDateString()]);
        $data = Holiday::factory()->raw();
        $this->expectException(NicoBadRequestException::class);
        $this->repository->update($holiday->id, $data);
    }

    public function testHolidayShouldNotUpdateForUnknownHoliday(): void
    {
        $data = Holiday::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), $data);
    }
}
