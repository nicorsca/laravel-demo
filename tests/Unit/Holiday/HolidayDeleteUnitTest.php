<?php

namespace Holiday;

use App\Modules\Holidays\Database\Models\Holiday;
use App\Modules\Holidays\Repositories\HolidayRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class HolidayDeleteUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Holidays\Repositories\HolidayRepository
     */
    private HolidayRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HolidayRepository(new Holiday());
    }

    public function testHolidayShouldDelete(): void
    {
        $holiday = Holiday::factory()->create(['date' => Carbon::tomorrow()->toDateString()]);

        $result = $this->repository->destroy($holiday->id);
        $this->assertEquals(true, $result);
        $this->assertNotNull($holiday->fresh()->deleted_at);
    }

    public function testHolidayShouldNotDeleteForUnknownHoliday(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroy(random_int(1000, 2000));
    }

    public function testHolidayShouldNotDeleteForPassedHolidays(): void
    {
        $holiday = Holiday::factory()->create(['date' => Carbon::yesterday()->toDateString()]);

        $this->expectException(NicoBadRequestException::class);
        $this->repository->destroy($holiday->id);
    }
}
