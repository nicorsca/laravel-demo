<?php


namespace Department;


use App\Modules\Departments\Database\Models\Department;
use App\Modules\Departments\Repositories\DepartmentRepository;
use App\Modules\Departments\Requests\DepartmentCreateRequest;
use App\Modules\Departments\Requests\DepartmentUpdateRequest;
use App\System\Department\Database\Models\Department as SystemDepartment;
use App\System\Employee\Database\Models\EmployeeView;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Exceptions\ResourceInUseException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class DepartmentUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Departments\Repositories\DepartmentRepository
     */
    protected DepartmentRepository $repository;

    /**
     * @var \App\System\Department\Database\Models\Department
     */
    protected SystemDepartment $department;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->department = new SystemDepartment();
        $this->repository = new DepartmentRepository(new Department());
    }

    public function testDepartmentShouldCreate(): void
    {
        $data = $this->department->factory()->raw();
        $this->repository->create($data);
        $this->assertDatabaseHas('departments', $data);
    }

    public function testValidationShouldBeDoneDuringDepartmentCreate(): void
    {
        $request = new DepartmentCreateRequest();
        $this->assertFalse($this->validateField($request, 'title', ''));
        $this->assertTrue($this->validateField($request, 'title', 'HR Department'));
        $this->assertFalse($this->validateField($request, 'description', ''));
        $this->assertTrue($this->validateField($request, 'description', 'This is test description'));
        $this->assertFalse($this->validateField($request, 'status', ''));
        $this->assertFalse($this->validateField($request, 'status', 3));
        $this->assertTrue($this->validateField($request, 'status', 2));
    }

    public function testDepartmentShouldUpdate(): void
    {
        $department = $this->department->factory()->create();
        $this->assertDatabaseHas('departments', ['id' => $department->id]);
        $data = $this->department->factory()->raw();
        $this->repository->update($department->id, $data);
        $data['id'] = $department->id;
        $this->assertDatabaseHas('departments', $data);
    }

    /**
     * @throws \Exception
     */
    public function testDepartmentShouldNotUpdateForUnknownDepartment(): void
    {
        $data = $this->department->factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), $data);
    }

    public function testValidationShouldBeDoneDuringDepartmentUpdate(): void
    {
        $request = new DepartmentUpdateRequest();
        $this->assertFalse($this->validateField($request, 'title', ''));
        $this->assertTrue($this->validateField($request, 'title', 'HR Department'));
        $this->assertFalse($this->validateField($request, 'description', ''));
        $this->assertTrue($this->validateField($request, 'description', 'This is test description'));
        $this->assertFalse($this->validateField($request, 'status', ''));
        $this->assertFalse($this->validateField($request, 'status', 3));
        $this->assertTrue($this->validateField($request, 'status', 2));
    }

    public function testDepartmentShouldDetail(): void
    {
        $department = $this->department->factory()->create();
        $result = $this->repository->getById($department->id);
        $this->assertEquals($department->id, $result->id);
        $this->assertEquals($department->title, $result->title);
        $this->assertEquals($department->description, $result->description);
        $this->assertEquals($department->status, $result->status);
    }

    public function testDepartmentDetailShouldHaveEmployees(): void
    {
        $department = $this->department->factory()->has(EmployeeView::factory()->forUser()->count(3))->create();
        $result = $this->repository->getById($department->id);
        $this->assertCount(3, $result->employees);
    }

    /**
     * @throws \Exception
     */
    public function testDepartmentShouldNotFetchForUnknownDepartment(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }

    public function testDepartmentShouldList(): void
    {
        $this->department->factory()->count(5)->create();
        $result = $this->repository->getList();
        $this->assertCount(5, $result);
    }

    public function testDepartmentListAreSortedAndOrderedAndSearchable(): void
    {
        $departments = $this->department->factory()->count(3)->create();
        $departments = $departments->sortByDesc('title')->pluck('title');
        $result = $this->repository->getList(['sort_by' => 'title', 'sort_order' => 'desc']);
        $result = $result->getCollection()->pluck('title');
        $this->assertEquals($departments, $result);

        $department = $this->department->factory()->create();
        $result = $this->repository->getList(['sort_by' => 'title', 'sort_order' => 'desc', 'keyword' => $department->title]);
        $this->assertEquals($department->title, $result->first()->title);
    }

    public function testDepartmentListIsFilteredByStatus(): void
    {
        $this->department->factory()->count(3)->create();
        $this->department->factory()->count(2)->create(['status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getList(['status' => Status::STATUS_PUBLISHED, 'keyword' => '']);

        $this->assertCount(3, $result);
    }

    public function testDepartmentListEmptyWithWrongParameters(): void
    {
        $this->department->factory()->count(3)->create();
        $this->department->factory()->count(2)->create(['status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getList(['sort_by' => 'xyz', 'sort_order' => 'xyz', 'status' => 'xyz', 'keyword' => 'xyz']);

        $this->assertCount(0, $result);
    }

    public function testDepartmentShouldDelete(): void
    {
        $department = $this->department->factory()->create();
        $result = $this->repository->destroy($department->id);
        $this->assertEquals(true, $result);
        $this->assertNotNull($department->refresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testDepartmentShouldNotDeleteForUnknownDepartment(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroy(random_int(1000, 2000));
    }

    public function testDepartmentShouldNotDeleteIfEmployeesExists(): void
    {
        $department = $this->department->factory()->has(EmployeeView::factory()->forUser()->count(3))->create();
        $this->expectException(ResourceInUseException::class);
        $this->repository->destroy($department->id);
    }

    public function testDepartmentStatusToggle(): void
    {
        $department = $this->department->factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $this->repository->toggleStatus($department->id);
        $this->assertDatabaseHas('departments', ['id' => $department->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $this->repository->toggleStatus($department->id);
        $this->assertDatabaseHas('departments', ['id' => $department->id, 'status' => Status::STATUS_PUBLISHED]);
    }

    public function testUnknownDepartmentStatusCannotToggle(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleStatus(rand(1000, 10000));
    }

    public function testDepartmentShouldNotChangeStatusIfDepartmentHasEmployees(): void
    {
        $department = $this->department->factory()->has(EmployeeView::factory()->forUser()->count(3))->create(['status' => Status::STATUS_PUBLISHED]);
        $this->expectException(ResourceInUseException::class);
        $this->repository->toggleStatus($department->id);
    }

    public function testUserShouldGetEmployeeCount(): void
    {
        $department = $this->department->factory()->create();
        $result = $this->repository->getById($department->id);
        $this->assertEquals(0, $result->employees_count);
    }
}
