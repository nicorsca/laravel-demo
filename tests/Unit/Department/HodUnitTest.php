<?php


namespace Department;


use App\Modules\Departments\Database\Models\Department;
use App\Modules\Departments\Exceptions\HodExistException;
use App\Modules\Departments\Mails\AssignHodMail;
use App\Modules\Departments\Mails\RevokeHodMail;
use App\Modules\Departments\Repositories\DepartmentRepository;
use App\Modules\Employees\Database\Models\Employee;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class HodUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Departments\Repositories\DepartmentRepository
     */
    protected DepartmentRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DepartmentRepository(new Department());
    }

    public function testHodShouldBeAssigned(): void
    {
        Mail::fake();
        $department = Department::factory()->create();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department->id, $employee->id);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => $employee->id
        ]);
        Mail::assertQueued(AssignHodMail::class);
        Mail::assertNotQueued(RevokeHodMail::class);
    }

    public function testHodShouldNotBeAssignedForUnknownDepartment(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod(random_int(1000, 2000), $employee->id);
    }

    public function testHodShouldAssignedByRemovingExistingHod(): void
    {
        Mail::fake();
        $department = Department::factory()->create();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department->id, $employee->id);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => $employee->id
        ]);
        $newHod = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department->id, $newHod->id);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => $newHod->id
        ]);
        Mail::assertQueued(AssignHodMail::class);
        Mail::assertQueued(RevokeHodMail::class);
    }

    public function testHodShouldBeRevoked(): void
    {
        $department = Department::factory()->create();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department->id, $employee->id);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => $employee->id
        ]);
        Mail::fake();
        $this->repository->updateHod($department->id, null);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => null,
        ]);
        Mail::assertNotQueued(AssignHodMail::class);
        Mail::assertQueued(RevokeHodMail::class);
    }

    public function testHodShouldNotBeRevokedIfHodIsAbsent(): void
    {
        Mail::fake();
        $department = Department::factory()->create();
        $this->repository->updateHod($department->id, null);
        $this->assertDatabaseHas('departments', [
            'id' => $department->id,
            'employee_id' => null,
        ]);
        Mail::assertNothingQueued();
    }

    public function testHodShouldNotBeRevokedForUnknownDepartment(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateHod(random_int(1000, 2000), null);
    }

    public function testMailShouldNotBeSentTwiceIfAssignedTwice(): void
    {
        $department = Department::factory()->create();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department->id, $employee->id);
        Mail::fake();
        $this->repository->updateHod($department->id, $employee->id);
        Mail::assertNothingQueued();
    }

    public function testAEmployeeCannotBeHodOfMultipleDepartments()
    {
        $department1 = Department::factory()->create();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository->updateHod($department1->id, $employee->id);
        $this->expectException(HodExistException::class);
        $department2 = Department::factory()->create();
        $this->repository->updateHod($department2->id, $employee->id);
    }
}
