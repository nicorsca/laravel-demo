<?php


namespace Authentication;


use App\Http\Controllers\Auth\ResetPasswordController;
use App\System\User\Database\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ResetPasswordUnitTest extends TestCase
{
    /**
     * @var \App\Http\Controllers\Auth\ResetPasswordController
     */
    protected ResetPasswordController $controller;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new ResetPasswordController();
    }

    public function testPasswordShouldReset(): void
    {
        $user = User::factory()->create(['status' => 2]);
        $data = [
            'token' => Password::broker()->createToken($user),
            'email' => $user->email,
            'password' => 'new@Password1',
            'password_confirmation' => 'new@Password1'
        ];
        $this->controller->reset(new Request($data));

        $this->assertTrue(Hash::check($data['password'], $user->fresh()->password));
        $this->assertEquals($user->email, $user->fresh()->email);
    }

    public function testPasswordShouldNotResetForWrongToken(): void
    {
        $user = User::factory()->create(['status' => 2]);
        $data = [
            'token' => Str::random(40),
            'email' => $user->email,
            'password' => 'new@Password1',
            'password_confirmation' => 'new@Password1'
        ];
        $result = $this->controller->reset(new Request($data));

        $expectedJson = [
            "message" => "The password reset token has expired or is invalid.",
            "code" => "passwords_token",
            "code_text" => "passwords.token"
        ];

        $this->assertEquals($expectedJson, $result->getOriginalContent());
        $this->assertEquals(400, $result->getStatusCode());
    }

    public function testPasswordShouldNotResetForWrongEmail(): void
    {
        $user = User::factory()->create(['status' => 2]);
        $data = [
            'token' => Password::broker()->createToken($user),
            'email' => $this->faker->email,
            'password' => 'new@Password1',
            'password_confirmation' => 'new@Password1'
        ];
        $result = $this->controller->reset(new Request($data));

        $expectedJson = [
            "message" => "The given email doesn't exist in our system.",
            "code" => "passwords_user",
            "code_text" => "passwords.user"
        ];

        $this->assertEquals($expectedJson, $result->getOriginalContent());
        $this->assertEquals(400, $result->getStatusCode());
    }

    public function testPasswordShouldNotResetForDifferentPasswordConfirmation(): void
    {
        $user = User::factory()->create(['status' => 2]);
        $data = [
            'token' => Password::broker()->createToken($user),
            'email' => $user->email,
            'password' => 'new@Password1',
            'password_confirmation' => 'new@Password1diff'
        ];
        $this->expectException(ValidationException::class);
        $this->controller->reset(new Request($data));
    }
}
