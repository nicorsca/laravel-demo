<?php


namespace Authentication;


use App\System\User\Database\Models\User;
use NicoSystem\Foundation\Status;

class LoginUnitTest extends BaseAuthUnitTest
{
    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testUserCanLogin(): void
    {
        $user = User::factory()->create([
            'status' => User::STATUS_ACTIVE,
            'is_locked' => User::STATUS_UNLOCKED,
        ]);
        $data = [
            'username' => $user->email,
            'password' => 'Password@2078',
        ];
        $result = $this->post('/api/auth/login', $data);
        $result->assertStatus(200)
            ->assertHeader('Access-Token')
            ->assertHeader('Refresh-Token')
            ->assertHeader('Token-Type')
            ->assertHeader('Expires-In');
    }

    public function testUserCannotLoginWithWrongCredentials(): void
    {
        $user = User::factory()->create([
            'status' => Status::STATUS_PUBLISHED,
            'is_locked' => User::STATUS_UNLOCKED,
        ]);
        $data = [
            'username' => $user->email,
            'password' => 'password1',
        ];
        $expectedJson = [
            'message' => 'Invalid username/password combination.',
            'code' => 'invalid_credentials',
            'code_text' => 'The user credentials were incorrect.'
        ];

        $result = $this->post('/api/auth/login', $data);
        $result->assertStatus(401)
            ->assertJson($expectedJson)
            ->assertHeaderMissing('Access-Token')
            ->assertHeaderMissing('Refresh-Token')
            ->assertHeaderMissing('Token-Type')
            ->assertHeaderMissing('Expires-In');
    }

    public function testUserShouldBlockAfterMultipleWrongLoginAttempts(): void
    {
        $user = User::factory()->create(['status' => 2]);

        $data = [
            'username' => $user->email,
            'password' => 'password@1'
        ];

        for ($i = 0; $i < 6; $i++) {
            $this->post('/api/auth/login', $data);
        }

        $expectedJson = [
            'message' => 'Too many wrong login attempts were made, please try again after some time',
            'code' => 'too_many_bad_login_attempt',
            'code_text' => 'User account is disable due to too many wrong attempt'
        ];

        //tried with right password
        $data['password'] = 'password';
        $result = $this->post('/api/auth/login', $data);

        $result->assertStatus(429)
            ->assertJson($expectedJson);
    }

    public function testValidationErrorShouldBeShownForEmptyUsernameAndPassword(): void
    {
        $data = [
            'username' => '',
            'password' => '',
        ];
        $result = $this->post('/api/auth/login', $data);
        $expectedJson = [
            'username' => ['The username field is required.'],
            'password' => ['The password field is required.']
        ];
        $result->assertStatus(417)
            ->assertJson($expectedJson);
    }
}
