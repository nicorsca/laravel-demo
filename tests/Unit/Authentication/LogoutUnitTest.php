<?php

namespace Authentication;

use App\System\User\Database\Models\User;
use Illuminate\Support\Facades\DB;

class LogoutUnitTest extends BaseAuthUnitTest
{

    /**
     * @var \App\System\User\Database\Models\User
     */
    private User $user;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'email_verified_at' => now(),
            'status' => User::STATUS_ACTIVE,
            'is_locked' => User::STATUS_UNLOCKED,
        ]);
    }

    public function testUserCanLogout()
    {
        $data = [
            'username' => $this->user->email,
            'password' => 'Password@2078',
        ];
        $login = $this->post('/api/auth/login', $data);
        $accessToken = 'Bearer ' . $login->headers->get('access-token');
        $this->assertDatabaseHas('oauth_access_tokens', [
            'user_id' => $this->user->id,
            'revoked' => 0
        ]);
        $response = $this->json('GET', 'api/auth/logout', [], ['authorization' => $accessToken]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('oauth_access_tokens', [
            'user_id' => $this->user->id,
            'revoked' => 1
        ]);
    }

    public function testUserCannotLogoutWithoutToken()
    {
        $response = $this->json('GET', 'api/auth/logout');
        $response->assertStatus(401);
    }

    public function testUserCannotLogoutAgainWithExpiredToken()
    {
        $data = [
            'username' => $this->user->email,
            'password' => 'Password@2078',
        ];
        $login = $this->post('/api/auth/login', $data);
        $accessToken = 'Bearer ' . $login->headers->get('access-token');
        $this->assertDatabaseHas('oauth_access_tokens', [
            'user_id' => $this->user->id,
            'revoked' => 0
        ]);
        DB::table('oauth_access_tokens')->where('user_id', $this->user->id)->update(['revoked' => 1]);
        $this->assertDatabaseHas('oauth_access_tokens', [
            'user_id' => $this->user->id,
            'revoked' => 1
        ]);
        $response = $this->json('GET', 'api/auth/logout', [], ['authorization' => $accessToken]);
        $response->assertStatus(401);
    }

}
