<?php

namespace Tests\Unit\Authentication;

use App\System\User\Database\Models\User;
use Laravel\Socialite\Facades\Socialite;
use Mockery;
use Tests\TestCase;

/**
 * Class SocialiteUnitTest
 * @package Tests\Unit\Authentication
 */
class SocialiteUnitTest extends TestCase
{

    public function testRedirectsToGoogleLogin(): void
    {
        $redirectUri = config('app.front_app_url');
        $response = $this->call('GET','auth/login/google', ['redirect_uri'=>$redirectUri.'/auth/callback']);

        $this->assertStringContainsString('accounts.google.com/o/oauth2/auth', $response->baseResponse->getTargetUrl());
    }

    public function testRedirectsToMicrosoftLogin(): void
    {
        $redirectUri = config('app.front_app_url');
        $response = $this->call('GET','auth/login/microsoft', ['redirect_uri'=>$redirectUri.'/auth/callback']);
        $this->assertStringContainsString('login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id', $response->baseResponse->getTargetUrl());
    }

    public function testNotRegisteredProviderShouldNotRedirect(): void
    {
        $response = $this->get('api/auth/login/linkedIn');
        $response->assertStatus(404);
    }

    public function testOnlyRegisteredUserCanLoginUsingSocialite(): void
    {
        $user = User::factory()->hasProviders()->create();
        $provider = $user->providers()->first();
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser
            ->shouldReceive('getId')
            ->andReturn($provider->provider_id)
            ->shouldReceive('getName')
            ->andReturn($user->name)
            ->shouldReceive('getEmail')
            ->andReturn($user->email)
            ->shouldReceive('getAvatar')
            ->andReturn($this->faker->imageUrl())
            ->shouldReceive('getRaw')
            ->andReturn(['id' => $provider->provider_id, 'name' => $user->name]);
        Socialite::shouldReceive('driver->stateless->user')->andReturn($abstractUser);
        $response = $this->withSession(['redirect_uri' => 'http://localhost:4200/callback'])->get('auth/login/google/callback');
        $response->assertStatus(302);
    }

    public function testUnregisteredUserCannotLoginUsingSocialite(): void
    {
        $id = $this->faker->uuid;
        $name = $this->faker->name;
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser
            ->shouldReceive('getId')
            ->andReturn($id)
            ->shouldReceive('getName')
            ->andReturn($name)
            ->shouldReceive('getEmail')
            ->andReturn($this->faker->email)
            ->shouldReceive('getAvatar')
            ->andReturn($this->faker->imageUrl())
            ->shouldReceive('getRaw')
            ->andReturn(['id' => $id, 'name' => $name]);;
        Socialite::shouldReceive('driver->stateless->user')->andReturn($abstractUser);
        $response = $this->withSession(['redirect_uri' => 'http://localhost:4200/callback'])->get('auth/login/google/callback');
        $response->assertStatus(302);
    }

}
