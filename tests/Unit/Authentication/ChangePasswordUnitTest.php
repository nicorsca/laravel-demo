<?php


namespace Authentication;


use App\System\User\Database\Models\User;
use Illuminate\Support\Facades\Hash;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class ChangePasswordUnitTest extends TestCase
{

    public function testUserCanChangePassword(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $data = [
            'old_password' => 'Password@2078',
            'new_password' => 'new@Password1',
            'new_password_confirmation'=>'new@Password1'
        ];
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $response = $this->put('/api/auth/change-password', $data, $headers);
        $response->assertStatus(200);
        $this->assertTrue(Hash::check($data['new_password'], $user->fresh()->password));
    }

    public function testUserCannotChangePasswordWhenOldAndNewPasswordAreSame(): void
    {
        $user = User::factory()->create([
            'status' => Status::STATUS_PUBLISHED,
            'password' => bcrypt('new@Password1'),
            ]);
        $data = [
            'old_password' => 'new@Password1',
            'new_password' => 'new@Password1',
            'new_password_confirmation' => 'new@Password1'
        ];
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $response = $this->put('/api/auth/change-password', $data, $headers);
        $response->assertStatus(417);
        $response->assertExactJson(['new_password' => [trans('auth.recent_password')]]);
    }

    public function testUserCannotChangePasswordWhenOldPasswordIsWrong(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $data = [
            'old_password' => 'wrong-password',
            'new_password' => 'new@Password1',
            'new_password_confirmation' => 'new@Password1'
        ];
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $response = $this->put('/api/auth/change-password', $data, $headers);
        $response->assertStatus(417);
        $response->assertExactJson(["old_password" => [trans('auth.old_password')]]);
    }
}
