<?php


namespace Authentication;


use App\Http\Controllers\Auth\ForgotPasswordController;
use App\System\User\Database\Models\User;
use Illuminate\Http\Request;
use Tests\TestCase;

class ForgotPasswordUnitTest extends TestCase
{
    /**
     * @var \App\Http\Controllers\Auth\ForgotPasswordController
     */
    protected ForgotPasswordController $controller;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new ForgotPasswordController();
    }

    public function testEmailShouldNotSendForUnregisteredEmail(): void
    {
        $response = $this->controller->sendResetLinkEmail(new Request(['email' => $this->faker->email]));
        $this->assertEquals($response->getStatusCode(), 400);
    }

    public function testEmailShouldSend(): void
    {
        $user = User::factory()->create(['status' => 2]);
        $this->controller->sendResetLinkEmail(new Request(['email' => $user->email]));

        $this->assertDatabaseHas('password_resets', ['email' => $user->email]);
    }

}
