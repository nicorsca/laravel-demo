<?php

namespace Attendance;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\AttendanceView;
use App\Modules\Attendances\Database\Models\EmployeeView;
use App\Modules\Attendances\Database\Models\Holiday;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Repositories\AttendanceReportRepository;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Tests\TestCase;

class AttendanceDashboardEmployeeUnitTest extends TestCase
{
    private Employee $employee;

    private AttendanceReportRepository $repository;

    public function testAttendanceSummaryShouldBeShownForGivenDateRange(): void
    {
        $checkInTime = Carbon::today()->unix() * 1000;
        $checkOutTime = Carbon::now()->unix() * 1000;
        Attendance::factory()->create([
            'attend_at' => $checkInTime,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_IN,
        ]);
        Attendance::factory()->create([
            'attend_at' => $checkOutTime,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $result = $this->repository->getEmployeeAttendanceSummary([
            'start_date' => Carbon::yesterday()->toDateString(),
            'end_date' => Carbon::tomorrow()->toDateString(),
        ], $this->employee->id);
        $this->assertInstanceOf(AttendanceView::class, $result);

        $this->assertEquals([
            'present_days' => 1,
            'leave_days' => 0,
            'total_office_time' => $checkOutTime - $checkInTime,
            'total_work_time' => $checkOutTime - $checkInTime,
            'total_break_time' => 0,
            'total_leave_time' => 0
        ], $result->toArray());

        Attendance::factory()->create([
            'attend_at' => Carbon::tomorrow()->unix() * 1000,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_IN,
        ]);
        Attendance::factory()->create([
            'attend_at' => Carbon::tomorrow()->unix() * 1000,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_OUT,
        ]);
        $result = $this->repository->getEmployeeAttendanceSummary([
            'start_date' => Carbon::yesterday()->toDateString(),
            'end_date' => Carbon::tomorrow()->toDateString(),
        ], $this->employee->id);
        $this->assertEquals([
            'present_days' => 1,
            'leave_days' => 1,
            'total_office_time' => $checkOutTime - $checkInTime,
            'total_work_time' => $checkOutTime - $checkInTime,
            'total_break_time' => 0,
            'total_leave_time' => 0
        ], $result->toArray());

        $result = $this->repository->getEmployeeAttendanceSummary([
            'start_date' => Carbon::today()->subMonth()->toDateString(),
            'end_date' => Carbon::today()->subDays(3)->toDateString(),
        ], $this->employee->id);
        $this->assertEquals([
            'present_days' => 0,
            'leave_days' => 0,
            'total_office_time' => 0,
            'total_work_time' => 0,
            'total_break_time' => 0,
            'total_leave_time' => 0
        ], $result->toArray());
    }

    public function testEmployeeSummaryShouldNotBeShownForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getEmployeeAttendanceSummary([
            'start_date' => Carbon::today()->subMonth()->toDateString(),
            'end_date' => Carbon::today()->subDays(3)->toDateString(),
        ], random_int(1000, 2000));
    }

    public function testEmployeeChartShouldBeShownOfEmployee(): void
    {
        Holiday::factory()->create(['date' => Carbon::yesterday()->toDateString()]);
        $checkInTime = Carbon::today()->unix() * 1000;
        $checkOutTime = Carbon::now()->unix() * 1000;
        Attendance::factory()->create([
            'attend_at' => $checkInTime,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_IN,
        ]);
        Attendance::factory()->create([
            'attend_at' => $checkOutTime,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);

        $leaveIn = Carbon::tomorrow()->unix() * 1000;
        $leaveOut = (Carbon::tomorrow()->unix() * 1000) + 10;
        Attendance::factory()->create([
            'attend_at' => $leaveIn,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_IN,
        ]);
        Attendance::factory()->create([
            'attend_at' => $leaveOut,
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_OUT,
        ]);
        $startDate = Carbon::yesterday()->toDateString();
        $endDate = Carbon::tomorrow()->toDateString();
        $result = $this->repository->getEmployeeAttendanceCalendar([
            'start_date' => $startDate,
            'end_date' => $endDate,
        ], $this->employee->id);

        $this->assertEquals($result['data'][0], [
            'date' => Carbon::yesterday()->toDateString(),
            'weekend' => in_array(Carbon::yesterday()->dayOfWeek, [0,6], false),
            'holiday' => true,
            'check_in' => null,
            'check_out' => null,
            'leave_in' => null,
            'leave_out' => null,
            'office_time' => null,
            'work_time' => null,
            'break_time' => null,
            'leave_time' => null,
            'breaks' => null,
        ]);
        $this->assertEquals(Arr::except($result['data'][1], 'breaks'), [
            'date' => Carbon::today()->toDateString(),
            'weekend' => in_array(Carbon::today()->dayOfWeek, [0,6], false),
            'holiday' => false,
            'check_in' => gmdate('Y-m-d H:i:s', $checkInTime),
            'check_out' => gmdate('Y-m-d H:i:s', $checkOutTime),
            'leave_in' => null,
            'leave_out' => null,
            'office_time' => (string)($checkOutTime - $checkInTime),
            'work_time' => (string)($checkOutTime - $checkInTime),
            'break_time' => '0',
            'leave_time' => '0'
        ]);
        $this->assertEquals(Arr::except($result['data'][2], 'breaks'), [
            'date' => Carbon::tomorrow()->toDateString(),
            'weekend' => in_array(Carbon::tomorrow()->dayOfWeek, [0,6], false),
            'holiday' => false,
            'check_in' => null,
            'check_out' => null,
            'leave_in' => gmdate('Y-m-d H:i:s', $leaveIn),
            'leave_out' => gmdate('Y-m-d H:i:s', $leaveOut),
            'office_time' => '0',
            'work_time' => '0',
            'break_time' => '0',
            'leave_time' => (string)($leaveOut - $leaveIn)
        ]);
    }

    public function testEmployeeChartShouldNotBeShownForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getEmployeeAttendanceCalendar([
            'start_date' => Carbon::today()->subMonth()->toDateString(),
            'end_date' => Carbon::today()->subDays(3)->toDateString(),
        ], random_int(1000, 2000));
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository = new AttendanceReportRepository(new AttendanceView(), new EmployeeView(), new Holiday(), new Setting());
    }
}
