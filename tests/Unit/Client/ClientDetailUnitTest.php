<?php

namespace Client;

use App\Modules\Clients\Database\Models\Client;
use App\Modules\Clients\Repositories\ClientRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ClientDetailUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Clients\Repositories\ClientRepository
     */
    private ClientRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ClientRepository(new Client());
    }


    public function testClientShouldDetail(): void
    {
        $client = Client::factory()->create();
        $result = $this->repository->getById($client->id);
        $this->assertInstanceOf(Client::class, $result);
        $this->assertEquals($client->id, $result->id);
        $this->assertEquals($client->name, $result->name);
        $this->assertEquals($client->avatar, $result->avatar);
    }

    public function testClientShouldNotDetailForUnknownClient(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }


    public function testClientShouldList(): void
    {
        Client::factory()->count(3)->create();

        $result = $this->repository->getList(['keyword' => '', 'status' => '', 'title' => '']);
        $this->assertCount(3, $result);
    }

    public function testClientShouldSearchThroughNameOrKeyword(): void
    {
        Client::factory()->count(3)->create();
        $client = Client::factory()->create();

        $result = $this->repository->getList(['keyword' => $client->name]);
        $this->assertCount(1, $result);
    }

    public function testClientListShouldBeEmptyForWrongInputs(): void
    {
        Client::factory()->count(5)->create();

        $result = $this->repository->getList(['keyword' => 'test-xyz']);
        $this->assertCount(0, $result);
    }
}
