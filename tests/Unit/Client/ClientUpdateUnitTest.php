<?php

namespace Client;

use App\Modules\Clients\Database\Models\Client;
use App\Modules\Clients\Repositories\ClientRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ClientUpdateUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Clients\Repositories\ClientRepository
     */
    private ClientRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ClientRepository(new Client());
    }

    public function testClientShouldBeUpdated(): void
    {
        $client = Client::factory()->create();
        $data = Client::factory()->raw();
        $result = $this->repository->update($client->id, $data);
        $this->assertInstanceOf(Client::class, $result);
        $this->assertEquals($result->name, $data['name']);
        $this->assertEquals($result->avatar, $data['avatar']);
        $this->assertDatabaseHas('clients', $data);
    }

    public function testClientShouldNotBeUpdatedForUnknownClient(): void
    {
        $data = Client::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), $data);
    }
}
