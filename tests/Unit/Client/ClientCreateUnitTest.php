<?php

namespace Client;

use App\Modules\Clients\Database\Models\Client;
use App\Modules\Clients\Repositories\ClientRepository;
use Tests\TestCase;

class ClientCreateUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Clients\Repositories\ClientRepository
     */
    private ClientRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ClientRepository(new Client());
    }

    public function testClientShouldBeCreated(): void
    {
        $data = Client::factory()->raw();
        $result = $this->repository->create($data);
        $this->assertInstanceOf(Client::class, $result);
        $this->assertEquals($result->name, $data['name']);
        $this->assertEquals($result->avatar, $data['avatar']);
        $this->assertDatabaseHas('clients', $data);
    }
}
