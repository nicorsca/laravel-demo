<?php

namespace Setting;

use App\Modules\Settings\Database\Models\Setting;
use App\Modules\Settings\Repositories\SettingRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class SettingDetailUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Settings\Repositories\SettingRepository
     */
    private SettingRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SettingRepository(new Setting());
    }

    public function testSettingDetailShouldFetch(): void
    {
        $setting = $this->repository->getList()->first();

        $result = $this->repository->getById($setting->id);
        $this->assertInstanceOf(Setting::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($setting->value, $result->value);
    }

    public function testSettingDetailShouldNotFetchForUnknownSetting(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }

    public function testSettingListShouldBeShown(): void
    {
        $setting = $this->repository->getList();
        $this->assertInstanceOf(LengthAwarePaginator::class, $setting);
    }
}
