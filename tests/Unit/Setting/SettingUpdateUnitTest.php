<?php

namespace Setting;

use App\Modules\Settings\Database\Models\Setting;
use App\Modules\Settings\Repositories\SettingRepository;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use App\System\Setting\Foundation\SettingType;
use Tests\TestCase;

class SettingUpdateUnitTest extends TestCase
{

    /**
     * @var \App\Modules\Settings\Repositories\SettingRepository
     */
    private SettingRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SettingRepository(new Setting());
    }

    public function testSettingValueShouldUpdateForArray(): void
    {
        $setting = $this->repository->getList(['type' => SettingType::ARRAY])->first();

        $data = ['value' => [LeaveType::SICK_LEAVE, LeaveType::BEREAVEMENT_LEAVE]];
        $result = $this->repository->update($setting->id, $data);
        $this->assertInstanceOf(Setting::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($data['value'], $result->value);
    }

    public function testSettingValueShouldUpdateForString(): void
    {
        $setting = $this->repository->getList(['key' => SettingKey::SYSTEM_FISCAL_DATE_START])->first();

        $data = ['value' => '01-01'];
        $result = $this->repository->update($setting->id, $data);
        $this->assertInstanceOf(Setting::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($data['value'], $result->value);
    }

    public function testSettingValueShouldUpdateForInteger(): void
    {
        $setting = $this->repository->getList(['type' => SettingType::INTEGER])->first();

        $data = ['value' => 10];
        $result = $this->repository->update($setting->id, $data);
        $this->assertInstanceOf(Setting::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($data['value'], $result->value);
    }
}
