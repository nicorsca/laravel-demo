<?php

namespace Project;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\ProjectRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ProjectDetailUnitTest extends TestCase
{
    /**
     * @var ProjectRepository
     */
    private ProjectRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ProjectRepository(new Project());
    }

    public function testProjectShouldDetail(): void
    {
        $project = Project::factory()->forClient()->create();

        $result = $this->repository->getById($project->id);

        $this->assertInstanceOf(Project::class, $result);
        $this->assertEquals($project->title, $result->title);
        $this->assertEquals($project->description, $result->description);
        $this->assertEquals($project->code, $result->code);
        $this->assertEquals($project->type, $result->type);
        $this->assertEquals($project->avatar, $result->avatar);
        $this->assertEquals($project->status, $result->status);
        $this->assertEquals($project->start_date, $result->start_date);
        $this->assertEquals($project->end_date, $result->end_date);
        $this->assertEquals($project->client_id, $result->client_id);
    }

    public function testProjectShouldNotDetailForUnknownProject(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }

    public function testProjectShouldList(): void
    {
        Project::factory()->forClient()->count(3)->create();

        $result = $this->repository->getList(['keyword' => '', 'status' => '', 'title' => '']);
        $this->assertCount(3, $result);
    }

    public function testProjectShouldSearchThroughTitleOrKeyword(): void
    {
        Project::factory()->forClient()->count(3)->create();
        $project = Project::factory()->forClient()->create();

        $result = $this->repository->getList(['keyword' => $project->title]);
        $this->assertCount(1, $result);
    }

    public function testProjectListShouldBeEmptyForWrongInputs(): void
    {
        Project::factory()->forClient()->count(3)->create();

        $result = $this->repository->getList(['keyword' => 'test-xyz']);
        $this->assertCount(0, $result);
    }

    public function testProjectShouldFilterByClient(): void
    {
        Project::factory()->forClient()->count(3)->create();
        $project = Project::factory()->forClient()->create();

        $result = $this->repository->getList(['client_id' => $project->client->id]);
        $this->assertCount(1, $result);
    }

    public function testProjectListIsFilteredByCode(): void
    {
        Project::factory()->forClient()->count(3)->create();
        $project = Project::factory()->forClient()->create();
        $result = $this->repository->getList(['code' => $project->code]);

        $this->assertCount(1, $result);
    }
}
