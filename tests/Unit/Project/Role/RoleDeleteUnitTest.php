<?php

namespace Project\Role;

use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Repositories\RoleRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class RoleDeleteUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Projects\Repositories\RoleRepository
     */
    private RoleRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new RoleRepository(new ProjectRole());
    }

    public function testRoleShouldBeDeleted(): void
    {
        $role = ProjectRole::factory()->create();
        $result = $this->repository->destroy($role->id);

        $this->assertTrue($result);
        $this->assertNotNull($role->fresh()->deleted_at);
    }

    public function testRoleShouldNotBeDeletedForUnknownRole(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroy(random_int(1000, 2090));
    }
}
