<?php

namespace Project\Role;

use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Repositories\RoleRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class RoleUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Projects\Repositories\RoleRepository
     */
    private RoleRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new RoleRepository(new ProjectRole());
    }

    public function testRoleShouldBeUpdated(): void
    {
        $role = ProjectRole::factory()->create();
        $data = ProjectRole::factory()->raw();

        $result = $this->repository->update($role->id, $data);
        $this->assertInstanceOf(ProjectRole::class, $result);
        $this->assertEquals($result->id, $role->id);
        $this->assertEquals($result->title, $data['title']);
        $this->assertDatabaseHas('project_roles', $data);
    }

    public function testRoleShouldNotBeUpdatedForUnknownRole(): void
    {
        $data = ProjectRole::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), $data);
    }
}
