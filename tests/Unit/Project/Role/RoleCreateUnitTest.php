<?php

namespace Project\Role;

use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Repositories\RoleRepository;
use Tests\TestCase;

class RoleCreateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Projects\Repositories\RoleRepository
     */
    private RoleRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new RoleRepository(new ProjectRole());
    }

    public function testRoleShouldBeCreated(): void
    {
        $data = ProjectRole::factory()->raw();

        $result = $this->repository->create($data);
        $this->assertInstanceOf(ProjectRole::class, $result);
        $this->assertDatabaseHas('project_roles', $data);
    }

}
