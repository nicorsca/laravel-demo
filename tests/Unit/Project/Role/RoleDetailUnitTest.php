<?php

namespace Project\Role;

use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Repositories\RoleRepository;
use Tests\TestCase;

class RoleDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Projects\Repositories\RoleRepository
     */
    private RoleRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new RoleRepository(new ProjectRole());
    }

    public function testProjectRoleShouldList(): void
    {
        ProjectRole::factory()->count(3)->create();

        $result = $this->repository->getList(['keyword' => '', 'status' => '', 'title' => '']);
        $this->assertCount(3, $result);
    }

    public function testProjectRoleShouldSearchThroughTitleOrKeyword(): void
    {
        ProjectRole::factory()->count(3)->create();
        $role = ProjectRole::factory()->create();

        $result = $this->repository->getList(['keyword' => $role->title]);
        $this->assertCount(1, $result);
    }

    public function testProjectRoleListShouldBeEmptyForWrongInputs(): void
    {
        ProjectRole::factory()->count(3)->create();

        $result = $this->repository->getList(['keyword' => 'test-xyz']);
        $this->assertCount(0, $result);
    }
}
