<?php


namespace Project\Document;


use App\Modules\Projects\Database\Models\Document;
use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\DocumentRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class DocumentDeleteUnitTest extends TestCase
{
    /**
     * @var DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @var \App\System\Project\Database\Models\Project
     */
    private \App\System\Project\Database\Models\Project $project;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Project(), new Document());
        $this->project = Project::factory()->forClient()->create();
    }

    public function testDocumentShouldDelete(): void
    {
        $document = $this->project->documents()->create(Document::factory()->raw());
        $this->repository->destroyDocument($this->project->id, $document->id);
        $this->assertNotNull($document->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testDocumentShouldNotDeleteForUnknownProject(): void
    {
        $document = $this->project->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyDocument(random_int(10000, 20000), $document->id);
    }

    public function testDocumentShouldNotDeleteForUnknownDocument(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyDocument($this->project->id, random_int(10000, 20000));
    }
}
