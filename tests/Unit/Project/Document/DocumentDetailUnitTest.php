<?php


namespace Project\Document;


use App\Modules\Projects\Database\Models\Document;
use App\System\Common\Database\Models\Document as SystemDocument;
use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\DocumentRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class DocumentDetailUnitTest extends TestCase
{
    /**
     * @var DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @var \App\System\Project\Database\Models\Project
     */
    private \App\System\Project\Database\Models\Project $project;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Project(), new Document());
        $this->project = Project::factory()->forClient()->create();
    }

    public function testDocumentDetailShouldFetch(): void
    {
        $document = $this->project->documents()->create(Document::factory()->raw());
        $result = $this->repository->getDocumentById($this->project->id, $document->id);
        $this->assertInstanceOf(SystemDocument::class, $result);
        $this->assertEquals($result->id, $document->id);
        $this->assertEquals($result->title, $document->title);
        $this->assertEquals($result->url, $document->url);
        $this->assertEquals($result->thumbnail, $document->thumbnail);
        $this->assertEquals($result->status, $document->status);
    }

    public function testDocumentDetailShouldNotFetchForUnknownProject(): void
    {
        $document = $this->project->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentById(random_int(10000, 20000), $document->id);
    }

    public function testDocumentDetailShouldNotFetchForUnknownDocument(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentById($this->project->id, random_int(10000, 20000));
    }

    public function testDocumentListShouldBeFetched(): void
    {
        $this->project->documents()->createMany(Document::factory()->count(5)->raw());
        $result = $this->repository->getDocumentList([], $this->project->id);
        $this->assertCount(5, $result);
    }

    public function testDocumentListShouldNotBeFetchedForUnknownProject(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentList([], random_int(1000, 2000));
    }

    public function testDocumentListAreSortedAndOrderedAndSearchable(): void
    {
        $documents = $this->project->documents()->createMany(Document::factory()->count(3)->raw());
        $documents = $documents->sortByDesc('title')->pluck('title');
        $result = $this->repository->getDocumentList(['sort_by' => 'title', 'sort_order' => 'desc'], $this->project->id);
        $result = $result->getCollection()->pluck('title');
        $this->assertEquals($documents, $result);

        $document = $this->project->documents()->create(Document::factory()->raw());
        $result = $this->repository->getDocumentList(['keyword' => $document->title], $this->project->id);
        $this->assertEquals($document->title, $result->first()->title);
    }

    public function testDocumentListIsFilteredByStatus(): void
    {
        $this->project->documents()->createMany(Document::factory()->count(3)->raw());
        $this->project->documents()->createMany(Document::factory()->count(2)->raw(['status' => Status::STATUS_UNPUBLISHED]));
        $result = $this->repository->getDocumentList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->project->id);

        $this->assertCount(3, $result);
    }

    public function testDocumentListEmptyWithWrongParameters(): void
    {
        $this->project->documents()->createMany(Document::factory()->count(3)->raw());
        $this->project->documents()->createMany(Document::factory()->count(2)->raw(['status' => Status::STATUS_UNPUBLISHED]));
        $result = $this->repository->getDocumentList(['status' => 'xyz', 'keyword' => 'xyz'], $this->project->id);

        $this->assertCount(0, $result);
    }
}
