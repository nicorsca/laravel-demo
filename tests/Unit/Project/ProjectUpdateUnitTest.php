<?php

namespace Project;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\ProjectRepository;
use App\System\Client\Database\Models\Client;
use App\System\Project\Foundation\ProjectStatus;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ProjectUpdateUnitTest extends TestCase
{
    /**
     * @var ProjectRepository
     */
    private ProjectRepository $repository;

    /**
     * @var \App\System\Client\Database\Models\Client
     */
    private Client $client;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ProjectRepository(new Project());
        $this->client = Client::factory()->create();
    }

    public function testProjectShouldBeUpdated(): void
    {
        $project = Project::factory()->forClient()->create();
        $data = Project::factory()->raw(['client_id' => $this->client->id]);

        $result = $this->repository->update($project->id, $data);
        $this->assertInstanceOf(Project::class, $result);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['description'], $result->description);
        $this->assertEquals($data['code'], $result->code);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['avatar'], $result->avatar);
        $this->assertEquals($data['status'], $result->status);
        $this->assertEquals($data['start_date'], $result->start_date);
        $this->assertEquals($data['end_date'], $result->end_date);
        $this->assertEquals($data['client_id'], $result->client_id);
        $this->assertDatabaseHas('projects', $data);
    }

    public function testProjectShouldNotBeUpdatedForUnknownProject(): void
    {
        $data = Project::factory()->raw(['client_id' => $this->client->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), $data);
    }

    public function testProjectShouldBeArchived(): void
    {
        $project = Project::factory()->forClient()->create();
        $this->repository->archive($project->id);
        $this->assertEquals(ProjectStatus::STATUS_ARCHIVE, $project->fresh()->status);
    }

    public function testProjectShouldNotBeArchivedForUnknownProject(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->archive(random_int(1000, 2000));
    }

    public function testProjectShouldNotBeArchivedForAlreadyArchivedProject(): void
    {
        $project = Project::factory()->forClient()->create(['status' => ProjectStatus::STATUS_ARCHIVE]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->archive($project->id);
    }
}
