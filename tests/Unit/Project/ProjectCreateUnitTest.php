<?php

namespace Project;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Repositories\ProjectRepository;
use App\System\Client\Database\Models\Client;
use Tests\TestCase;

class ProjectCreateUnitTest extends TestCase
{
    /**
     * @var ProjectRepository
     */
    private ProjectRepository $repository;

    /**
     * @var \App\System\Client\Database\Models\Client
     */
    private Client $client;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ProjectRepository(new Project());
        $this->client = Client::factory()->create();
    }

    public function testProjectShouldBeCreated(): void
    {
        $data = Project::factory()->raw(['client_id' => $this->client->id]);

        $result = $this->repository->create($data);
        $this->assertInstanceOf(Project::class, $result);
        $this->assertEquals($data['title'], $result->title);
        $this->assertEquals($data['description'], $result->description);
        $this->assertEquals($data['code'], $result->code);
        $this->assertEquals($data['type'], $result->type);
        $this->assertEquals($data['avatar'], $result->avatar);
        $this->assertEquals($data['status'], $result->status);
        $this->assertEquals($data['start_date'], $result->start_date);
        $this->assertEquals($data['end_date'], $result->end_date);
        $this->assertEquals($data['client_id'], $result->client_id);
        $this->assertDatabaseHas('projects', $data);
    }

}
