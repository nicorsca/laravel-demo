<?php


namespace Employee\History;


use App\Events\Employee\History\Deactivated;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\History;
use App\Modules\Employees\Repositories\HistoryRepository;
use App\System\Project\Database\Models\Project;
use App\System\Project\Database\Models\ProjectRole;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class DeactivateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\HistoryRepository
     */
    private HistoryRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    public function testEmployeeShouldBeDeactivated(): void
    {
        $data = History::factory()->raw(['url' => $this->faker->url]);
        $this->repository->deactivate($data, $this->employee->id);
        $data['status'] = Employee::STATUS_INACTIVE;
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('employee_histories', Arr::only($data, ['date', 'status', 'comment', 'employee_id']));
        $this->assertEquals(Employee::STATUS_INACTIVE, $this->employee->user->fresh()->status);
        $this->assertEquals($data['date'], $this->employee->fresh()->detached_at);
        $this->assertDatabaseHas('documents', [
            'url' => $data['url'],
            'documentary_id' => $this->employee->histories()->get()->last()->id,
            'documentary_type' => 'History',
        ]);
    }

    public function testEmployeeShouldNotBeDeactivatedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $data = History::factory()->raw();
        $this->repository->deactivate($data, random_int(1000, 2000));
    }

    public function testEmployeeShouldNotBeDeactivatedForAlreadyDeactivatedEmployee(): void
    {
        $this->expectException(NicoBadRequestException::class);
        $data = History::factory()->raw();
        $this->repository->deactivate($data, $this->employee->id);
        $this->repository->deactivate($data, $this->employee->id);
    }

    public function testEventShouldBeFiredDuringDeactivate(): void
    {
        Event::fake();
        $data = History::factory()->raw(['url' => $this->faker->url]);
        $this->repository->deactivate($data, $this->employee->id);
        Event::assertDispatched(Deactivated::class);
    }

    public function testTokenShouldBeRevokeOnceEmployeeIsDeactivated(): void
    {
        $user = $this->employee->user;
        $token = $user->createToken('test')->token->toArray();
        $data = History::factory()->raw(['url' => $this->faker->url]);
        $this->repository->deactivate($data, $this->employee->id);
        $token['revoked'] = 1;
        $this->assertDatabaseHas('oauth_access_tokens', Arr::only($token, ['id', 'user_id', 'client_id', 'name', 'revoked', 'created_at']));
    }

    public function testWhenEmployeeIsDeactivatedProjectShouldBeUnlicensed(): void
    {
        $projects = Project::factory()->forClient()->count(2)->create();
        $role = ProjectRole::factory()->create();
        foreach ($projects as $project) {
            $project->employees()->syncWithoutDetaching([$this->employee->id => ['role_id' => $role->id, 'licensed' => true]]);
        }

        $data = History::factory()->raw(['url' => $this->faker->url]);
        $this->repository->deactivate($data, $this->employee->id);
        foreach ($projects as $project) {
            $this->assertDatabaseHas('project_employees', [
                'employee_id' => $this->employee->id,
                'project_id' => $project->id,
                'role_id' => $role->id,
                'licensed' => false
            ]);
        }
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HistoryRepository(new Employee(), new History());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }
}
