<?php


namespace Employee\History;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\History;
use App\Modules\Employees\Repositories\HistoryRepository;
use App\System\Employee\Database\Models\History as SystemHistory;
use App\System\User\Database\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use NicoSystem\Exceptions\NicoBadRequestException;
use Tests\TestCase;

class ActivateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\HistoryRepository
     */
    private HistoryRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new HistoryRepository(new Employee(), new History());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testEmployeeShouldBeActivated(): void
    {
        $data = History::factory()->raw(['url' => $this->faker->url]);
        $this->repository->deactivate($data, $this->employee->id);
        $this->repository->activate($data, $this->employee->id);
        $data['status'] = Employee::STATUS_ACTIVE;
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('employee_histories', Arr::only($data, ['date', 'status', 'comment', 'employee_id']));
        $this->assertEquals($data['status'], $this->employee->user->status);
        $this->assertEquals(User::STATUS_LOCKED, $this->employee->user->is_locked);
        $this->assertDatabaseHas('employees', [
            'id' => $this->employee->id,
            'joined_at' => Carbon::now()->format('Y-m-d'),
            'detached_at' => null,
        ]);
        $this->assertDatabaseHas('documents', [
            'url' => $data['url'],
            'documentary_id' => $this->employee->histories()->get()->last()->id,
            'documentary_type' => 'History',
        ]);
    }

    public function testEmployeeShouldNotBeActivatedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $data = History::factory()->raw();
        $this->repository->activate($data, random_int(1000, 2000));
    }

    public function testEmployeeShouldNotBeActivatedForAlreadyActivatedEmployee(): void
    {
        $this->expectException(NicoBadRequestException::class);
        $data = History::factory()->raw();
        $this->repository->activate($data, $this->employee->id);
    }
}
