<?php


namespace Employee\Address;


use App\Modules\Employees\Database\Models\Address;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\AddressRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AddressDetailUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\AddressRepository
     */
    private AddressRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AddressRepository(new Employee(), new Address());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testAddressDetailShouldFetch(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $result = $this->repository->getAddressById($this->employee->id, $address->id);
        $this->assertInstanceOf(\App\System\Common\Database\Models\Address::class, $result);
        $this->assertEquals($address->id, $result->id);
        $this->assertEquals($address->country, $result->country);
        $this->assertEquals($address->state, $result->state);
        $this->assertEquals($address->city, $result->city);
        $this->assertEquals($address->street, $result->street);
        $this->assertEquals($address->zip_code, $result->zip_code);
        $this->assertEquals($address->type, $result->type);
    }

    public function testAddressDetailShouldNotFetchForUnknownEmployee(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getAddressById(random_int(10000, 20000), $address->id);
    }

    public function testAddressDetailShouldNotFetchForUnknownAddress(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getAddressById($this->employee->id, random_int(10000, 20000));
    }

    public function testAddressListShouldBeFetched(): void
    {
        Address::factory()->count(2)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $result = $this->repository->getAddressList([], $this->employee->id);
        $this->assertCount(2, $result);
    }

    public function testAddressListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getAddressList([], random_int(1000, 2000));
    }

    public function testAddressListAreSortedAndOrdered(): void
    {
        $addresses = Address::factory()->count(2)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $addresses = $addresses->sortByDesc('type')->pluck('country');
        $result = $this->repository->getAddressList(['sort_by' => 'type', 'sort_order' => 'desc'], $this->employee->id);
        $result = $result->getCollection()->pluck('country');
        $this->assertEquals($addresses, $result);
    }

    public function testAddressListIsFilteredByStatus(): void
    {
        Address::factory()->count(1)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        Address::factory()->count(1)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee', 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getAddressList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->employee->id);

        $this->assertCount(1, $result);
    }

    public function testAddressListEmptyWithWrongParameters(): void
    {
        Address::factory()->count(1)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        Address::factory()->count(1)->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee', 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getAddressList(['status' => 'xyz', 'keyword' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
