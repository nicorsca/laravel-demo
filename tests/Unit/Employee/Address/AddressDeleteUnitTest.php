<?php


namespace Employee\Address;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\AddressRepository;
use App\Modules\Employees\Database\Models\Address;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AddressDeleteUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\AddressRepository
     */
    private AddressRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AddressRepository(new Employee(), new Address());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testAddressShouldDelete(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $result = $this->repository->destroyAddress($this->employee->id, $address->id);
        $this->assertTrue($result);
        $this->assertNotNull($address->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testAddressShouldNotDeleteForUnknownEmployee(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyAddress(random_int(10000, 20000), $address->id);
    }

    public function testAddressShouldNotDeleteForUnknownAddress(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyAddress($this->employee->id, random_int(10000, 20000));
    }
}
