<?php


namespace Employee\AdditionalInfo;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\ContactRepository;
use App\System\Common\Database\Models\Contact;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ContactUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\ContactRepository
     */
    private ContactRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ContactRepository(new Employee(), new \App\Modules\Employees\Database\Models\Contact());
    }

    public function testEmployeeContactsShouldCreateWhenIdNotPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $data = Contact::factory()->count(2)->raw();
        $this->repository->updateContacts($employee->id, $data);
        foreach ($data as $contact) {
            $contact['contactable_id'] = $employee->id;
            $contact['contactable_type'] = 'Employee';
            $this->assertDatabaseHas('contacts', $contact);
        }
    }

    public function testEmployeeContactsShouldUpdateWhenIdPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $contact = $employee->contacts()->create(Contact::factory()->raw());
        $data = Contact::factory()->count(2)->raw();
        $data[0]['id'] = $contact->id;
        $this->repository->updateContacts($employee->id, $data);
        $updatedContact = $contact->fresh();
        $this->assertEquals($data[0]['id'], $updatedContact->id);
        $this->assertEquals($data[0]['number'], $updatedContact->number);
        $this->assertEquals($data[0]['type'], $updatedContact->type);
        $this->assertEquals($data[0]['status'], $updatedContact->status);
    }

    public function testEmployeeContactsShouldDeleteWhenExistingContactIdNotPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $contact = $employee->contacts()->create(Contact::factory()->raw());
        $data = Contact::factory()->count(2)->raw();
        $this->repository->updateContacts($employee->id, $data);
        $this->assertNotNull($contact->fresh()->deleted_at);
    }

    public function testEmployeeContactShouldDeleteWhenContactsIsEmpty(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $contact = $employee->contacts()->create(Contact::factory()->raw());
        $data = [];
        $this->repository->updateContacts($employee->id, $data);
        $this->assertNotNull($contact->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testEmployeeContactShouldNotUpdateForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $data = Contact::factory()->raw();
        $this->repository->updateContacts(random_int(1000, 2000), $data);
    }
}
