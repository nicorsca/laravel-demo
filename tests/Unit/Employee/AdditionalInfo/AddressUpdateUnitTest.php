<?php


namespace Employee\AdditionalInfo;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\AddressRepository;
use App\System\Common\Database\Models\Address;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AddressUpdateUnitTest extends TestCase
{
    /**
     * @var AddressRepository
     */
    private AddressRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AddressRepository(new Employee(), new \App\Modules\Employees\Database\Models\Address());
    }

    public function testEmployeeAddressesShouldCreateWhenIdNotPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $data = Address::factory()->count(2)->raw();
        $this->repository->updateAddresses($employee->id, $data);
        foreach ($data as $address) {
            $address['addressable_id'] = $employee->id;
            $address['addressable_type'] = 'Employee';
            $this->assertDatabaseHas('addresses', $address);
        }
    }

    public function testEmployeeAddressesShouldUpdateWhenIdPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $address = $employee->addresses()->create(Address::factory()->raw());
        $data = Address::factory()->count(2)->raw();
        $data[0]['id'] = $address->id;
        $this->repository->updateAddresses($employee->id, $data);
        $updatedAddress = $address->fresh();
        $this->assertEquals($data[0]['id'], $updatedAddress->id);
        $this->assertEquals($data[0]['country'], $updatedAddress->country);
        $this->assertEquals($data[0]['state'], $updatedAddress->state);
        $this->assertEquals($data[0]['city'], $updatedAddress->city);
        $this->assertEquals($data[0]['street'], $updatedAddress->street);
        $this->assertEquals($data[0]['zip_code'], $updatedAddress->zip_code);
        $this->assertEquals($data[0]['type'], $updatedAddress->type);
        $this->assertEquals($data[0]['status'], $updatedAddress->status);
    }

    public function testEmployeeAddressesShouldDeleteWhenExistingAddressIdNotPresent(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $address = $employee->addresses()->create(Address::factory()->raw());
        $data = Address::factory()->count(2)->raw();
        $this->repository->updateAddresses($employee->id, $data);
        $this->assertNotNull($address->fresh()->deleted_at);
    }

    public function testEmployeeAddressShouldDeleteWhenAddressesIsEmpty(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $address = $employee->addresses()->create(Address::factory()->raw());
        $data = [];
        $this->repository->updateAddresses($employee->id, $data);
        $this->assertNotNull($address->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testEmployeeAddressShouldNotUpdateForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $data = Address::factory()->raw();
        $this->repository->updateAddresses(random_int(1000, 2000), $data);
    }
}
