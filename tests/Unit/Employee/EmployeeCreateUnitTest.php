<?php


namespace Employee;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Database\Models\User;
use App\Modules\Employees\Mails\EmployeeInvitationMail;
use App\Modules\Employees\Repositories\EmployeeRepository;
use App\System\Department\Database\Models\Department as SystemDepartment;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class EmployeeCreateUnitTest extends TestCase
{

    /**
     * @var EmployeeRepository
     */
    protected EmployeeRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new EmployeeRepository(new Employee(), new User(), new Setting());
    }

    public function testEmployeeShouldCreateIfUserDoesNotExistsInUserTable(): void
    {
        $data = User::factory()->raw();
        $data += Employee::factory()->raw();
        $data['department_id'] = SystemDepartment::factory()->create()->id;
        $this->repository->create($data);
        $this->assertDatabaseHas('users', [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'email' => $data['email'],
            'email_verified_at' => null,
            'status' => User::STATUS_ACTIVE,
            'is_locked' => User::STATUS_LOCKED,
        ]);
        $date = Carbon::parse($data['joined_at']);
        $this->assertDatabaseHas('employees', [
            'code' => 'EN' . $date->year . '01',
            'personal_email' => $data['personal_email'],
            'position' => $data['position'],
            'joined_at' => $data['joined_at'],
            'detached_at' => null,
            'gender' => $data['gender'],
            'dob' => $data['dob'],
            'marital_status' => $data['marital_status'],
            'pan_no' => $data['pan_no'],
            'department_id' => $data['department_id']
        ]);
        $this->assertDatabaseHas('employee_histories', [
            'date' => $data['joined_at'],
            'status' => User::STATUS_ACTIVE,
            'comment' => trans('responses.employee.history.created_comment'),
        ]);
    }

    public function testInvitationMailShouldBeSentToEmployee(): void
    {
        Mail::fake();
        $data = User::factory()->raw();
        $data += Employee::factory()->raw();
        $data['department_id'] = SystemDepartment::factory()->create()->id;
        $this->repository->create($data);
        Mail::assertQueued(EmployeeInvitationMail::class);
    }

    public function testEmployeeCodeShouldNotRepeat(): void
    {
        $date = Carbon::yesterday();
        $employee = Employee::factory()->forUser()->forDepartment()->create([
            'joined_at' => $date->format('Y-m-d'),
            'code' => 'EN' . $date->year . '01'
        ]);
        $data = User::factory()->raw();
        $data += Employee::factory()->raw([
            'joined_at' => $employee->joined_at
        ]);
        $data['department_id'] = SystemDepartment::factory()->create()->id;
        $this->repository->create($data);
        $this->assertDatabaseHas('employees', [
            'code' => 'EN' . $date->year . '02',
            'joined_at' => $data['joined_at'],
        ]);
    }

    public function testUserPreferencesShouldBeAddedDuringEmployeeCreate(): void
    {
        $data = User::factory()->raw();
        $data += Employee::factory()->raw();
        $data['department_id'] = SystemDepartment::factory()->create()->id;
        $result = $this->repository->create($data);
        $settings = Setting::whereIn('key', UserPreferenceKey::options())->get(['key', 'value', 'category'])->toArray();

        foreach ($settings as $setting) {
            $setting['user_id'] = $result->user->id;
            $this->assertDatabaseHas('user_preferences', $setting);
        }

    }
}
