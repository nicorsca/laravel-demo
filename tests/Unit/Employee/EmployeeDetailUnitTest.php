<?php


namespace Employee;


use App\Modules\Employees\Database\Models\EmployeeView;
use App\Modules\Employees\Repositories\EmployeeViewRepository;
use App\System\Common\Database\Models\Address;
use App\System\Common\Database\Models\Bank;
use App\System\Common\Database\Models\Contact;
use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class EmployeeDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\EmployeeViewRepository
     */
    private EmployeeViewRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new EmployeeViewRepository(new EmployeeView());
    }

    public function testEmployeeListShouldBeFetched(): void
    {
        Employee::factory()->forUser()->forDepartment()->count(5)->create();
        $result = $this->repository->getList([]);
        $this->assertCount(5, $result);
    }

    public function testEmployeeListIsFilteredByStatus(): void
    {
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_INACTIVE]))->forDepartment()->count(3)->create();
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->count(2)->create();
        $result = $this->repository->getList(['status' => User::STATUS_ACTIVE, 'keyword' => '']);

        $this->assertCount(2, $result);
    }

    public function testEmployeeListIsFilteredByName(): void
    {
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_INACTIVE]))->forDepartment()->count(3)->create();
        Employee::factory()->for(User::factory()->state(['first_name' => 'first', 'middle_name' => 'middle', 'last_name' => 'last', 'status' => User::STATUS_ACTIVE]))->forDepartment()->count(2)->create();
        $result = $this->repository->getList(['name' => 'first middle']);

        $this->assertCount(2, $result);
    }

    public function testEmployeeListIsFilteredByDepartment(): void
    {
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_INACTIVE]))->forDepartment(['title' => 'test department'])->count(3)->create();
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->count(2)->create();
        $result = $this->repository->getList(['department' => 'test department']);

        $this->assertCount(3, $result);
    }

    public function testEmployeeListIsFilteredByDepartmentId(): void
    {
        $department = Department::factory()->create();
        Employee::factory()->state(['department_id' => $department->id])->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->count(3)->create();
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->count(2)->create();
        $result = $this->repository->getList(['department_id' => $department->id]);

        $this->assertCount(3, $result);
    }

    public function testEmployeeListIsFilteredByCode(): void
    {
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_INACTIVE]))->forDepartment()->count(3)->create();
        $employee = Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->create();
        $result = $this->repository->getList(['code' => $employee->code]);

        $this->assertCount(1, $result);
    }

    public function testEmployeeListEmptyWithWrongParameters(): void
    {
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_INACTIVE]))->forDepartment()->count(3)->create();
        Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->count(2)->create();
        $result = $this->repository->getList(['status' => 'xyz', 'keyword' => 'xyz']);

        $this->assertCount(0, $result);
    }

    public function testEmployeeDetailShouldFetch(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $employee->addresses()->createMany(Address::factory()->count(2)->raw());
        $employee->contacts()->createMany(Contact::factory()->count(2)->raw());
        $bank = $employee->bank()->create(Bank::factory()->raw())->toArray();
        $result = $this->repository->getById($employee->id);
        $this->assertCount(2, $result->addresses);
        $this->assertCount(2, $result->contacts);
        $this->assertEquals($bank, $result->bank->toArray());
    }

    public function testEmployeeDetailShouldNotFetchForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }
}
