<?php


namespace Employee\Bank;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\BankRepository;
use App\System\Common\Database\Models\Bank;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class BankDeleteUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\BankRepository
     */
    private BankRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new BankRepository(new Employee(), new \App\Modules\Employees\Database\Models\Bank());
    }

    public function testBankShouldDelete(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $this->repository->destroyBank($employee->id, $bank->id);
        $this->assertNotNull($bank->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testBankShouldNotDeleteForUnknownEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyBank(random_int(10000, 20000), $bank->id);
    }

    public function testBankShouldNotDeleteForUnknownBank(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyBank($employee->id, random_int(10000, 20000));
    }
}
