<?php


namespace Employee\Bank;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\BankRepository;
use App\System\Common\Database\Models\Bank;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class BankUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\BankRepository
     */
    private BankRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new BankRepository(new Employee(), new \App\Modules\Employees\Database\Models\Bank());
    }

    public function testBankShouldUpdate(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $data = Bank::factory()->raw();
        $this->repository->updateBank($data, $employee->id, $bank->id);
        $data['bankable_id'] = $employee->id;
        $data['bankable_type'] = 'Employee';
        $this->assertDatabaseHas('banks', $data);
    }

    public function testBankShouldNotUpdateForUnknownEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateBank([], random_int(10000, 20000), $bank->id);
    }

    public function testBankShouldNotUpdateForUnknownBank(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateBank([], $employee->id, random_int(10000, 20000));
    }

    public function testBankShouldToggleStatus(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $result = $this->repository->toggleBankStatus($employee->id, $bank->id);
        $this->assertInstanceOf(Bank::class, $result);
        $this->assertEquals(Status::STATUS_UNPUBLISHED, $result->status);
    }

    public function testBankShouldNotToggleStatusForUnknownEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $bank = $employee->bank()->create(Bank::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleBankStatus(random_int(10000, 20000), $bank->id);
    }

    public function testBankShouldNotToggleStatusForUnknownBank(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleBankStatus($employee->id, random_int(10000, 20000));
    }
}
