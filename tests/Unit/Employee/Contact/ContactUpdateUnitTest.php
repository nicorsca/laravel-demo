<?php


namespace Employee\Contact;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\ContactRepository;
use App\Modules\Employees\Database\Models\Contact;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class ContactUpdateUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\ContactRepository
     */
    private ContactRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ContactRepository(new Employee(), new Contact());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }


    public function testContactShouldUpdate(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $data = Contact::factory()->raw();
        $result = $this->repository->updateContact($data, $this->employee->id, $contact->id);
        $this->assertInstanceOf(Contact::class, $result);
        $data['contactable_id'] = $this->employee->id;
        $data['contactable_type'] = 'Employee';
        $this->assertDatabaseHas('contacts', $data);
    }

    public function testContactShouldNotUpdateForUnknownEmployee(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateContact([], random_int(10000, 20000), $contact->id);
    }

    public function testContactShouldNotUpdateForUnknownContact(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateContact([], $this->employee->id, random_int(10000, 20000));
    }

    public function testContactShouldToggleStatus(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $result = $this->repository->toggleContactStatus($this->employee->id, $contact->id);
        $this->assertInstanceOf(Contact::class, $result);
        $this->assertEquals(Status::STATUS_UNPUBLISHED, $result->status);
    }

    public function testContactShouldNotToggleStatusForUnknownEmployee(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleContactStatus(random_int(10000, 20000), $contact->id);
    }

    public function testContactShouldNotToggleStatusForUnknownContact(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleContactStatus($this->employee->id, random_int(10000, 20000));
    }
}
