<?php


namespace Employee\Contact;


use App\Modules\Employees\Database\Models\Contact;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\ContactRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ContactDeleteUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\ContactRepository
     */
    private ContactRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ContactRepository(new Employee(), new Contact());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testContactShouldDelete(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $result = $this->repository->destroyContact($this->employee->id, $contact->id);
        $this->assertTrue($result);
        $this->assertNotNull($contact->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testContactShouldNotDeleteForUnknownEmployee(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyContact(random_int(10000, 20000), $contact->id);
    }

    public function testContactShouldNotDeleteForUnknownContact(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyContact($this->employee->id, random_int(10000, 20000));
    }
}
