<?php


namespace Employee\Contact;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Database\Models\Contact;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\ContactRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ContactCreateUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\ContactRepository
     */
    private ContactRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ContactRepository(new Employee(), new Contact());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testContactShouldBeCreated(): void
    {
        $data = Contact::factory()->raw();
        $result = $this->repository->createContact($data, $this->employee->id);
        $this->assertInstanceOf(Contact::class, $result);
        $data['contactable_id'] = $this->employee->id;
        $data['contactable_type'] = 'Employee';
        $this->assertDatabaseHas('contacts', $data);
    }

    /**
     * @throws \Exception
     */
    public function testCreateContactShouldNotCreateForUnknownEmployee(): void
    {
        $data = Contact::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createContact($data, random_int(1000, 2000));
    }

}
