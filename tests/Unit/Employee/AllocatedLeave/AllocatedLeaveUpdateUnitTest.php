<?php


namespace Employee\AllocatedLeave;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Repositories\AllocatedLeaveRepository;
use App\System\Employee\Database\Models\AllocatedLeave;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AllocatedLeaveUpdateUnitTest extends TestCase
{
    use FiscalYearTrait;

    /**
     * @var \App\Modules\Employees\Repositories\AllocatedLeaveRepository
     */
    private AllocatedLeaveRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Database\Models\Setting
     */
    private Setting $setting;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setting = new Setting();
        $this->repository = new AllocatedLeaveRepository(new Employee(), new \App\Modules\Employees\Database\Models\AllocatedLeave(), new Setting());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testLeaveShouldUpdateForUnpublishedCurrentFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $data = AllocatedLeave::factory()->raw();
        $result = $this->repository->updateLeave($data, $this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $data['id'] = $leave->id;
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('allocated_leaves', $data);
    }

    public function testLeaveShouldUpdateForPublishedFutureFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate(false);
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $data = AllocatedLeave::factory()->raw();
        $result = $this->repository->updateLeave(Arr::only($data, ['title', 'type', 'days', 'status']), $this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $data['id'] = $leave->id;
        $data['employee_id'] = $this->employee->id;
        $data = array_merge($data, $fiscalDates);
        $this->assertDatabaseHas('allocated_leaves', $data);
    }

    public function testLeaveShouldUpdateForUnpublishedPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $data = AllocatedLeave::factory()->raw();
        $result = $this->repository->updateLeave(Arr::only($data, ['title', 'type', 'days', 'status']), $this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $data['id'] = $leave->id;
        $data['employee_id'] = $this->employee->id;
        $data = array_merge($data, $fiscalDates);
        $this->assertDatabaseHas('allocated_leaves', $data);
    }

    public function testLeaveShouldNotUpdateForPublishedCurrentFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_PUBLISHED]);
        $data = AllocatedLeave::factory()->raw();
        $this->expectException(NicoBadRequestException::class);
        $this->repository->updateLeave($data, $this->employee->id, $leave->id);
    }

    public function testLeaveShouldNotUpdateForPublishedPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $data = AllocatedLeave::factory()->raw();
        $this->expectException(NicoBadRequestException::class);
        $this->repository->updateLeave(Arr::only($data, ['title', 'type', 'days', 'status']), $this->employee->id, $leave->id);
    }

    public function testLeaveShouldNotUpdateForUnknownEmployee(): void
    {
        $leave = AllocatedLeave::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateLeave([], random_int(10000, 20000), $leave->id);
    }

    public function testLeaveShouldNotUpdateForUnknownLeave(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateLeave([], $this->employee->id, random_int(10000, 20000));
    }

    public function testLeaveShouldToggleStatusForUnpublishedCurrentFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->toggleLeaveStatus($this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
    }

    public function testLeaveShouldToggleStatusForPublishedFutureFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate(false);
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $result = $this->repository->toggleLeaveStatus($this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $this->assertEquals(Status::STATUS_UNPUBLISHED, $result->status);
    }

    public function testLeaveShouldToggleStatusForUnpublishedPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->toggleLeaveStatus($this->employee->id, $leave->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
    }

    public function testLeaveShouldNotToggleStatusForPublishedCurrentFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_PUBLISHED]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->toggleLeaveStatus($this->employee->id, $leave->id);
    }

    public function testLeaveShouldNotToggleStatusForPublishedPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->toggleLeaveStatus($this->employee->id, $leave->id);
    }

    public function testLeaveShouldNotToggleStatusForUnknownEmployee(): void
    {
        $leave = AllocatedLeave::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleLeaveStatus(random_int(10000, 20000), $leave->id);
    }

    public function testLeaveShouldNotToggleStatusForUnknownLeave(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleLeaveStatus($this->employee->id, random_int(10000, 20000));
    }
}
