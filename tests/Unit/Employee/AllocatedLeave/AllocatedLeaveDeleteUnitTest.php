<?php


namespace Employee\AllocatedLeave;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Repositories\AllocatedLeaveRepository;
use App\System\Employee\Database\Models\AllocatedLeave;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AllocatedLeaveDeleteUnitTest extends TestCase
{
    use FiscalYearTrait;

    /**
     * @var \App\Modules\Employees\Repositories\AllocatedLeaveRepository
     */
    private AllocatedLeaveRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Database\Models\Setting
     */
    private Setting $setting;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setting = new Setting();
        $this->repository = new AllocatedLeaveRepository(new Employee(), new \App\Modules\Employees\Database\Models\AllocatedLeave(), new Setting());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testLeaveShouldDeleteForNextFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate(false);
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $this->repository->destroyLeave($this->employee->id, $leave->id);
        $this->assertNotNull($leave->fresh()->deleted_at);
    }

    public function testAllocatedLeaveShouldDeleteForCurrentFiscalYearUnpublishedAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate(true);
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $this->repository->destroyLeave($this->employee->id, $leave->id);
        $this->assertNotNull($leave->fresh()->deleted_at);
    }

    public function testAllocatedLeaveShouldBeDeletedForUnpublishedAndPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $this->repository->destroyLeave($this->employee->id, $leave->id);
        $this->assertNotNull($leave->fresh()->deleted_at);
    }

    public function testAllocatedLeaveShouldNotBeDeleteForPublishedCurrentFiscalYear(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->destroyLeave($this->employee->id, $leave->id);
    }

    public function testAllocatedLeaveShouldNotBeDeletedForPastFiscalYearAllocatedLeave(): void
    {
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $fiscalDates['start_date'] = Carbon::parse($fiscalDates['start_date'])->subYear();
        $fiscalDates['end_date'] = Carbon::parse($fiscalDates['end_date'])->subYear();
        $leave = AllocatedLeave::factory()->create($fiscalDates + ['employee_id' => $this->employee->id]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->destroyLeave($this->employee->id, $leave->id);
    }

    /**
     * @throws \Exception
     */
    public function testLeaveShouldNotDeleteForUnknownEmployee(): void
    {
        $leave = AllocatedLeave::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyLeave(random_int(10000, 20000), $leave->id);
    }

    public function testLeaveShouldNotDeleteForUnknownLeave(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyLeave($this->employee->id, random_int(10000, 20000));
    }
}
