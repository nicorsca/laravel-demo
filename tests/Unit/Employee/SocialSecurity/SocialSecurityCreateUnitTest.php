<?php


namespace Employee\SocialSecurity;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\SocialSecurity;
use App\Modules\Employees\Repositories\SocialSecurityRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class SocialSecurityCreateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\SocialSecurityRepository
     */
    private SocialSecurityRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SocialSecurityRepository(new Employee(), new SocialSecurity());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testSocialSecurityShouldBeCreated(): void
    {
        $data = SocialSecurity::factory()->raw();
        $result = $this->repository->createSocialSecurity($data, $this->employee->id);
        $this->assertInstanceOf(SocialSecurity::class, $result);
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('social_securities', $data);
    }

    /**
     * @throws \Exception
     */
    public function testCreateSocialSecurityShouldNotCreateForUnknownEmployee(): void
    {
        $data = SocialSecurity::factory()->raw();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createSocialSecurity($data, random_int(1000, 2000));
    }
}
