<?php


namespace Employee\SocialSecurity;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\SocialSecurity;
use App\Modules\Employees\Repositories\SocialSecurityRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class SocialSecurityDeleteUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\SocialSecurityRepository
     */
    private SocialSecurityRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SocialSecurityRepository(new Employee(), new SocialSecurity());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testSocialSecurityShouldDelete(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $result = $this->repository->destroySocialSecurity($this->employee->id, $socialSecurity->id);
        $this->assertTrue($result);
        $this->assertNotNull($socialSecurity->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testSocialSecurityShouldNotDeleteForUnknownEmployee(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroySocialSecurity(random_int(10000, 20000), $socialSecurity->id);
    }

    public function testSocialSecurityShouldNotDeleteForUnknownSocialSecurity(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroySocialSecurity($this->employee->id, random_int(10000, 20000));
    }
}
