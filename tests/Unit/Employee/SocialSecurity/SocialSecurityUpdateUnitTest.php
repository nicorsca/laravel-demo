<?php


namespace Employee\SocialSecurity;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\SocialSecurity;
use App\Modules\Employees\Repositories\SocialSecurityRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class SocialSecurityUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\SocialSecurityRepository
     */
    private SocialSecurityRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new SocialSecurityRepository(new Employee(), new SocialSecurity());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testSocialSecurityShouldUpdate(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $data = SocialSecurity::factory()->raw();
        $result = $this->repository->updateSocialSecurity($data, $this->employee->id, $socialSecurity->id);
        $this->assertInstanceOf(SocialSecurity::class, $result);
        $data['employee_id'] = $this->employee->id;
        $this->assertDatabaseHas('social_securities', $data);
    }

    public function testSocialSecurityShouldNotUpdateForUnknownEmployee(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateSocialSecurity([], random_int(10000, 20000), $socialSecurity->id);
    }

    public function testSocialSecurityShouldNotUpdateForUnknownSocialSecurity(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateSocialSecurity([], $this->employee->id, random_int(10000, 20000));
    }

    public function testSocialSecurityShouldToggleStatus(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id, 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->toggleSocialSecurityStatus($this->employee->id, $socialSecurity->id);
        $this->assertInstanceOf(SocialSecurity::class, $result);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
    }

    public function testSocialSecurityShouldNotToggleStatusForUnknownEmployee(): void
    {
        $socialSecurity = SocialSecurity::factory()->create(['employee_id' => $this->employee->id]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleSocialSecurityStatus(random_int(10000, 20000), $socialSecurity->id);
    }

    public function testSocialSecurityShouldNotToggleStatusForUnknownSocialSecurity(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleSocialSecurityStatus($this->employee->id, random_int(10000, 20000));
    }
}
