<?php


namespace Employee\Document;


use App\Events\Employee\Document\Updated;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\DocumentRepository;
use App\System\Common\Database\Models\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Event;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class DocumentUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Employee(), new \App\Modules\Employees\Database\Models\Document());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testDocumentShouldUpdate(): void
    {
        Event::fake();
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $data = Document::factory()->raw();
        $result = $this->repository->updateDocument($data, $this->employee->id, $document->id);
        $this->assertInstanceOf(Document::class, $result);
        $data['documentary_id'] = $this->employee->id;
        $data['documentary_type'] = 'Employee';
        $this->assertDatabaseHas('documents', $data);
        Event::assertDispatched(Updated::class);
    }

    public function testDocumentShouldNotUpdateForUnknownEmployee(): void
    {
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateDocument([], random_int(10000, 20000), $document->id);
    }

    public function testDocumentShouldNotUpdateForUnknownDocument(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateDocument([], $this->employee->id, random_int(10000, 20000));
    }

    public function testDocumentShouldToggleStatus(): void
    {
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $result = $this->repository->toggleDocumentStatus($this->employee->id, $document->id);
        $this->assertInstanceOf(Document::class, $result);
        $this->assertEquals(Status::STATUS_UNPUBLISHED, $result->status);
    }

    public function testDocumentShouldNotToggleStatusForUnknownEmployee(): void
    {
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleDocumentStatus(random_int(10000, 20000), $document->id);
    }

    public function testDocumentShouldNotToggleStatusForUnknownDocument(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->toggleDocumentStatus($this->employee->id, random_int(10000, 20000));
    }
}
