<?php


namespace Employee\Document;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\DocumentRepository;
use App\System\Common\Database\Models\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class DocumentDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Employee(), new \App\Modules\Employees\Database\Models\Document());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testDocumentDetailShouldFetch(): void
    {
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $result = $this->repository->getDocumentById($this->employee->id, $document->id);
        $this->assertInstanceOf(Document::class, $result);
        $this->assertEquals($result->id, $document->id);
        $this->assertEquals($result->title, $document->title);
        $this->assertEquals($result->url, $document->url);
        $this->assertEquals($result->thumbnail, $document->thumbnail);
        $this->assertEquals($result->status, $document->status);
    }

    public function testDocumentDetailShouldNotFetchForUnknownEmployee(): void
    {
        $document = $this->employee->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentById(random_int(10000, 20000), $document->id);
    }

    public function testDocumentDetailShouldNotFetchForUnknownDocument(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentById($this->employee->id, random_int(10000, 20000));
    }

    public function testDocumentListShouldBeFetched(): void
    {
        $this->employee->documents()->createMany(Document::factory()->count(5)->raw());
        $result = $this->repository->getDocumentList([], $this->employee->id);
        $this->assertCount(5, $result);
    }

    public function testDocumentListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getDocumentList([], random_int(1000, 2000));
    }

    public function testDocumentListAreSortedAndOrderedAndSearchable(): void
    {
        $documents = $this->employee->documents()->createMany(Document::factory()->count(3)->raw());
        $documents = $documents->sortByDesc('title')->pluck('title');
        $result = $this->repository->getDocumentList(['sort_by' => 'title', 'sort_order' => 'desc'], $this->employee->id);
        $result = $result->getCollection()->pluck('title');
        $this->assertEquals($documents, $result);

        $document = $this->employee->documents()->create(Document::factory()->raw());
        $result = $this->repository->getDocumentList(['keyword' => $document->title], $this->employee->id);
        $this->assertEquals($document->title, $result->first()->title);
    }

    public function testDocumentListIsFilteredByStatus(): void
    {
        $this->employee->documents()->createMany(Document::factory()->count(3)->raw());
        $this->employee->documents()->createMany(Document::factory()->count(2)->raw(['status' => Status::STATUS_UNPUBLISHED]));
        $result = $this->repository->getDocumentList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->employee->id);

        $this->assertCount(3, $result);
    }

    public function testDocumentListEmptyWithWrongParameters(): void
    {
        $this->employee->documents()->createMany(Document::factory()->count(3)->raw());
        $this->employee->documents()->createMany(Document::factory()->count(2)->raw(['status' => Status::STATUS_UNPUBLISHED]));
        $result = $this->repository->getDocumentList(['status' => 'xyz', 'keyword' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
