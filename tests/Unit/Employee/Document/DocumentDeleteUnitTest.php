<?php


namespace Employee\Document;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\DocumentRepository;
use App\System\Common\Database\Models\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class DocumentDeleteUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\DocumentRepository
     */
    private DocumentRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new DocumentRepository(new Employee(), new \App\Modules\Employees\Database\Models\Document());
    }

    public function testDocumentShouldDelete(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $document = $employee->documents()->create(Document::factory()->raw());
        $this->repository->destroyDocument($employee->id, $document->id);
        $this->assertNotNull($document->fresh()->deleted_at);
    }

    /**
     * @throws \Exception
     */
    public function testDocumentShouldNotDeleteForUnknownEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $document = $employee->documents()->create(Document::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyDocument(random_int(10000, 20000), $document->id);
    }

    public function testDocumentShouldNotDeleteForUnknownDocument(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->expectException(ModelNotFoundException::class);
        $this->repository->destroyDocument($employee->id, random_int(10000, 20000));
    }
}
