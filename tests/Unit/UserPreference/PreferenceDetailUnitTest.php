<?php

namespace Tests\Unit\UserPreference;

use App\Modules\UserPreferences\Database\Models\UserPreference;
use App\Modules\UserPreferences\Repositories\UserPreferenceRepository;
use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class PreferenceDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\UserPreferences\Repositories\UserPreferenceRepository
     */
    private UserPreferenceRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private Employee $employee;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new UserPreferenceRepository(new UserPreference());
        $this->employee = Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->create();
        $this->artisan('db:seed --class=UserPreferenceSeeder');
    }

    public function testSettingDetailShouldFetch(): void
    {
        $setting = $this->repository->getUserPreferencesList($this->employee->user_id)->first();

        $result = $this->repository->getById($setting->id);
        $this->assertInstanceOf(UserPreference::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($setting->value, $result->value);
    }

    public function testSettingDetailShouldNotFetchForUnknownSetting(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }

    public function testSettingListShouldBeShown(): void
    {
        $setting = $this->repository->getUserPreferencesList($this->employee->user_id);
        $this->assertInstanceOf(LengthAwarePaginator::class, $setting);
    }
}
