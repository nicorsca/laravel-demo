<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadFileRequest;
use App\Http\Requests\UploadMultipleFileRequest;
use App\System\Foundation\FileUpload\Upload;
use Illuminate\Http\JsonResponse;

class FileController extends Controller
{
    use Upload;

    /**
     * @param \App\Http\Requests\UploadFileRequest $request
     * @return JsonResponse
     */
    public function uploadSingleFile(UploadFileRequest $request): JsonResponse
    {
        return $this->responseOk($this->upload($request));
    }

    /**
     * @param \App\Http\Requests\UploadMultipleFileRequest $request
     * @return JsonResponse
     */
    public function uploadMultipleFiles(UploadMultipleFileRequest $request): JsonResponse
    {
        return $this->responseOk($this->multipleUpload($request));
    }
}
