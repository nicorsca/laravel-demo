<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showLinkRequestForm(): RedirectResponse
    {
        return redirect()->to(get_front_app_url(url('/login')));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response): JsonResponse
    {
        return $this->responseOk(null, trans('auth.reset_link'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response): JsonResponse
    {
        return $this->responseBadRequest($response, turn_to_app_constant_format($response));
    }
}
