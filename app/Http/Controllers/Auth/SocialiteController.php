<?php

namespace App\Http\Controllers\Auth;

use App\Events\User\UserSocialiteLoggedIn;
use App\Events\User\UserSocialiteLogin;
use App\Exceptions\InvalidCallBackException;
use App\Http\Controllers\Controller;
use App\System\AppConstants;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use NicoSystem\Exceptions\NicoAuthenticationException;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Requests\NicoRequest;

/**
 * Class SocialiteController
 * @package App\Http\Controllers\Auth
 */
class SocialiteController extends Controller
{
    /**
     * Redirect the user to the Provider authentication page.
     *
     * @param NicoRequest $request
     * @param string $provider
     * @return RedirectResponse
     */
    public function redirectToProvider(NicoRequest $request, string $provider): RedirectResponse
    {
        try {
            $this->validateRequest($request->all());
            $this->validateProvider($provider);
            $request->session()->put('redirect_uri', $request->get('redirect_uri'));

            return Socialite::driver($provider)->stateless()->redirect();
        } catch (InvalidCallBackException $e) {
            throw $e;
        } catch (NicoBadRequestException | NicoException | \Exception $e) {
            return $this->redirectWebError($request, $e);
        }
    }

    /**
     * @param array $inputs
     */
    private function validateRequest(array $inputs)
    {
        $uriExists = Arr::has($inputs, 'redirect_uri');
        $callbackUris = explode(',', config('app.sso_redirect_uris'));
        if (!$uriExists || !in_array($inputs['redirect_uri'], $callbackUris)) {
            throw new InvalidCallBackException('Invalid redirect uri');
        }
    }

    /**
     * @param $provider
     *
     * @return void
     */
    protected function validateProvider($provider): void
    {
        if (!in_array($provider, ['microsoft', 'google'])) {
            throw new NicoBadRequestException(trans('auth.invalid_provider'), AppConstants::UNPROCESSABLE_ENTITY, null, AppConstants::UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @param $request
     * @param $exception
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectWebError($request, $exception): RedirectResponse
    {
        $redirectUrl = $request->session()->get('redirect_uri');
        $status = $exception->getCode();
        $message = $exception->getMessage() ?? 'Invalid request';
        $respCode = AppConstants::getAppMsgCodeFromStatusCode($exception->getCode());

        return redirect()->away($redirectUrl . "?status={$status}&respCode={$respCode}&message={$message}");
    }

    /**
     * Obtain the user information from Provider.
     *
     * @param NicoRequest $request
     * @param string $provider
     * @return RedirectResponse
     */
    public function handleProviderCallback(NicoRequest $request, string $provider): RedirectResponse
    {
        $redirectUrl = $request->session()->pull('redirect_uri');
        try {
            $this->validateProvider($provider);
            $authUser = Socialite::driver($provider)->stateless()->user();
            $user = event(new UserSocialiteLogin($provider, $authUser))[0];
            $request->session()->forget('redirect_uri');
            $accessToken = $user->createToken(Str::random(8))->accessToken;
            $tokenType = 'Bearer';
            $expiresIn = config('auth.passport_access_token_expire_in_seconds');
            $params = "?access_token={$accessToken}&token_type={$tokenType}&expires_in={$expiresIn}";
            event(new UserSocialiteLoggedIn('users.sso.' . $provider, $user));

            return redirect()->away($redirectUrl . $params);
        } catch (ModelNotFoundException $e) {
            return redirect()->away($redirectUrl . "?status={$e->getCode()}&respCode='" . AppConstants::ERR_USER_NOT_REGISTERED . "'&message=" . trans('appconstant.user_not_registered'));
        } catch (ClientException $e) {
            $exception = new NicoAuthenticationException($e->getMessage());
            return redirect()->away($redirectUrl . "?status={$exception->getCode()}&respCode={$exception->getResponseCode()}&message={$exception->getMessage()}");
        } catch (NicoBadRequestException | NicoException $e) {
            return redirect()->away($redirectUrl . "?status={$e->getCode()}&respCode={$e->getResponseCode()}&message={$e->getMessage()}");
        } catch (\Exception $e) {
            return $this->redirectWebError($request, $e);
        }
    }
}
