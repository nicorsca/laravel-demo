<?php

namespace App\Http\Controllers\Auth;

use App\Events\User\UserLoginLogout;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $user = Auth::user();
        $user->token()->revoke();

        event(new UserLoginLogout(UserLoginLogout::USER_LOGOUT));

        return $this->responseOk(null);
    }
}
