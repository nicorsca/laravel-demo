<?php

namespace App\Http\Controllers\Auth;

use App\Events\User\UserLoginLogout;
use App\System\AppConstants;
use App\System\User\Database\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use NicoSystem\Foundation\NicoResponseTraits;
use Psr\Http\Message\ServerRequestInterface;

class LoginController extends AccessTokenController
{
    use NicoResponseTraits, AuthenticatesUsers;

    /**
     * password grant type
     */
    const GRANT_TYPE_PASSWORD = 'password';

    /**
     *Authenticate User with check multiple Login attempts
     *
     * @param ServerRequestInterface $request
     * @return JsonResponse
     */
    public function authenticate(ServerRequestInterface $request): JsonResponse
    {
        //validate input
        $rule = ['username' => 'required|email', 'password' => 'required'];
        $validator = Validator::make($request->getParsedBody(), $rule);
        if ($validator->fails()) {
            return $this->responseValidationError($validator->messages());
        }

        $user = User::whereEmail($request->getParsedBody()['username'])->whereStatus(User::STATUS_INACTIVE)->first();
        if ($user) {
            return $this->responseUnAuthorize(trans('auth.unauthorized'), AppConstants::ERR_INVALID_CREDENTIAL);
        }

        //convert PSR-7 Request  to Laravel (Symfony Request)
        $httpRequest = new Request($request->getParsedBody());

        //check if user has reached max Login attempts
        if ($this->hasTooManyLoginAttempts($httpRequest)) {
            $this->fireLockoutEvent($httpRequest);

            return $this->responseTooManyAttempts(trans('auth.multiple_attempts'), AppConstants::ERR_TOO_MANY_LOGIN_ATTEMPT);
        }

        $credentials = [
            'email' => $httpRequest->username,
            'password' => $httpRequest->password,
        ];

        if (Auth::attempt($credentials)) {
            //Authentication passed...

            //reset failed login attemps
            $this->clearLoginAttempts($httpRequest);

            $request = $request->withParsedBody(
                array_merge(
                    $request->getParsedBody(),
                    $this->getAuthParamsForClient(static::GRANT_TYPE_PASSWORD))
            );
            $response = $this->issueToken($request);

            return $this->parseResultForResponse($response);
        } else {
            //count user failed login attempts
            $this->incrementLoginAttempts($httpRequest);

            return $this->responseUnAuthorize(trans('auth.unauthorized'), AppConstants::ERR_INVALID_CREDENTIAL);
        }
    }

    /**
     * @param $grantType
     * @return array
     */
    protected function getAuthParamsForClient($grantType): array
    {
        return [
            'grant_type' => $grantType,
            'client_id' => config('baseclient.id'),
            'client_secret' => config('baseclient.secret')
        ];
    }

    /**
     * @param Response $response
     * @return JsonResponse
     */
    protected function parseResultForResponse(Response $response): JsonResponse
    {
        if ($response->getStatusCode() == 401) {
            //invalid username password issue
            $message = json_decode($response->getContent());

            if ($message->error == AppConstants::ERR_INVALID_CREDENTIAL ||
                $message->error == AppConstants::ERR_TOO_MANY_LOGIN_ATTEMPT) {
                return $this->responseUnAuthorize($message->message, $message->error);
            } else {
                return $this->responseBadRequest($message->message, $message->error);
            }

        } elseif ($response->getStatusCode() == 200) {
            $user = Auth::user();
            if (!$user->email_verified_at) {
                $user->email_verified_at = now();
                $user->save();
            }
            event(new UserLoginLogout(UserLoginLogout::USER_LOGIN));

            $content = json_decode($response->getContent());

            return $this->responseOk($user, 'ok', 'ok', [
                'Access-Token' => $content->access_token,
                'Refresh-Token' => $content->refresh_token,
                'Token-Type' => $content->token_type,
                'Expires-In' => $content->expires_in,
            ]);
        }

        return $this->responseServerError($response->getContent());
    }
}
