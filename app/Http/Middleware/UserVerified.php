<?php

namespace App\Http\Middleware;

use App\Exceptions\UserNotRegisteredException;
use Closure;
use Illuminate\Http\Request;

class UserVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user('api')->email_verified_at == null) {
            throw new UserNotRegisteredException();
        }
        return $next($request);
    }
}
