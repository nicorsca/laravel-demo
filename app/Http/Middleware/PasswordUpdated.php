<?php
/**
 * Created by PhpStorm.
 * User: nicore
 * Date: 11/30/18
 * Time: 11:32 PM
 */

namespace App\Http\Middleware;

use App\Exceptions\PasswordNotUpdatedException;
use Illuminate\Http\Request;

class PasswordUpdated
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        if ($request->user('api')->is_locked) {
            throw new PasswordNotUpdatedException(trans('responses.user.is_locked'));
        }
        return $next ($request);
    }
}
