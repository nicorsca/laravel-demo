<?php

namespace App\Http\Middleware;

use App\System\Foundation\Scope\SystemQueryScope;
use App\System\User\Database\Models\User;
use App\System\User\Foundation\RoleName;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleScope
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse
    {
        $roleName = $request->user()->role_name;
        if ($roleName === RoleName::EMPLOYEE) {
            $this->addEmployeeScope($request->user());
            $this->addEmployeeRelationScope($request->user());
        }
        return $next($request);
    }

    /**
     * @param User $user
     */
    protected function addEmployeeScope(User $user): void
    {
        $query = SystemQueryScope::getQueryInstance("employees");
        $query->addWhere('id', '=', $user->employee->id);
    }

    /**
     * @param User $user
     */
    protected function addEmployeeRelationScope(User $user): void
    {
        $query = SystemQueryScope::getQueryInstance("employee_relation");
        $query->addWhere('employee_id', '=', $user->employee->id);
    }

}
