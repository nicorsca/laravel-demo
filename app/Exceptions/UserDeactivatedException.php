<?php

namespace App\Exceptions;

use NicoSystem\Exceptions\NicoException;

class UserDeactivatedException extends NicoException
{
    protected $code = 403;

    protected string $respCode = "user_deactivated";
}
