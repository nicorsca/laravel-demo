<?php

namespace App\Exceptions;

use Exception;
use NicoSystem\Exceptions\NicoException;

class ResourceExistsException extends NicoException
{
    protected $code = 409;

    protected string $respCode = "resource_already_exists";
}
