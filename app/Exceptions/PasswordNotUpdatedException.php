<?php
/**
 * Created by PhpStorm.
 * User: nicore
 * Date: 12/1/18
 * Time: 12:21 AM
 */

namespace App\Exceptions;


use NicoSystem\Exceptions\NicoException;

class PasswordNotUpdatedException extends NicoException
{
    protected $code = 300;

    protected string $respCode = "password_not_updated";
}
