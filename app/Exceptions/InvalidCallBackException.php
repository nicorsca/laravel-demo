<?php

namespace App\Exceptions;

use NicoSystem\Exceptions\NicoException;

class InvalidCallBackException extends NicoException
{

    protected $code = 405;

    protected string $respCode = 'invalid_callback_url';
}
