<?php

/**
 * @OA\Get(
 *     path="/api/clients",
 *     summary="Fetch list of clients",
 *     tags={"Clients"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"name", "created_at"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by name",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ClientListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/clients/{client_id}",
 *     summary="Get detail of a client",
 *     tags={"Clients"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="client_id",
 *         in="path",
 *         description="ID of a client",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ClientModel")
 *     )
 * )
 */

/**
 * @OA\Post (
 *     path="/api/clients",
 *     summary="Create new client",
 *     tags={"Clients"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ClientCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ClientModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/clients/{client_id}",
 *     summary="Update a client",
 *     tags={"Clients"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="client_id",
 *         in="path",
 *         description="ID of a client",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ClientCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ClientModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/clients/{client_id}",
 *     summary="Delete a Client",
 *     tags={"Clients"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="client_id",
 *         in="path",
 *         description="ID of a Client",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */
