<?php
/**
 * @OA\Get(
 *     path="/api/settings",
 *     summary="Fetch list of system settings",
 *     tags={"System Settings"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"key"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by key",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="category",
 *         in="query",
 *         description="filter by category",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SettingListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/settings/{setting_id}",
 *     summary="Get detail of a setting",
 *     tags={"System Settings"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="setting_id",
 *         in="path",
 *         description="ID of a setting",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SettingResponseModel")
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/settings/{setting_id}",
 *     summary="Update a setting",
 *     tags={"System Settings"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="setting_id",
 *         in="path",
 *         description="ID of a setting",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(@OA\Property(property="value", type="string"),),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SettingResponseModel"),
 *     )
 * )
 */
