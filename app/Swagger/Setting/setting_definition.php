<?php
/**
 * @OA\Schema(
 *  schema="SettingCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",    type="integer", default="1", readOnly=true),
 * @OA\Property(property="key",   type="string"),
 * @OA\Property(property="value", type="string"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="SettingModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/SettingCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="type",       type="string"),
 * @OA\Property(property="category",       type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="SettingResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/SettingModel"),
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="SettingListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/SettingModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
