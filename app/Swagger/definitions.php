<?php
/**
 * @OA\Schema                         (
 *  schema="UnauthorizedResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="body",      default="null"),
 * @OA\Property(property="status",    type="object",
 *          allOf={
 * @OA\Schema                         (
 * @OA\Property(property="messsage",  default="unauthorized"),
 * @OA\Property(property="code",      default="unauthorized"),
 * @OA\Property(property="code_text", default="unauthorized"),
 *          )}
 *     )
 *  )}
 * )
 */

/**
 * @OA\Schema                         (
 *  schema="ServerErrorResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="body",      default="null"),
 * @OA\Property(property="status",    type="object",
 *          allOf={
 * @OA\Schema                         (
 * @OA\Property(property="message",   default="server error occurred. Report the issue to developer/vendor.",type="string"),
 * @OA\Property(property="code",      default="server_error",type="string"),
 * @OA\Property(property="code_text", default="server_error",type="string"),
 *          )}
 *     )
 *  )}
 * )
 */

/**
 * @OA\Schema                         (
 *     schema="ResponseStatusModel",
 * @OA\Property(property="message",   type="string"),
 * @OA\Property(property="code",      type="string"),
 * @OA\Property(property="code_text", type="string"),
 * )
 */

/**
 * @OA\Schema                         (
 *     schema="ResponseStatusOkModel",
 * @OA\Property(property="message",   type="string", default=""),
 * @OA\Property(property="code",      type="string",default="ok"),
 * @OA\Property(property="code_text", type="string",default="ok"),
 *
 * )
 */


/**
 * @OA\Schema                         (
 *     schema="ResponseStatusEmptyRequiredFieldsModel",
 * @OA\Property(property="message",   type="string", default="validation error"),
 * @OA\Property(property="code",      type="string",default="empty_required_field"),
 * @OA\Property(property="code_text", type="string",default="empty_required_field"),
 * )
 */

/**
 * @OA\Schema                                           (
 *     schema="ListDataBody",
 * @OA\Property(property="current_page",type="integer"  ,default=1),
 * @OA\Property(property="first_page_url",type="string" ,default="http//url/api/model?page=1"),
 * @OA\Property(property="from",type="integer"          ,default=1),
 * @OA\Property(property="last_page",type="integer"     ,default=1),
 * @OA\Property(property="last_page_url",type="string"  ,default="http//url/api/model?page=pagenumber"),
 * @OA\Property(property="links",                       type="array", @OA\Items(ref="#/components/schemas/ListLinksDataBody")),
 * @OA\Property(property="next_page_url",type="string"  ,default="null"),
 * @OA\Property(property="path",type="string"           ,default="http//url/api/model?page=pagenumber"),
 * @OA\Property(property="per_page",type="integer"      ,default="15"),
 * @OA\Property(property="prev_page_url",type="string"  ,default="null"),
 * @OA\Property(property="to",type="integer"            ,default=1),
 * @OA\Property(property="total",type="integer"         ,default=1)
 * )
 */

/**
 * @OA\Schema(
 *     schema="ResponseModelNotFound",
 * @OA\Property(property="message",   type="string", default="The item/page you were looking for cannot be found."),
 * @OA\Property(property="code",      type="string",default="not_found"),
 * @OA\Property(property="code_text", type="string",default="not founr"),
 *
 * )
 */

/**
 * @OA\Schema(
 *     schema="ListLinksDataBody",
 *     allOf={
 * @OA\Schema   (
 * @OA\Property (property="url", type="string", default="{url}/api/{model}?page=1"),
 * @OA\Property (property="label", type="string", default="1"),
 * @OA\Property (property="active", type="boolean", default=true),
 *      ),
 *     }
 * )
 */
