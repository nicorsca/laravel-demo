<?php
/**
 * @OA\Schema(
 *  schema="EmployeeCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",             type="integer", default="1", readOnly=true),
 * @OA\Property(property="code",           type="string"),
 * @OA\Property(property="personal_email", type="string"),
 * @OA\Property(property="position",       type="string"),
 * @OA\Property(property="joined_at",      type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="gender",         type="integer"),
 * @OA\Property(property="dob",            type="string"),
 * @OA\Property(property="marital_status", type="integer"),
 * @OA\Property(property="avatar",         type="string"),
 * @OA\Property(property="pan_no",         type="integer", default="100000000"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="EmployeeModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/EmployeeCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeRequestModel",
 * @OA\Property(property="first_name",     type="string"),
 * @OA\Property(property="last_name",      type="string"),
 * @OA\Property(property="middle_name",    type="string"),
 * @OA\Property(property="email",          type="string"),
 * @OA\Property(property="personal_email", type="string"),
 * @OA\Property(property="position",       type="string"),
 * @OA\Property(property="joined_at",      type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="gender",         type="integer"),
 * @OA\Property(property="dob",            type="string"),
 * @OA\Property(property="marital_status", type="integer"),
 * @OA\Property(property="pan_no",         type="integer", default="100000000"),
 * @OA\Property(property="department_id",  type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/EmployeeModel"),
 * @OA\Schema   (
 * @OA\Property (property="user", type="object", ref="#/components/schemas/UserModel"),
 * @OA\Property (property="department", type="object", ref="#/components/schemas/DepartmentModel"),
 *      )
 *     }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeCreateResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/EmployeeModel"),
 * @OA\Schema   (
 * @OA\Property (property="user", type="object", ref="#/components/schemas/UserModel"),
 *      )
 *     }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (
 * @OA\Property(property="id",                          type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",                        type="string"),
 * @OA\Property(property="email",                       type="string"),
 * @OA\Property(property="status",                      type="integer"),
 * @OA\Property(property="avatar",                      type="string"),
 * @OA\Property(property="code",                        type="string"),
 * @OA\Property(property="position",                    type="string"),
 * @OA\Property(property="joined_at",                   type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="department",                  type="string"),
 * @OA\Property(property="created_at",                  type="string"),
 *     ),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeDetailResponseModel",
 *  allOf={
 * @OA\Schema                              (
 * @OA\Property(property="id",             type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",           type="string"),
 * @OA\Property(property="first_name",     type="string"),
 * @OA\Property(property="last_name",      type="string"),
 * @OA\Property(property="middle_name",    type="string"),
 * @OA\Property(property="email",          type="string"),
 * @OA\Property(property="status",         type="integer"),
 * @OA\Property(property="avatar",         type="string"),
 * @OA\Property(property="code",           type="string"),
 * @OA\Property(property="personal_email", type="string"),
 * @OA\Property(property="position",       type="string"),
 * @OA\Property(property="joined_at",      type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="detached_at",    type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="gender",         type="integer"),
 * @OA\Property(property="dob",            type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="marital_status", type="integer"),
 * @OA\Property(property="pan_no",         type="string"),
 * @OA\Property(property="department_id",  type="integer"),
 * @OA\Property(property="created_at",     type="string"),
 * @OA\Property(property="updated_at",     type="string"),
 * @OA\Property(property="department",     type="string"),
 *     ),
 * @OA\Schema                              (
 * @OA\Property                            (property="addresses", type="array", @OA\Items(ref="#/components/schemas/AddressModel")),
 * @OA\Property                            (property="contacts", type="array", @OA\Items(ref="#/components/schemas/ContactModel")),
 * @OA\Property                            (property="bank", type="object", ref="#/components/schemas/BankModel"),
 * @OA\Property                            (property="allocated_leaves", type="array", @OA\Items(ref="#/components/schemas/EmployeeAllocatedLeaveModel")),
 * @OA\Property                            (property="documents", type="array", @OA\Items(ref="#/components/schemas/DocumentModel")),
 *      )
 *     }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AdditionalInfoAddressRequestModel",
 * @OA\Property(property="id",       type="integer", default="1"),
 * @OA\Property(property="country",  type="string"),
 * @OA\Property(property="state",    type="string"),
 * @OA\Property(property="city",     type="string"),
 * @OA\Property(property="street",   type="string"),
 * @OA\Property(property="zip_code", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="AdditionalInfoContactRequestModel",
 * @OA\Property(property="id",     type="integer", default="1"),
 * @OA\Property(property="number", type="string"),
 * @OA\Property(property="type",   type="integer"),
 * @OA\Property(property="status", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeAdditionalInfoRequestModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="addresses", type="array", @OA\Items(ref="#/components/schemas/AdditionalInfoAddressRequestModel")),
 * @OA\Property(property="contacts",  type="array", @OA\Items(ref="#/components/schemas/AdditionalInfoContactRequestModel")),
 *     )
 *  }
 * )
 */
