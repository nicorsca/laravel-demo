<?php
/**
 * @OA\Schema(
 *  schema="SocialSecurityCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",         type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",      type="string"),
 * @OA\Property(property="number",     type="string"),
 * @OA\Property(property="start_date", type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="status",     type="integer"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="SocialSecurityModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/SocialSecurityCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="end_date",   type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="employee_id", type="integer"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="SocialSecurityUpdateRequestModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/SocialSecurityCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="end_date", type="string", default="YYYY-MM-DD"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="SocialSecurityResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/SocialSecurityModel"),
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="SocialSecurityListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/SocialSecurityModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


