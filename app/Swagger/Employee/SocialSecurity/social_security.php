<?php

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/social-securities",
 *     summary="Fetch list of employee social securities",
 *     tags={"Employee Social Securities"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "number", "start_date", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="number",
 *         in="query",
 *         description="search by number",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=UNPUBLISHED, 2=PUBLISHED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/social-securities/{social_security_id}",
 *     summary="Get detail of a social security",
 *     tags={"Employee Social Securities"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Parameter(
 *         name="social_security_id",
 *         in="path",
 *         description="ID of a social security",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post                                                               (
 *     path="/api/employees/{employee_id}/social-securities",
 *     summary="Create new social security",
 *     tags={"Employee Social Securities"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/social-securities/{social_security_id}",
 *     summary="Update a social security",
 *     tags={"Employee Social Securities"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="social_security_id",
 *         in="path",
 *         description="ID of a social security",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityUpdateRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/employees/{employee_id}/social-securities/{social_security_id}",
 *     summary="Delete a social security",
 *     tags={"Employee Social Securities"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="social_security_id",
 *         in="path",
 *         description="ID of a SocialSecurity",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/social-securities/{social_security_id}/status",
 *     summary="Toggle the status of social security",
 *     tags={"Employee Social Securities"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="social_security_id",
 *         in="path",
 *         description="ID of a Social Security",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/SocialSecurityResponseModel"),
 *     )
 * )
 */
