<?php
/**
 * @OA\Schema(
 *  schema="EmployeeHistoryCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",      type="integer", default="1", readOnly=true),
 * @OA\Property(property="date",    type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="comment", type="string"),
 * @OA\Property(property="status",  type="integer"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="EmployeeHistoryModel",
 *      allOf={
 * @OA\Schema                          (ref="#/components/schemas/EmployeeHistoryCommonModel"),
 * @OA\Schema(
 * @OA\Property(property="employee_id", type="integer"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeHistoryRequestModel",
 * @OA\Property(property="comment",   type="string"),
 * @OA\Property(property="url",       type="string"),
 * @OA\Property(property="thumbnail", type="string"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeHistoryResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/EmployeeHistoryModel"),
 * @OA\Schema   (
 * @OA\Property (property="document", type="object", ref="#/components/schemas/DocumentModel"),
 *      )
 *     }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeHistoryListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/EmployeeHistoryResponseModel"),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
