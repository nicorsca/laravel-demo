<?php
/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",         type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",      type="string"),
 * @OA\Property(property="days",       type="integer"),
 * @OA\Property(property="start_date", type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="end_date",   type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="status",     type="integer"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/EmployeeAllocatedLeaveCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="employee_id", type="integer"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveUpdateRequestModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="title",      type="string"),
 * @OA\Property(property="days",      type="string"),
 * @OA\Property(property="type",      type="string"),
 * @OA\Property(property="status",     type="integer"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveCreateRequestModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/EmployeeAllocatedLeaveUpdateRequestModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="current_fiscal_year", type="boolean"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/EmployeeAllocatedLeaveModel"),
 *  }
 * )
 */


/**
 * @OA\Schema(
 *  schema="EmployeeAllocatedLeaveListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/EmployeeAllocatedLeaveModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
