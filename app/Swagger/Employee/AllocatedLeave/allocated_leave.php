<?php

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/allocated-leaves",
 *     summary="Fetch list of employee allocated-leaves allocations",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "days", "start_date", "end_date", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=Unpublished, 2=Published",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/allocated-leaves/{allocated_leave_id}",
 *     summary="Get detail of a leave allocated",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Parameter(
 *         name="allocated_leave_id",
 *         in="path",
 *         description="ID of a leave",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post                                                                       (
 *     path="/api/employees/{employee_id}/allocated-leaves",
 *     summary="Create new leave allocation",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveCreateRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/allocated-leaves/{allocated_leave_id}",
 *     summary="Update a leave allocated",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="allocated_leave_id",
 *         in="path",
 *         description="ID of a leave",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveUpdateRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/employees/{employee_id}/allocated-leaves/{allocated_leave_id}",
 *     summary="Delete a leave allocated",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="allocated_leave_id",
 *         in="path",
 *         description="ID of a Leave",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/allocated-leaves/{allocated_leave_id}/status",
 *     summary="Toggle the status of leave allocated",
 *     tags={"Employee Allocated Leaves"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="allocated_leave_id",
 *         in="path",
 *         description="ID of a Leave",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeAllocatedLeaveResponseModel"),
 *     )
 * )
 */
