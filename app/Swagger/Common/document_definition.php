<?php

/**
 * @OA\Schema(
 *  schema="DocumentCommonModel",
 * @OA\Property(property="id",        type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",     type="string"),
 * @OA\Property(property="url",       type="string"),
 * @OA\Property(property="thumbnail", type="string"),
 * @OA\Property(property="status",    type="integer"),
 * @OA\Property(property="type", type="string"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="DocumentModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/DocumentCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="documentary_id", type="string"),
 * @OA\Property(property="documentary_type", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="DocumentListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/DocumentModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

