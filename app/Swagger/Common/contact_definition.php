<?php
/**
 * @OA\Schema(
 *  schema="ContactCommonModel",
 * @OA\Property(property="id",     type="integer", default="1", readOnly=true),
 * @OA\Property(property="number", type="string"),
 * @OA\Property(property="type",   type="integer"),
 * @OA\Property(property="status", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="ContactModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/ContactCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="contactable_id",       type="integer"),
 * @OA\Property(property="contactable_type",       type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ContactResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/ContactModel"),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ContactListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/ContactModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
