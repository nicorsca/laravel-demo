<?php
/**
 * @OA\Schema(
 *  schema="AddressCommonModel",
 * @OA\Property(property="id",       type="integer", default="1", readOnly=true),
 * @OA\Property(property="country",  type="string"),
 * @OA\Property(property="state",    type="string"),
 * @OA\Property(property="city",     type="string"),
 * @OA\Property(property="street",   type="string"),
 * @OA\Property(property="zip_code", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="AddressModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/AddressCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="type",       type="integer"),
 * @OA\Property(property="addressable_id",       type="integer"),
 * @OA\Property(property="addressable_type",       type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AddressCreateRequestModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/AddressCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="type", type="integer"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AddressResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/AddressModel"),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AddressListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/AddressModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

