<?php
/**
 * @OA\Schema(
 *  schema="LeaveCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",        type="string", default="Application for leave request"),
 * @OA\Property(property="description",  type="string"),
 * @OA\Property(property="start_at",     type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="end_at",       type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="type",         type="string", default="sick"),
 * @OA\Property(property="is_emergency", type="boolean", default=false),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="LeaveModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/LeaveCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="days",       type="integer", default=1),
 * @OA\Property(property="status",     type="integer", default=1),
 * @OA\Property(property="reason",     type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveRequestModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/LeaveCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="requested_to", type="integer"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveEmergencyRequestModel",
 *      allOf={
 * @OA\Schema(
 *      @OA\Property(property="title",        type="string", default="Application for leave request"),
 *      @OA\Property(property="description",  type="string"),
 *      @OA\Property(property="start_at",     type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="end_at",       type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="type",         type="string", default="sick"),
 *      @OA\Property(property="employee_id", type="integer"),
 *     ),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveEmployeeModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",         type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",       type="string"),
 * @OA\Property(property="email",      type="string"),
 * @OA\Property(property="avatar",     type="string"),
 * @OA\Property(property="code",       type="string"),
 * @OA\Property(property="department", type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/LeaveModel"),
 * @OA\Schema   (
 * @OA\Property (property="requested", type="object", ref="#/components/schemas/LeaveEmployeeModel"),
 * @OA\Property (property="employee", type="object", ref="#/components/schemas/LeaveEmployeeModel"),
 *      )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveListModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",        type="string", default="Application for leave request"),
 * @OA\Property(property="start_at",     type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="end_at",       type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="days",         type="integer", default=1),
 * @OA\Property(property="type",         type="string", default="sick"),
 * @OA\Property(property="status",       type="integer", default=1),
 * @OA\Property(property="is_emergency", type="boolean", default=false),
 * @OA\Property(property="reason",       type="string"),
 * @OA\Property(property="created_at",   type="string"),
 * @OA\Property(property="updated_at",   type="string"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="LeaveListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/LeaveModel"),
 * @OA\Schema                                           (
 * @OA\Property                                         (property="requested", type="object", ref="#/components/schemas/LeaveEmployeeModel"),
 * @OA\Property                                         (property="employee", type="object", ref="#/components/schemas/LeaveEmployeeModel"),
 *              )
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="LeaveSummaryResponseModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property (property="leave_type", type="object",
 *      allOf={
 *      @OA\Schema(
 *          @OA\Property(property="allocated",        type="integer"),
 *          @OA\Property(property="leaves",  type="integer"),
 *     )}
 * ),
 *     )}
 * )
 */
