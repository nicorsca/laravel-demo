<?php
/**
 * @OA\Schema(
 *  schema="ProjectCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="description",       type="string"),
 * @OA\Property(property="code",       type="string"),
 * @OA\Property(property="type",       type="string"),
 * @OA\Property(property="avatar",       type="string"),
 * @OA\Property(property="start_date",       type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/ProjectCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="end_date", type="string"),
 * @OA\Property(property="status", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectRequestModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="description",       type="string"),
 * @OA\Property(property="code",       type="string"),
 * @OA\Property(property="type",       type="string"),
 * @OA\Property(property="avatar",       type="string"),
 * @OA\Property(property="start_date",       type="string"),
 * @OA\Property(property="client_id",       type="integer"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectDetailResponseModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/ProjectModel",
 *     ),
 * @OA\Schema(
 * @OA\Property (property="client", type="object", ref="#/components/schemas/ClientModel"),
 * @OA\Property                            (property="documents", type="array", @OA\Items(ref="#/components/schemas/DocumentModel")),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectListModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="code",       type="string"),
 * @OA\Property(property="type",       type="string"),
 * @OA\Property(property="avatar",       type="string"),
 * @OA\Property(property="status", type="string"),
 * @OA\Property(property="start_date",       type="string"),
 * @OA\Property(property="end_date", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     ),
 * @OA\Schema(
 * @OA\Property (property="client", type="object", ref="#/components/schemas/ClientModel"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/ProjectListModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


