<?php

/**
 * @OA\Put(
 *     path="/api/projects/{project_id}/assign",
 *     summary="Assign a project to a employee or update the project role",
 *     tags={"Project Members"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ProjectMemberCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent({}),
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/projects/{project_id}/revoke",
 *     summary="Revoke employee from a project",
 *     tags={"Project Members"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(@OA\Property(property="employee_id",       type="integer"),),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent({}),
 *     )
 * )
 */
