<?php
/**
 * @OA\Schema(
 *  schema="ProjectMemberCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="employee_ids",          type="array", @OA\Items()),
 * @OA\Property(property="role_id",       type="integer"),
 *     )}
 * )
 */
