<?php

/**
 * @OA\Get(
 *     path="/api/projects",
 *     summary="Fetch list of projects",
 *     tags={"Projects"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"name", "created_at"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by name",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="code",
 *         in="query",
 *         description="search by code",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="search by type",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="client_id",
 *         in="query",
 *         description="filter by client id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ProjectListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/projects/{project_id}",
 *     summary="Get detail of a project",
 *     tags={"Projects"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ProjectDetailResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post (
 *     path="/api/projects",
 *     summary="Create new project",
 *     tags={"Projects"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ProjectRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ProjectModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/projects/{project_id}",
 *     summary="Update a project",
 *     tags={"Projects"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ProjectRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ProjectModel"),
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/projects/{project_id}/archive",
 *     summary="Archive a project",
 *     tags={"Projects"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ProjectModel"),
 *     )
 * )
 */
