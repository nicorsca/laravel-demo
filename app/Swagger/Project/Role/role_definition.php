<?php
/**
 * @OA\Schema(
 *  schema="ProjectRoleCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectRoleModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/ProjectRoleCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ProjectRoleListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/ProjectRoleModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
