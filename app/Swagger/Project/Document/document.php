<?php


/**
 * @OA\Get(
 *     path="/api/projects/{project_id}/documents",
 *     summary="Fetch list of project documents",
 *     tags={"Project Documents"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=UNPUBLISHED, 2=PUBLISHED",
 * @OA\Schema(
 *             type="string",
 *             enum={"0","1", "2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DocumentListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Post                                                         (
 *     path="/api/projects/{project_id}/documents",
 *     summary="Add new project document to the organization",
 *     tags={"Project Documents"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/DocumentCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DocumentModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/projects/{project_id}/documents/{document_id}",
 *     summary="Get detail of a project document",
 *     tags={"Project Documents"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="document_id",
 *         in="path",
 *         description="ID of a document",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DocumentModel")
 *     )
 * )
 */


/**
 * @OA\Put(
 *     path="/api/projects/{project_id}/documents/{document_id}",
 *     summary="Update a project document",
 *     tags={"Project Documents"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\Parameter(
 *         name="document_id",
 *         in="path",
 *         description="ID of a document",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/DocumentCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DocumentModel"),
 *     )
 * )
 */

/**
 * @OA\Delete                      (
 *     path="/api/projects/{project_id}/documents/{document_id}",
 *     summary="Delete a project document",
 *     tags={"Project Documents"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="project_id",
 *         in="path",
 *         description="ID of a project",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\Parameter(
 *         name="document_id",
 *         in="path",
 *         description="ID of a document",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object"),
 *     )
 * )
 */
