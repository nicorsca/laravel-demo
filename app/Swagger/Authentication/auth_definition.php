<?php
/**
 * @OA\Schema(
 *  schema="LoginRequestModel",
 * @OA\Property (property="username", type="string"),
 * @OA\Property (property="password", type="string"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="LoginResponseModel",
 *  allOf={
 * @OA\Schema(ref="#/components/schemas/UserModel"),
 *   }
 * )
 */


/**
 * @OA\Schema(
 *  schema="ResetPasswordModel",
 * @OA\Property (property="token", type="string"),
 * @OA\Property (property="email", type="string"),
 * @OA\Property (property="password", type="string"),
 * @OA\Property (property="password_confirmation", type="string"),
 * )
 */

/**
 * @OA\Schema                                         (
 *     schema="ChangePasswordModel",
 * @OA\Property(property="old_password",              type="string"),
 * @OA\Property(property="new_password",              type="string"),
 * @OA\Property(property="new_password_confirmation", type="string"),
 * )
 */
