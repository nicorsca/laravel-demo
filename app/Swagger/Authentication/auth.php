<?php
/**
 * @OA\Post                                                       (
 *     path="/api/auth/login",
 *     summary="Login using username and password",
 *     tags={"Auth"},
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/LoginRequestModel")
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/LoginResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/auth/login/{provider}",
 *     summary="Login using google or microsoft",
 *     tags={"Auth"},
 * @OA\Parameter(
 *         name="provider",
 *         in="path",
 *         description="Enter Provider name",
 *         required=true,
 * @OA\Schema(
 *          type="string",
 *          enum={"google","microsoft"}
 *          ),
 *     ),
 * @OA\Parameter(
 *         name="redirect_uri",
 *         in="query",
 *         description="enter redirect uri",
 *          required=true,
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=302,
 *          description="redirect to the provider and if authenticated then redirect to frontend",
 *     )
 *
 * )
 */

/**
 * @OA\Post                       (
 *     path="/api/password/email",
 *     summary="Request for reset password",
 *     tags={"Auth"},
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(@OA\Property   (property="email", type="string"))
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     )
 *
 * )
 */

/**
 * @OA\Post                                                       (
 *     path="/api/password/reset",
 *     summary="Change password using reset password link",
 *     tags={"Auth"},
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ResetPasswordModel")
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     )
 * )
 */

/**
 * @OA\Put                                                         (
 *     path="/api/auth/change-password",
 *     summary="Change password for authenticated user",
 *     tags={"Auth"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ChangePasswordModel")
 *    ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/auth/me",
 *     summary="Get authenticated user detail",
 *     tags={"Auth"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/auth/me/details",
 *     summary="Get authenticated detail of employee",
 *     tags={"Auth"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeDetailResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get                        (
 *     path="/api/auth/logout",
 *     summary="Logout of the system",
 *     tags={"Auth"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     )
 * )
 */

/**
 * @OA\Put                                                         (
 *     path="/api/auth/me/avatar",
 *     summary="Update avatar of own",
 *     tags={"Auth"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(@OA\Property   (property="avatar", type="string"))
 *    ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserResponseModel")
 *     )
 * )
 */
