<?php
/**
 * @OA\Schema(
 *  schema="FileUploadResponseModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="path",       type="string"),
 * @OA\Property(property="thumb_path", type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="FileUploadListResponseModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/FileUploadResponseModel")),
 *     ),
 *  }
 * )
 */
