<?php
/**
 * @OA\Schema(
 *  schema="HolidayCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="date", type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="remarks",      type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="MultipleHolidayRequestModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="holidays",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/HolidayCommonModel"),
 *     }
 *     )),
 *     ),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="HolidayModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/HolidayCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="HolidayListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/HolidayModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


