<?php

/**
 * @OA\Get(
 *     path="/api/holidays",
 *     summary="Fetch list of holidays",
 *     tags={"Holidays"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "created_at", "date"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         description="Filter date start from date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         description="Filter date end less than or equal to end at: required with start_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/HolidayListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/holidays/{holiday_id}",
 *     summary="Get detail of a holiday",
 *     tags={"Holidays"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="holiday_id",
 *         in="path",
 *         description="ID of a holiday",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/HolidayModel")
 *     )
 * )
 */

/**
 * @OA\Post (
 *     path="/api/holidays",
 *     summary="Create new holiday",
 *     tags={"Holidays"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/HolidayCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/HolidayModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/holidays/{holiday_id}",
 *     summary="Update a holiday",
 *     tags={"Holidays"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="holiday_id",
 *         in="path",
 *         description="ID of a holiday",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/HolidayCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/HolidayModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/holidays/{holiday_id}",
 *     summary="Delete a Holiday",
 *     tags={"Holidays"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="holiday_id",
 *         in="path",
 *         description="ID of a Holiday",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */

/**
 * @OA\Post (
 *     path="/api/holidays/multiple",
 *     summary="Create new multiple holidays",
 *     tags={"Holidays"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/MultipleHolidayRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/HolidayListResponseModel"),
 *     ),
 *
 * )
 */
