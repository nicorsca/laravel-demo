<?php

/**
 * @OA\Get(
 *     path="/api/managers",
 *     summary="Fetch list of employees having role as manager",
 *     tags={"Managers"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"name", "email", "position", "joined_at", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by name, email",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="department",
 *         in="query",
 *         description="search by department title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="department_id",
 *         in="query",
 *         description="search by department id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="code",
 *         in="query",
 *         description="search by code",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 0=UNLOCKED, 1=LOCKED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","0"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeListResponseModel")
 *     )
 *
 * )
 */


/**
 * @OA\Get(
 *     path="/api/managers/{employee_id}",
 *     summary="Get detail of a employee",
 *     tags={"Managers"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeeDetailResponseModel")
 *     )
 * )
 */
