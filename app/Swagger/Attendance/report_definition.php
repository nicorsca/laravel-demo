<?php

/**
 * @OA\Schema(
 *  schema="AttendanceListModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="employee_id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="attend_date",        type="string", default="YYYY-MM-DD"),
 * @OA\Property(property="check_in",     type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="check_out",       type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="leave_in",       type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="leave_out",       type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="office_time",       type="string", default="H:i:s"),
 * @OA\Property(property="work_time",       type="string", default="H:i:s"),
 * @OA\Property(property="break_time",       type="string", default="H:i:s"),
 * @OA\Property(property="leave_time",       type="string", default="H:i:s"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="AttendanceListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/AttendanceListModel"),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeesAttendanceSummaryReportModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="employee_id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="present_days",        type="integer"),
 * @OA\Property(property="leave_days",        type="integer"),
 * @OA\Property(property="avg_check_in",        type="integer"),
 * @OA\Property(property="avg_check_out",        type="integer"),
 * @OA\Property(property="total_office_time",        type="integer"),
 * @OA\Property(property="avg_office_time",        type="integer"),
 * @OA\Property(property="total_work_time",        type="integer"),
 * @OA\Property(property="avg_work_time",        type="integer"),
 * @OA\Property(property="total_break_time",        type="integer"),
 * @OA\Property(property="avg_break_time",        type="integer"),
 * @OA\Property(property="working_days",        type="integer"),
 * @OA\Property(property="absent_days",        type="integer"),
 * @OA\Property(property="employee",     type="object", allOf={
 *     @OA\Schema(
 *          @OA\Property(property="id",        type="integer"),
 *          @OA\Property(property="name",        type="string"),
 *          @OA\Property(property="email",        type="string"),
 *          @OA\Property(property="avatar",        type="string"),
 *          @OA\Property(property="position",        type="string"),
 * )
 *     }),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="EmployeesAttendanceSummaryReportResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/EmployeesAttendanceSummaryReportModel"),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeesAttendanceCalendarReportModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",        type="string"),
 * @OA\Property(property="avatar",        type="string"),
 * @OA\Property(property="attendances",     type="array", @OA\Items(allOf={
 *     @OA\Schema(
 *                  @OA\Property(property="date",        type="string"),
 *                  @OA\Property(property="present",        type="boolean"),
 *                  @OA\Property(property="leave",        type="boolean"),
 *                  @OA\Property(property="weekend",        type="boolean"),
 *                  @OA\Property(property="holiday",        type="boolean"),
 * )
 *     })),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="EmployeesAttendanceCalendarReportResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/EmployeesAttendanceCalendarReportModel"),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceDashboardEmployeeChartModel",
 *      allOf={
 * @OA\Schema(
 *      @OA\Property(property="date", type="string"),
 *      @OA\Property(property="weekend", type="boolean"),
 *      @OA\Property(property="holiday", type="boolean"),
 *      @OA\Property(property="check_in",     type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="check_out",       type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="leave_in",       type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="leave_out",       type="string", default="YYYY-MM-DD H:i:s"),
 *      @OA\Property(property="office_time",       type="integer"),
 *      @OA\Property(property="work_time",       type="integer"),
 *      @OA\Property(property="break_time",       type="integer"),
 *      @OA\Property(property="leave_time",       type="integer"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceDashboardEmployeeChartResponseModel",
 *      allOf={
 * @OA\Property(type="array",
 *      @OA\Items(
 *          allOf={
 * @OA\Schema(ref="#/components/schemas/AttendanceDashboardEmployeeChartModel"),
 *          },
 *     ))}
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceTodayReportModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="name",        type="string"),
 * @OA\Property(property="email",        type="string"),
 * @OA\Property(property="avatar",        type="string"),
 * @OA\Property(property="id",        type="integer"),
 * @OA\Property(property="code",        type="integer"),
 * @OA\Property(property="position",        type="integer"),
 * @OA\Property (property="employee_work_time", type="string"),
 * ),
 * @OA\Schema(
 *          ref="#/components/schemas/AttendanceListModel",
 *     ),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceTodayReportResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/AttendanceTodayReportModel"),
 *          }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */
