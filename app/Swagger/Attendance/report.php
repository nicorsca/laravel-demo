<?php

/**
 * @OA\Get(
 *     path="/api/attendances",
 *     summary="Fetch list of attendances of employees",
 *     tags={"Attendance Reports"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"attend_date", "check_in"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="Filter by employee_id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="on_leave",
 *         in="query",
 *         description="employees attendance who are on leave",
 * @OA\Schema(
 *             type="integer",
 *             enum={"1"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/summary",
 *     summary="Get employees attendance summary of the employee within a date range.",
 *     tags={"Attendance Reports"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="Filter by employee_id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeesAttendanceSummaryReportResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/average",
 *     summary="Fetch list of attedance average data according to date range",
 *     tags={"Attendance Reports"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="Filter by employee_id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceDashboardEmployeeChartResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/calendar",
 *     summary="Get employees attendance date range wise calendar activities.",
 *     tags={"Attendance Reports"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="Filter by employee_id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/EmployeesAttendanceCalendarReportResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/employees/{employee_id}/summary",
 *     summary="Get attendance summary of the employee within a date range.",
 *     tags={"Attendance Reports"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(
 *      @OA\Property(property="present_days",         type="integer"),
 *      @OA\Property(property="leave_days",         type="integer"),
 *      @OA\Property(property="total_office_time",         type="integer"),
 *      @OA\Property(property="total_work_time",         type="integer"),
 *      @OA\Property(property="total_break_time",         type="integer"),
 *      @OA\Property(property="total_leave_time",         type="integer"),
 * )
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/employees/{employee_id}/calendar",
 *     summary="Fetch data within date range of employee attendance",
 *     tags={"Attendance Reports"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"name", "email", "attend_date", "check_in"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceDashboardEmployeeChartResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/today/summary",
 *     summary="Get summarized attendance data of today",
 *     tags={"Attendance Reports"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(
 *      @OA\Property(property="employees",         type="integer"),
 *      @OA\Property(property="present",         type="integer"),
 *      @OA\Property(property="leaves",         type="integer"),
 *      @OA\Property(property="absent",         type="integer"),
 *      @OA\Property(property="on_time_login",         type="integer"),
 *      @OA\Property(property="late_login",         type="integer"),
 * )
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/today/attendance",
 *     summary="Fetch list of employees including their attendances of today",
 *     tags={"Attendance Reports"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceTodayReportResponseModel")
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/reports/summary/excel",
 *     summary="Get export to excel of employees attendance summary of the employee within a date range.",
 *     tags={"Attendance Reports"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         required=true,
 *         description="required with end_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="end_date",
 *         in="query",
 *         required=true,
 *         description="required with start_date for date range of attend_date",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="Filter by employee_id",
 * @OA\Schema(
 *             type="integer",
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="successful",
 *         @OA\Schema(
 *              type="file"
 *         )
 *     )
 *)
 */
