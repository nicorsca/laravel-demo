<?php
/**
 * @OA\Schema(
 *  schema="DepartmentCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="description", type="string"),
 * @OA\Property(property="status",      type="integer"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="DepartmentModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/DepartmentCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="DepartmentResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/DepartmentModel"),
 * @OA\Schema  (@OA\Property(property="employees_count", type="integer")),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="DepartmentEmployeesModel",
 * @OA\Property (property="id", type="integer"),
 * @OA\Property (property="name", type="string"),
 * @OA\Property (property="email", type="string"),
 * @OA\Property (property="position", type="string"),
 * @OA\Property (property="avatar", type="string"),
 * )
 */

/**
 * @OA\Schema(
 *  schema="DepartmentDetailResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/DepartmentModel"),
 * @OA\Schema   (@OA\Property(property="employees_count", type="integer")),
 * @OA\Schema   (
 * @OA\Property (property="hod", type="object", allOf={
 * @OA\Schema(
 * @OA\Property (property="id", type="integer"),
 * @OA\Property (property="name", type="string"),
 * @OA\Property (property="position", type="string"),
 * @OA\Property (property="avatar", type="string"),
 *              )
 *          }),
 * @OA\Property (property="employees", type="array",  @OA\Items(ref="#/components/schemas/DepartmentEmployeesModel")),
 *      )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="DepartmentListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/DepartmentModel"),
 * @OA\Schema                                           (
 * @OA\Property                                         (property="hod", type="object", ref="#/components/schemas/DepartmentEmployeesModel"),
 *              )
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


