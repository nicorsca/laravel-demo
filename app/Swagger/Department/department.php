<?php

/**
 * @OA\Get(
 *     path="/api/departments",
 *     summary="Fetch list of departments",
 *     tags={"Departments"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"title", "created_at", "status"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by title",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=UNPUBLISHED, 2=PUBLISHED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DepartmentListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/departments/{department_id}",
 *     summary="Get detail of a department",
 *     tags={"Departments"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="department_id",
 *         in="path",
 *         description="ID of a department",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DepartmentDetailResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post (
 *     path="/api/departments",
 *     summary="Create new department",
 *     tags={"Departments"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/DepartmentCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DepartmentResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/departments/{department_id}",
 *     summary="Update a department",
 *     tags={"Departments"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="department_id",
 *         in="path",
 *         description="ID of a department",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/DepartmentCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DepartmentResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/departments/{department_id}",
 *     summary="Delete a Department",
 *     tags={"Departments"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="department_id",
 *         in="path",
 *         description="ID of a Department",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */

/**
 * @OA\Put(
 *     path="/api/departments/{department_id}/status",
 *     summary="Toggle the status of department",
 *     tags={"Departments"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="department_id",
 *         in="path",
 *         description="ID of a Department",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/DepartmentResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Put (
 *     path="/api/departments/{department_id}/hod",
 *     summary="Assign or revoke a hod",
 *     tags={"Departments"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="department_id",
 *         in="path",
 *         description="ID of a Department",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(
 * @OA\Property(property="employee_id", type="integer", default="1"),
 *        ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object"),
 *     )
 * )
 */
