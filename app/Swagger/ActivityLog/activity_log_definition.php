<?php
/**
 * @OA\Schema(
 *  schema="ActivityLogModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",          type="integer", default="1", readOnly=true),
 * @OA\Property(property="title",       type="string"),
 * @OA\Property(property="data", type="string"),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 * @OA\Property(property="description", type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="ActivityLogResponseModel",
 *  allOf={
 * @OA\Schema  (ref="#/components/schemas/ActivityLogModel"),
 * @OA\Schema  (@OA\Property(property="model", type="object")),
 * @OA\Schema  (@OA\Property(property="user", type="object", ref="#/components/schemas/UserModel")),
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="ActivityLogListResponseModel",
 *  allOf={
 * @OA\Schema(
 * @OA\Property(property="data",                        type="array", @OA\Items(
 *          allOf={
 * @OA\Schema                                           (ref="#/components/schemas/ActivityLogResponseModel"),
 *     }
 *     )),
 *     ),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 * }
 * )
 */


