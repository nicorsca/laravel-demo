<?php

/**
 * Determines if the coming request is an api call or not
 * @param \Illuminate\Http\Request $request
 *
 * @return bool
 */
function is_api_request(\Illuminate\Http\Request $request): bool
{
    return $request->segment(1) == 'api';
}

/**
 * @param $string
 * @return array|string|null
 */
function turn_to_app_constant_format($string): array|string|null
{
    //replace all the special characters with underscore
    return preg_replace("/[^a-zA-Z0-9]/", '_', $string);
}

/**
 * Split name into 3 parts
 * @param $name
 * @return bool|array
 */
function split_name($name): bool|array
{
    $parts = array();

    while (strlen(trim($name)) > 0) {
        $name = trim($name);
        $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $parts[] = $string;
        $name = trim(preg_replace('#' . preg_quote($string, '#') . '#', '', $name));
    }

    if (empty($parts)) {
        return false;
    }

    $parts = array_reverse($parts);
    $name = array();
    $name['first_name'] = $parts[0];
    $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
    $name['last_name'] = (isset($parts[2])) ? $parts[2] : (isset($parts[1]) ? $parts[1] : '');

    return $name;
}

if (!function_exists('get_front_app_url')) {
    /**
     * @param $url
     * @return string
     */
    function get_front_app_url($url): string
    {
        $appUrl = url('/');
        $frontAppUrl = config('app.front_app_url');

        return str_replace($appUrl, $frontAppUrl, $url);
    }
}

if (!function_exists('convert_date_time_range_to_days')) {
    /**
     * @param string $startAt
     * @param string $endAt
     * @return float
     */
    function convert_date_time_range_to_days(string $startAt, string $endAt): float
    {
        $start = date_create($startAt);
        $end = date_create($endAt);
        $interval = date_diff($start, $end);
        $days = $interval->days;

        return ($interval->h > 12) ? $days + 1 : $days . '.5';
    }
}

if (!function_exists('check_date_is_between_date_range')) {
    /**
     * @param string $currentDate
     * @param string $startDate
     * @param string $endDate
     * @return bool
     */
    function check_date_is_between_date_range(string $currentDate, string $startDate, string $endDate): bool
    {
        $currentDate = date('Y-m-d', strtotime($currentDate));
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        return ($currentDate >= $startDate) && ($currentDate <= $endDate);
    }
}

if (!function_exists('check_date_time_is_between_date_range')) {
    /**
     * @param string $currentDateTime
     * @param string|null $startDateTime
     * @param string|null $endDateTime
     * @return bool
     */
    function check_date_time_is_between_date_range(string $currentDateTime, string $startDateTime = null, string $endDateTime = null): bool
    {
        $isGreaterThanStart = !$startDateTime || $currentDateTime > $startDateTime;
        $isLessThanEnd = !$endDateTime || $currentDateTime < $endDateTime;

        return $isGreaterThanStart && $isLessThanEnd;
    }
}

if (!function_exists('convert_hour_minute_to_seconds')) {
    /**
     * @param string $time
     * @return int
     */
    function convert_hour_minute_to_seconds(string $time): int
    {
        [$hours, $minutes] = explode(':', $time, 2);
        return $minutes * 60 + $hours * 3600;
    }
}

if (!function_exists('convert_unix_to_timezone_date_time')) {
    /**
     * @param string $unix
     * @param string $timezone
     * @param string $format
     * @return string
     */
    function convert_unix_to_timezone_date_time(string $unix, string $timezone = 'UTC', string $format = 'Y-m-d H:i:s'): string
    {
        $date = new DateTime();
        $date->setTimestamp($unix);
        return $date->setTimezone(new DateTimeZone($timezone))->format($format);
    }
}

if (!function_exists('convert_date_time_to_timezone')) {
    /**
     * @param string $dateTime
     * @param string $timezone
     * @param string $format
     *
     * @return string
     */
    function convert_date_time_to_timezone(string $dateTime, string $timezone = 'UTC', string $format = 'Y-m-d H:i:s'): string
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);

        return $date->setTimezone(new DateTimeZone($timezone))->format($format);
    }
}

if (!function_exists('convert_seconds_to_hour_minute')) {
    /**
     * @param int $seconds
     *
     * @return string
     */
    function convert_seconds_to_hour_minute(int $seconds): string
    {
        $hour = $seconds/3600;
        $time = explode('.', $hour);
        $minutes = count($time) === 2 ? substr($time[1], 0, 2) : 0;

        return $time[0] . 'h ' . $minutes . 'm';
    }
}

