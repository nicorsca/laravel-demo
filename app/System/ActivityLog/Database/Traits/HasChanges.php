<?php


namespace App\System\ActivityLog\Database\Traits;


use Illuminate\Database\Eloquent\Model;

trait HasChanges
{
    /**
     * @var array
     */
    protected array $oldAttributes = [];

    /**
     * boot has changes
     */
    public static function bootHasChanges(): void
    {
        if (static::actionToRecorded()->contains('updated')) {
            static::updating(function (Model $model) {
                //temporary hold the original attributes on the model
                //as we'll need these in the updating event
                $originalValue = $model->getOriginal();
                $attributes = $model->attributeToLogged();
                foreach ($attributes as $attribute) {
                    if (!array_key_exists($attribute, $originalValue)) {
                        $originalValue[$attribute] = null;
                    }
                }
                $oldValues = $model->replicate()->setRawAttributes($originalValue);

                $model->oldAttributes = static::logChanges($oldValues);
            });
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    public static function logChanges(Model $model): array
    {
        $changes = [];
        $attributes = $model->attributeToLogged();

        $newModel = clone $model;

        $newModel->append(array_filter($attributes, function ($key) use ($newModel) {
            return !array_key_exists($key, $newModel->getAttributes());
        }));

        $collection = collect($newModel);

        foreach ($attributes as $attribute) {
            $changes += $collection->only($attribute)->toArray();
        }

        return $changes;
    }

    /**
     * get attribute to be logged
     * @param $actionName
     * @return array
     */
    public function attributeValuesToLogged($actionName): array
    {
        if (!count($this->attributeToLogged())) {
            return [];
        }
        $data['new'] = self::logChanges($this->exists ? $this->fresh() ?? $this : $this);

        if (static::actionToRecorded()->contains('updated') && $actionName == 'updated') {
            $nullData = array_fill_keys(array_keys($data['new']), null);
            $data['old'] = array_merge($nullData, $this->oldAttributes);
        }

        //compare the old data with new data to find changed data only
        if (isset($data['old'])) {
            //getting updated new data
            $data['new'] = array_udiff_assoc(
                $data['new'],
                $data['old'],
                function ($new, $old) {
                    return $new <=> $old;
                }
            );
            //getting old data of new data
            $data['old'] = collect($data['old'])
                ->only(array_keys($data['new']))
                ->all();
        }

        return $data;
    }

    /**
     * @return array
     */
    public function attributeToLogged(): array
    {
        $attributes = [];

        //log fillable property on;y
        $attributes = array_merge($attributes, $this->getFillable());

        //if any log attribute is provided then return them as well
        if (isset(static::$logAttributes) && is_array(static::$logAttributes)) {
            $attributes = array_merge($attributes, array_diff(static::$logAttributes, ['*']));

            if (in_array('*', static::$logAttributes)) {
                $attributes = array_merge($attributes, array_keys($this->getAttributes()));
            }
        }
        //ignore the hidden attribute
        return array_diff($attributes, $this->getHidden());
    }
}
