<?php


namespace App\System\ActivityLog;


use App\System\ActivityLog\Database\Models\ActivityLog;
use Illuminate\Support\Facades\Auth;

class ActivityLogger
{
    /**
     * @var string
     */
    private string $title;

    private ?string $data = null;

    private ?string $model = null;

    private ?int $model_id = null;

    /**
     * save log
     */
    public function saveLog(int $user_id = null): void
    {
        if (Auth::check()) {
            $user_id = Auth::id();
        } elseif ($this->getModel() == 'User') {
            $user_id = $this->getModelId();
        }

        $log = new ActivityLog();
        $log->title = $this->getTitle();
        $log->data = $this->getData();
        $log->user_id = $user_id;
        $log->modular_type = $this->getModel();
        $log->modular_id = $this->getModelId();
        $log->save();
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return ActivityLogger
     */
    public function setModel(string $model): ActivityLogger
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getModelId(): ?int
    {
        return $this->model_id;
    }

    /**
     * @param int $model_id
     * @return \App\System\ActivityLog\ActivityLogger
     */
    public function setModelId(int $model_id): ActivityLogger
    {
        $this->model_id = $model_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return \App\System\ActivityLog\ActivityLogger
     */
    public function setTitle(string $title): ActivityLogger
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string | null
     */
    public function getData(): ?string
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return ActivityLogger
     */
    public function setData(array $data): ActivityLogger
    {
        $this->data = collect($data);
        return $this;
    }
}
