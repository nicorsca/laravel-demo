<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Department\Database\Models;

use App\System\AppBaseModel;
use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Database\Models\EmployeeView;
use Database\Factories\DepartmentFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'status', 'employee_id'];

    /**
     * @var array
     */
    protected array $sortableColumns = ['title', 'status', 'created_at'];

    /**
     * Department constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['user_id']);
    }

    /**
     * @return DepartmentFactory
     */
    protected static function newFactory(): DepartmentFactory
    {
        return DepartmentFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function hod(): BelongsTo
    {
        return $this->belongsTo(EmployeeView::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function employees(): HasMany
    {
        return $this->hasMany(EmployeeView::class, 'department_id', 'id');
    }

}
