<?php


namespace App\System;


use App\System\ActivityLog\Database\Traits\LogsActivity;
use App\System\Foundation\Scope\AddEmployeeScopeTrait;
use App\System\User\Database\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Foundation\Database\BaseModel;
use NicoSystem\Foundation\Database\EditorLogs;
use NicoSystem\Foundation\Status;

abstract class AppBaseModel extends BaseModel
{
    use SoftDeletes, EditorLogs, HasFactory, LogsActivity,AddEmployeeScopeTrait;

    /**
     * @const STATUS_ACTIVE
     */
    const STATUS_ACTIVE = 1;
    /**
     * @const STATUS_INACTIVE
     */
    const STATUS_INACTIVE = 0;

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'created_by', 'updated_by', 'deleted_by', 'pivot'];

    /**
     * AppBaseModel constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

    }

    /**
     * Bootstrap the model and its traits.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

    }

    abstract protected static function newFactory();

    /**
     * Tell what model should the model use while updating/creating/deleting.
     * Example return Auth::user()
     *
     * @return Authenticatable|null
     */
    public function editorProvider(): ?Authenticatable
    {
        return Auth::user();
    }

    /**
     * What model to use if editor log is enabled. example User::class
     *
     * @return string
     */
    public function getEditorModelClassName(): string
    {
        return User::class;
    }

    /**
     * Active scope definition
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeActive(Builder $builder): Builder
    {
        return $builder->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Check whether the model is editable or not
     *
     * @return boolean
     */
    public function isEditable(): bool
    {
        return $this->getAttribute('status') !== Status::STATUS_SUSPENDED;
    }

    /**
     * @return string
     */
    public function getMorphClass(): string
    {
        return class_basename($this);
    }
}
