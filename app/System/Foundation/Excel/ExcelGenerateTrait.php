<?php

namespace App\System\Foundation\Excel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

trait ExcelGenerateTrait
{

    /**
     * @param array $data
     * @param array $columns
     * @param array $widths
     * @return \PhpOffice\PhpSpreadsheet\Writer\Xlsx
     */
    protected function createExcelOrder(array $data, array $columns, array $widths): Xlsx
    {
        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        /* For Excel Column Headers */
        $row = 1;
        $col = 'A';
        foreach ($columns as $key => $column) {
            $sheet->setCellValue($col . $row, $column);
            $col++;
        }

        /* For Excel Data populate */
        $row = 2;
        foreach ($data as $value) {
            $col = 'A';
            foreach ($columns as $index => $column) {
                $sheet->setCellValue($col . $row, $value[$index])
                    ->getColumnDimension($col)
                    ->setWidth($widths[$index]);
                $col++;
            }
            $row++;
        }

        return new Xlsx($spreadsheet);
    }

}
