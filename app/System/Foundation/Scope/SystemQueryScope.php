<?php

namespace App\System\Foundation\Scope;

use ReflectionClass;

class SystemQueryScope
{
    /**
     * @var
     */
    public const EMPLOYEE = 'employee';

    /**
     * @var array $queries
     */
    public static array $queries = [];

    /**
     * @param $name
     *
     * @return Query
     */
    public static function getQueryInstance($name): Query
    {
        foreach (static::$queries as $queryName => $instance) {
            if ($name === $queryName) {
                return $instance;
            }
        }
        static::$queries[$name] = new Query();

        return static::$queries[$name];
    }

    /**
     * @return array
     */
    public static function getOptions(): array
    {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}
