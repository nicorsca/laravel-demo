<?php

namespace App\System\Foundation\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class GlobalEmployeeScope implements Scope
{
    public function __construct(protected Query $query)
    {
    }

    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model) : void
    {
        $this->query->queryParser($builder,$model);
    }
}
