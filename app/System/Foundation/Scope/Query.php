<?php

namespace App\System\Foundation\Scope;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Query
{
    protected array $builders = [];

    /**
     * @param $firstValue
     * @param $comparator
     * @param $secondValue
     */
    public function addWhere($firstValue, $comparator, $secondValue) : void
    {
        $this->builders['where'][] = [$firstValue, $comparator, $secondValue];
    }

    /**
     * @param $column
     * @param array $in
     */
    public function addWhereIn($column, array $in) : void
    {
        $this->builders['whereIn'][] = [$column, $in];
    }

    /**
     * @param Closure $closure
     */
    public function addWhereClosure(Closure $closure) : void
    {
        $this->builders['where'][] = $closure;
    }


    /**
     * @param Builder $query
     * @param Model $model
     *
     * @return Builder
     */
    public function queryParser(Builder $query, Model $model): Builder
    {

        foreach ($this->builders as $key => $operands) {
            foreach ($operands as $operand) {
                if ($operand instanceof Closure) {
                    $query->$key($operand);
                } elseif ($key == 'where') {
                    $query->where($model->qualifyColumn($operand[0]), $operand[1], $operand[2]);
                } elseif ($key == 'whereIn') {
                    $query->whereIn($model->qualifyColumn($operand[0]), $operand[1]);
                }
            }
        }
        return $query;
    }
}
