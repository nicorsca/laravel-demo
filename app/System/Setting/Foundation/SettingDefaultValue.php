<?php


namespace App\System\Setting\Foundation;


use App\System\Leave\Foundation\LeaveType;

class SettingDefaultValue
{

    public const LEAVES_CAN_REQUEST_BEFORE_DAYS_VALUE = 5;

    public const LEAVES_MAX_REQUEST_DAYS_VALUE = 10;

    public const SYSTEM_FISCAL_DATE_START_VALUE = '07-16';

    public const ATTENDANCE_EMPLOYEE_UPDATE_ENABLE_VALUE = true;

    public const ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS_VALUE = 1;

    public const SYSTEM_WEEKENDS_VALUE = [0, 6];

    public const DEFAULT_EMAIL_GROUPS_VALUE = [
        'lasata.shakya@ensue.us',
        'amar.lamichhane@ensue.us',
    ];

    public const PROJECT_TYPE_VALUE = ['in_house', 'client', 'others'];

    public const LEAVES_TYPE = [
        LeaveType::SICK_LEAVE,
        LeaveType::PERSONAL_LEAVE,
        LeaveType::MATERNITY_LEAVE,
        LeaveType::PATERNITY_LEAVE,
        LeaveType::BEREAVEMENT_LEAVE,
        LeaveType::COMPENSATORY_LEAVE,
    ];

    public const ATTENDANCE_EMPLOYEE_WORKING_HOURS_VALUE = 8;
}
