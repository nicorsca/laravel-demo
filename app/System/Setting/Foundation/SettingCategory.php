<?php

namespace App\System\Setting\Foundation;

class SettingCategory
{
    public const UI = 'ui';

    public const SYSTEM = 'system';

    public const LEAVES = 'leaves';

    public const ATTENDANCES = 'attendances';

    public const PROJECTS = 'projects';
}
