<?php


namespace App\System\Setting\Foundation;


use App\System\UserPreference\Foundation\UserPreferenceKey;

class SettingKey
{

    public const LEAVES_TYPE = 'leaves_type';

    public const LEAVES_MAX_REQUEST_DAYS = 'leaves_max_request_days';

    public const LEAVES_CAN_REQUEST_BEFORE_DAYS = 'leaves_can_request_before_days';

    public const SYSTEM_FISCAL_DATE_START = 'system_fiscal_date_start';

    public const ATTENDANCE_EMPLOYEE_UPDATE_ENABLE = 'attendance_employee_update_enable';

    public const ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS = 'attendance_employee_update_before_days';

    public const ATTENDANCE_EMPLOYEE_WORKING_HOURS = 'employee_working_hours';

    public const SYSTEM_WEEKENDS = 'system_weekends';

    public const DEFAULT_EMAIL_GROUPS = 'default_email_groups';

    public const PROJECT_TYPE = 'project_type';

    /**
     * @return string[]
     */
    public static function options(): array
    {
        return [
            self::LEAVES_TYPE,
            self::LEAVES_MAX_REQUEST_DAYS,
            self::LEAVES_CAN_REQUEST_BEFORE_DAYS,
            self::SYSTEM_FISCAL_DATE_START,
            self::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE,
            self::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS,
            UserPreferenceKey::EMPLOYEE_WORK_TIME,
            UserPreferenceKey::USER_TIMEZONE,
            UserPreferenceKey::USER_THEME,
            UserPreferenceKey::USER_LIST_VIEW,
            self::DEFAULT_EMAIL_GROUPS,
        ];
    }
}
