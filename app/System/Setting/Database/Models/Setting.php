<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Setting\Database\Models;

use App\System\AppBaseModel;
use App\System\Setting\Foundation\SettingType;

class Setting extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['key', 'value'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'key';

    protected array $sortableColumns = ['key', 'value'];

    protected static function newFactory(): void
    {
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValueAttribute($value): static
    {
        if ($this->type == SettingType::ARRAY) {
            $value = implode('|', $value);
        }
        $this->attributes['value'] = strtolower($value);

        return $this;
    }

    /**
     * @param $value
     * @return array|string
     */
    public function getValueAttribute($value): array|string
    {
        if ($this->type == SettingType::ARRAY && !is_array($value)) {
            $value  = explode('|', $value);
        }

        return $value;
    }
}
