<?php

namespace App\System\Attendance\Database\Models;

use App\System\AppBaseModel;
use App\System\Employee\Database\Models\EmployeeView;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use Database\Factories\AttendanceFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Attendance extends AppBaseModel
{

    protected string $globalQueryName = 'employee_relation';
    /**
     * @var string[]
     */
    protected $fillable = ['attend_at', 'type', 'punch_count', 'reason', 'browser', 'device', 'location', 'ip', 'fingerprint', 'employee_id', 'parent_id', 'status'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'attend_at';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['attend_at', 'type', 'status', 'created_at'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['employee_id', 'parent_id']);
    }

    /**
     * @return \Database\Factories\AttendanceFactory
     */
    protected static function newFactory(): AttendanceFactory
    {
        return AttendanceFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(EmployeeView::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Attendance::class, 'parent_id', 'id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $startAt
     * @param string $endAt
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereAttendAtLies(Builder $query, string $startAt, string $endAt): Builder
    {
        return $query->where('attend_at', '>=', $startAt)
            ->where('attend_at', '<=', $endAt);
    }
}
