<?php

namespace App\System\Common\Foundation;

class ContactType
{
    /**
     * Contact type Personal
     */
    const TYPE_PERSONAL = 1;

    /**
     * Contact type Home
     */
    const TYPE_HOME = 2;

    /**
     * Contact type Office
     */
    const TYPE_WORK = 3;

    /**
     * @return array
     */
    public static function options(): array
    {
        return [self::TYPE_PERSONAL, self::TYPE_HOME, self::TYPE_WORK];
    }

}
