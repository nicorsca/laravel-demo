<?php


namespace App\System\Common\Foundation;


class AddressType
{
    /**
     * Temporary Address
     */
    const TYPE_TEMPORARY = 1;

    /**
     * Permanent Address
     */
    const TYPE_PERMANENT = 2;

    /**
     * @return int[]
     */
    public static function options(): array
    {
        return [
            self::TYPE_TEMPORARY,
            self::TYPE_PERMANENT,
        ];
    }
}
