<?php


namespace App\System\Common\Repositories;


use Illuminate\Support\Collection;
use NicoSystem\Repositories\BaseRepository;

abstract class BaseCommonRepository extends BaseRepository
{
    /**
     * @param $model
     * @param string $relation
     * @param string $modelId
     * @param array  $inputs
     */
    public function updateRelations($model, string $relation, string $modelId, array $inputs): void
    {
        $model = $model->findOrfail($modelId);;

        $existingRelations = $model->{$relation};
        if (empty($inputs)) {
            $this->deleteRelations($model, $relation, $existingRelations->pluck('id'));
            return;
        }

        $existingIds = $existingRelations->pluck('id');
        $inputsIds = array_column($inputs, 'id');

        $relationToDeleteIds = $existingIds->diff($inputsIds);
        $this->deleteRelations($model, $relation, $relationToDeleteIds);
        $this->updateOrCreateRelations($model, $relation, $inputs);
    }

    /**
     * @param $model
     * @param string                         $relation
     * @param \Illuminate\Support\Collection $ids
     */
    private function deleteRelations($model, string $relation, Collection $ids): void
    {
        $model->{$relation}()->whereIn('id', $ids)->delete();
    }

    /**
     * @param $model
     * @param string $relation
     * @param array  $inputs
     */
    private function updateOrCreateRelations($model, string $relation, array $inputs): void
    {
        foreach ($inputs as $data) {
            $model->{$relation}()->updateOrCreate(
                [
                'id' => $data['id'] ?? null
                ], $data
            );
        }
    }
}
