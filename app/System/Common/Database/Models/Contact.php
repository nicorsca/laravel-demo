<?php

namespace App\System\Common\Database\Models;

use App\System\AppBaseModel;
use Database\Factories\ContactFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use NicoSystem\Foundation\Status;

class Contact extends AppBaseModel
{
    /**
     * Contact constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var string[]
     */
    protected $fillable = ['number', 'type', 'status', 'contactable_id', 'contactable_type'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'number';

    protected array $sortableColumns = ['number', 'type', 'status', 'created_at'];

    /**
     * @var array
     */
    protected $attributes = [
        'status' => Status::STATUS_PUBLISHED
    ];

    /**
     * @return MorphTo
     */
    public function contactable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return ContactFactory
     */
    protected static function newFactory(): ContactFactory
    {
        return new ContactFactory();
    }
}
