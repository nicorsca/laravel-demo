<?php


namespace App\System\Common\Database\Models;


use App\System\AppBaseModel;
use Database\Factories\BankFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Bank extends AppBaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['name', 'branch', 'account_number', 'status', 'bankable_id', 'bankable_type'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'name';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['name', 'branch', 'account_number', 'status', 'created_at'];

    /**
     * Bank constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return BankFactory
     */
    protected static function newFactory(): BankFactory
    {
        return BankFactory::new();
    }

    /**
     * @return MorphTo
     */
    public function bankable(): MorphTo
    {
        return $this->morphTo();
    }

}
