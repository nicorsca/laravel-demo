<?php


namespace App\System\Employee\Foundation;


class Gender
{
    /**
     * female gender
     */
    const FEMALE = 0;

    /**
     * male gender
     */
    const MALE = 1;

    /**
     * other gender
     */
    const OTHER = 2;

    /**
     * @return int[]
     */
    public static function options(): array
    {
        return [
            self::FEMALE,
            self::MALE,
            self::OTHER
        ];
    }

}
