<?php


namespace App\System\Employee\Foundation;


class MaritalStatus
{
    /**
     * female gender
     */
    const Married = 2;

    /**
     * male gender
     */
    const Single = 1;


    /**
     * @return int[]
     */
    public static function options(): array
    {
        return [
            self::Single,
            self::Married,
        ];
    }
}
