<?php


namespace App\System\Employee\Database\Models;


use App\System\AppBaseModel;
use App\System\Common\Database\Models\Document;
use Database\Factories\EmployeeHistoryFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class History extends AppBaseModel
{

    /**
     * @var string
     */
    protected $table = 'employee_histories';

    /**
     * @var string[]
     */
    protected $fillable = ['date', 'status', 'comment', 'employee_id'];

    /**
     * History constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'date';

    /**
     * @var array
     */
    protected array $sortableColumns = ['date', 'status', 'created_at'];

    /**
     * @var string[]
     */
    protected $with = [
        'document',
    ];

    /**
     * @return \Database\Factories\EmployeeHistoryFactory
     */
    protected static function newFactory(): EmployeeHistoryFactory
    {
        return EmployeeHistoryFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function employee():BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    /**
     * @return MorphOne
     */
    public function document(): MorphOne
    {
        return $this->morphOne(Document::class, 'documentary', 'documentary_type', 'documentary_id');
    }

}
