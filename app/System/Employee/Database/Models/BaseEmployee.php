<?php

namespace App\System\Employee\Database\Models;

use App\System\AppBaseModel;
use App\System\Attendance\Database\Models\Attendance;
use App\System\Attendance\Database\Models\AttendanceView;
use App\System\Common\Database\Models\Address;
use App\System\Common\Database\Models\Bank;
use App\System\Common\Database\Models\Contact;
use App\System\Common\Database\Models\Document;
use App\System\Leave\Database\Models\Leave;
use App\System\User\Database\Models\User;
use Database\Factories\EmployeeFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class BaseEmployee extends AppBaseModel
{
    /**
     * @var string
     */
    protected $table = 'vw_employees';

    /**
     * @var string[]
     */
    protected $hidden = [
        'email_verified_at',
        'is_locked',
        'user_id',
        'created_by',
        'updated_by',
        'deleted_by',
        'deleted_at'
    ];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'name';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['name', 'email', 'position', 'joined_at', 'created_at', 'status'];

    protected string $globalQueryName = 'employees';

    /**
     * EmployeeView constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return EmployeeFactory
     */
    protected static function newFactory(): EmployeeFactory
    {
        return EmployeeFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return MorphMany
     */
    public function addresses(): MorphMany
    {
        return $this->morphMany(Address::class, 'addressable', 'addressable_type', 'addressable_id');
    }

    /**
     * @return MorphMany
     */
    public function contacts(): MorphMany
    {
        return $this->morphMany(Contact::class, 'contactable', 'contactable_type', 'contactable_id');
    }

    /**
     * @return MorphMany
     */
    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentary', 'documentary_type', 'documentary_id');
    }

    /**
     * @return MorphOne
     */
    public function bank(): MorphOne
    {
        return $this->morphOne(Bank::class, 'bankable', 'bankable_type', 'bankable_id');
    }

    /**
     * @return HasMany
     */
    public function histories(): HasMany
    {
        return $this->hasMany(History::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function socialSecurities(): HasMany
    {
        return $this->hasMany(SocialSecurity::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function attendances(): HasMany
    {
        return $this->hasMany(Attendance::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function attends(): HasMany
    {
        return $this->hasMany(AttendanceView::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function allocatedLeaves(): HasMany
    {
        return $this->hasMany(AllocatedLeave::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function leaves(): HasMany
    {
        return $this->hasMany(Leave::class, 'employee_id', 'id');
    }

    /**
     * @return string
     */
    public function getMorphClass(): string
    {
        return 'Employee';
    }
}
