<?php

namespace App\System\Employee\Database\Models;

use App\System\Employee\Foundation\Scope\ManagerRole;

class Manager extends BaseEmployee
{
    protected string $globalQueryName = 'managers';


    protected static function boot(): void
    {
        parent::boot();
        static::addGlobalScope(new ManagerRole());
    }
}
