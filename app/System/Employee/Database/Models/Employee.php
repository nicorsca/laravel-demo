<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Employee\Database\Models;

use App\System\AppBaseModel;
use App\System\Attendance\Database\Models\Attendance;
use App\System\Common\Database\Models\Address;
use App\System\Common\Database\Models\Bank;
use App\System\Common\Database\Models\Contact;
use App\System\Common\Database\Models\Document;
use App\System\Department\Database\Models\Department;
use App\System\Leave\Database\Models\Leave;
use App\System\Project\Database\Models\Project;
use App\System\User\Database\Models\User;
use Database\Factories\EmployeeFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Employee extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'personal_email',
        'position',
        'joined_at',
        'detached_at',
        'gender',
        'dob',
        'marital_status',
        'pan_no',
        'user_id',
        'department_id'
    ];

    /**
     * @var array
     */
    protected array $sortableColumns = ['code', 'personal_email', 'created_at'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'code';

    protected string $globalQueryName = 'employees';

    /**
     * Employee constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['user_id', 'department_id']);
    }

    /**
     * @return \Database\Factories\EmployeeFactory
     */
    protected static function newFactory(): EmployeeFactory
    {
        return EmployeeFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    /**
     * @return MorphMany
     */
    public function addresses(): MorphMany
    {
        return $this->morphMany(Address::class, 'addressable', 'addressable_type', 'addressable_id');
    }

    /**
     * @return MorphMany
     */
    public function contacts(): MorphMany
    {
        return $this->morphMany(Contact::class, 'contactable', 'contactable_type', 'contactable_id');
    }

    /**
     * @return MorphMany
     */
    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentary', 'documentary_type', 'documentary_id');
    }

    /**
     * @return MorphOne
     */
    public function bank(): MorphOne
    {
        return $this->morphOne(Bank::class, 'bankable', 'bankable_type', 'bankable_id');
    }

    /**
     * @return HasMany
     */
    public function histories(): HasMany
    {
        return $this->hasMany(History::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function allocatedLeaves(): HasMany
    {
        return $this->hasMany(AllocatedLeave::class, 'employee_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function attendances(): HasMany
    {
        return $this->hasMany(Attendance::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaves(): HasMany
    {
        return $this->hasMany(Leave::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(Project::class, 'project_employees', 'employee_id',
            'project_id')->withPivot('role_id', 'licensed');
    }
}
