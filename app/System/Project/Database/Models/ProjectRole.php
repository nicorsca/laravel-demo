<?php

namespace App\System\Project\Database\Models;

use App\System\AppBaseModel;
use Database\Factories\ProjectRoleFactory;

class ProjectRole extends AppBaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['title'];

    /**
     * @return \Database\Factories\ProjectRoleFactory
     */
    protected static function newFactory(): ProjectRoleFactory
    {
        return ProjectRoleFactory::new();
    }
}
