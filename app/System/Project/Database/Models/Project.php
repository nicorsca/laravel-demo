<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Project\Database\Models;

use App\System\AppBaseModel;
use App\System\Client\Database\Models\Client;
use App\System\Common\Database\Models\Document;
use App\System\Employee\Database\Models\EmployeeView;
use Database\Factories\ProjectFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Project extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'code', 'type', 'avatar', 'status', 'start_date', 'end_date', 'client_id'];
    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['title', 'code', 'type', 'start_date'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['client_id']);
    }

    /**
     * @return ProjectFactory
     */
    protected static function newFactory(): ProjectFactory
    {
        return ProjectFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    /**
     * @return MorphMany
     */
    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentary', 'documentary_type', 'documentary_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employees(): BelongsToMany
    {
        return $this->belongsToMany(EmployeeView::class, 'project_employees', 'project_id', 'employee_id')
            ->withPivot(['role_id', 'licensed']);
    }
}
