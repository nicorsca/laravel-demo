<?php

namespace App\System\Project\Foundation;

class ProjectStatus
{
    const STATUS_ARCHIVE = 1;

    const STATUS_UNARCHIVE = 2;
}
