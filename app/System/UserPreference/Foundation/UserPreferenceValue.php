<?php

namespace App\System\UserPreference\Foundation;

class UserPreferenceValue
{
    public const EMPLOYEE_WORK_TIME_VALUE = '03:15-12:15';

    public const USER_TIMEZONE_VALUE = 'asia/kathmandu';

    public const DARK_THEME  = 'dark';
    public const LIGHT_THEME = 'light';

    public const GRID_VIEW  = 'grid';
    public const TABLE_VIEW = 'table';

    /**
     * @return string[]
     */
    public static function themeOptions(): array
    {
        return [
            self::DARK_THEME,
            self::LIGHT_THEME
        ];
    }

    /**
     * @return string[]
     */
    public static function listViewOptions(): array
    {
        return [
            self::GRID_VIEW,
            self::TABLE_VIEW
        ];
    }
}
