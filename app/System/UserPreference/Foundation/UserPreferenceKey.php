<?php

namespace App\System\UserPreference\Foundation;

class UserPreferenceKey
{

    const EMPLOYEE_WORK_TIME = 'employee_work_time';

    const USER_TIMEZONE = 'user_timezone';

    const USER_THEME = 'user_theme';

    const USER_LIST_VIEW = 'user_list_view';

    /**
     * @return string[]
     */
    public static function options(): array
    {
        return [
            self::EMPLOYEE_WORK_TIME,
            self::USER_TIMEZONE,
            self::USER_THEME,
            self::USER_LIST_VIEW
        ];
    }
}
