<?php


namespace App\System\Leave\Foundation;


class LeaveStatus
{
    public const DECLINED = 0;

    public const PENDING = 1;

    public const APPROVED = 2;

    public const CANCELLED = 3;

    /**
     * @return int[]
     */
    public static function options(): array
    {
        return [
            self::CANCELLED,
            self::APPROVED,
            self::PENDING,
            self::DECLINED,
        ];
    }
}
