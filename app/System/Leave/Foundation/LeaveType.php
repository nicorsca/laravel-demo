<?php


namespace App\System\Leave\Foundation;


class LeaveType
{
    public const SICK_LEAVE = 'sick';

    public const PERSONAL_LEAVE = 'personal';

    public const MATERNITY_LEAVE = 'maternity';

    public const PATERNITY_LEAVE = 'paternity';

    public const BEREAVEMENT_LEAVE = 'bereavement';

    public const COMPENSATORY_LEAVE = 'compensatory';

    public const UNPAID_LEAVE = 'unpaid';

    public const CANCELLATION_LEAVE = 'cancellation';
}
