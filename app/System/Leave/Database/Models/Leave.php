<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Leave\Database\Models;

use App\System\AppBaseModel;
use App\System\Employee\Database\Models\EmployeeView;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Database\Factories\LeaveFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Leave extends AppBaseModel
{
    use FiscalYearTrait;

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'start_at', 'end_at', 'days', 'type', 'status',
        'is_emergency', 'requested_to', 'employee_id', 'reason', 'parent_id', 'parent_type'];

    /**
     * @var string[]
     */
    protected $casts = [
        'is_emergency' => 'boolean',
    ];
    /**
     * @var string
     */
    protected string $defaultSortColumn = 'created_at';
    /**
     * @var string
     */
    protected string $defaultSortOrder = 'desc';

    /**
     * @var array
     */
    protected array $sortableColumns = ['title', 'created_at','start_at','end_at','status'];

    /**
     * @var string
     */
    protected string $globalQueryName = 'employee_relation';
    /**
     * Leave constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['requested_to', 'employee_id']);
    }

    /**
     * @return \Database\Factories\LeaveFactory
     */
    protected static function newFactory(): LeaveFactory
    {
        return LeaveFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function requested(): BelongsTo
    {
        return $this->belongsTo(EmployeeView::class, 'requested_to', 'id')->withoutGlobalScope(GlobalEmployeeScope::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(EmployeeView::class, 'employee_id', 'id');
    }
}
