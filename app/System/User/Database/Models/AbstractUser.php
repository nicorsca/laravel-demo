<?php


namespace App\System\User\Database\Models;


use App\System\AppBaseModel;
use App\System\User\Database\ResetPasswordNotification;
use Database\Factories\UserFactory;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class AbstractUser extends AppBaseModel implements AuthenticatableContract, CanResetPassword
{
    use Authenticatable, HasApiTokens, Notifiable, HasRoles;

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'email', 'email_verified_at', 'password', 'status', 'is_locked', 'remember_token', 'avatar'];

    /**
     * @var string
     */
    protected $table = "users";

    /**
     * @var string
     */
    protected string $guard_name = 'web';

    /**
     * AbstractUser constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['email_verified_at', 'password', 'remember_token', 'is_locked', 'roles']);
        $this->append(['role_name']);
    }

    /**
     * @return \Database\Factories\UserFactory
     */
    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }

    /**
     * @return string|null
     */
    public function getRoleNameAttribute(): ?string
    {
        return $this->getRoleNames()[0] ?? null;
    }

    /**
     * @return string
     */
    public function getEmailForPasswordReset(): string
    {
        return $this->email;
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
