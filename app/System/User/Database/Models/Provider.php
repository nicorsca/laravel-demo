<?php


namespace App\System\User\Database\Models;


use App\System\AppBaseModel;
use Database\Factories\ProviderFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use NicoSystem\Foundation\Database\BaseModel;

class Provider extends BaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['provider', 'provider_id', 'user_id', 'avatar', 'extra'];

    /**
     * @var string[]
     */
    protected $hidden = ['deleted_at', 'user_id'];

    /**
     * @var string[]
     */
    protected $casts = [
        'extra' => 'array',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return ProviderFactory
     */
    protected static function newFactory(): ProviderFactory
    {
        return ProviderFactory::new();
    }
}
