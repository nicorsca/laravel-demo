<?php

namespace App\System\User\Database;


use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends ResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $receiver = json_decode($notifiable);

        return (new MailMessage())
            ->subject(trans('reset_password_email.subject'))
            ->view(
                'emails.reset-password', [
                'url' => get_front_app_url(url('auth/reset-password')) . "?token={$this->token}&email={$receiver->email}",
                'token' => $this->token,
                'title' => trans('reset_password_email.title'),
                'email' => $receiver->email,
                'receiver' => ucfirst($receiver->first_name),
                ]
            );
    }
}
