<?php

namespace App\System\User\Foundation;

class RoleName
{
    public const MANAGER = 'manager';

    public const EMPLOYEE = 'employee';

    /**
     * @return string[]
     */
    public static function options(): array
    {
        return [
            self::MANAGER,
            self::EMPLOYEE,
        ];
    }
}
