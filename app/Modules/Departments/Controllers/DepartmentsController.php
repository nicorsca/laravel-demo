<?php

namespace App\Modules\Departments\Controllers;

use App\Modules\Departments\Interfaces\DepartmentInterface;
use App\Modules\Departments\Requests\DepartmentCreateRequest;
use App\Modules\Departments\Requests\DepartmentUpdateRequest;
use App\Modules\Departments\Requests\UpdateHodRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

/**
 * Class DepartmentsController
 *
 * @package App\Modules\Departments\Controllers
 */
class DepartmentsController extends BaseController
{
    /**
     * DepartmentsController constructor.
     *
     * @param DepartmentInterface $repository
     */
    public function __construct(private DepartmentInterface $repository)
    {
    }

    /**
     * @param  \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @return JsonResponse
     */
    public function index(NicoListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param  DepartmentCreateRequest $request
     * @return JsonResponse
     */
    public function store(DepartmentCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->except('user_id')));
    }

    /**
     * @param  DepartmentUpdateRequest $request
     * @param  string                  $id
     * @return JsonResponse
     */
    public function update(DepartmentUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->except('user_id')));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->toggleStatus($id));
    }

    /**
     * @param  \App\Modules\Departments\Requests\UpdateHodRequest $request
     * @param  string                                             $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateHod(UpdateHodRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->updateHod($id, $request->employee_id));
    }
}
