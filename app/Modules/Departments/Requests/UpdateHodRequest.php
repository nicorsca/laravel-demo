<?php


namespace App\Modules\Departments\Requests;


use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

class UpdateHodRequest extends NicoRequest
{

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'employee_id' => [
                'nullable',
                'integer',
                Rule::exists('employees', 'id')->whereNull('deleted_at')
            ],
        ];
    }

}
