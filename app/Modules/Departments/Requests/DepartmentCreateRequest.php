<?php
namespace App\Modules\Departments\Requests;

use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class DepartmentCreateRequest
 *
 * @package App\Modules\Departments\Requests
 */
class DepartmentCreateRequest extends NicoRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:50|unique:departments,title,NULL,id,deleted_at,NULL',
            'description' => 'required|string|max:500',
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }
}
