<?php


namespace App\Modules\Departments\Listeners;


use App\Events\Department\UpdateHod;
use App\Modules\Departments\Events\Deleting;
use App\Modules\Departments\Events\Updating;
use App\Modules\Departments\Mails\AssignHodMail;
use App\Modules\Departments\Mails\RevokeHodMail;
use App\Modules\Employees\Database\Models\EmployeeView;
use Illuminate\Support\Facades\Mail;
use NicoSystem\Exceptions\ResourceInUseException;

class DepartmentEventSubscriber
{

    /**
     * @param Updating $event
     */
    public function onDepartmentUpdating(Updating $event): void
    {
        $department = $event->department;
        $changedFields = $department->getDirty();
        if ($department->isDirty('status')) {
            unset($changedFields['status']);
            if ($department->employees()->count() > 0) {
                if (count($changedFields) > 0) {
                    $department->setAttribute('status', $department->getOriginal('status'));
                } else {
                    throw new ResourceInUseException(trans('responses.department.hod_not_exists'), 'err_employee_exists_exception');
                }
            }
        }
    }

    /**
     * @param Deleting $event
     */
    public function onDepartmentDeleting(Deleting $event): void
    {
        if ($event->department->employees()->count()) {
            throw new ResourceInUseException(trans('responses.department.employees_exists'), 'err_employee_exists_exception');
        }
    }

    /**
     * @param \App\Events\Department\UpdateHod $event
     */
    public function onUpdateHod(UpdateHod $event): void
    {
        $department = $event->department;
        if ($event->oldHodId) {
            $hod = EmployeeView::find($event->oldHodId);
            Mail::to($hod->email)->queue(new RevokeHodMail($department, $hod));
        }
        if ($event->newHodId) {
            $hod = EmployeeView::find($event->newHodId);
            Mail::to($hod->email)->queue(new AssignHodMail($department, $hod));
        }
    }

    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen(Updating::class, [DepartmentEventSubscriber::class, 'onDepartmentUpdating']);
        $events->listen(Deleting::class, [DepartmentEventSubscriber::class, 'onDepartmentDeleting']);
        $events->listen(UpdateHod::class, [DepartmentEventSubscriber::class, 'onUpdateHod']);
    }
}
