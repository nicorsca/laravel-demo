<?php

namespace App\Modules\Departments\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface DepartmentInterface
 *
 * @package App\Modules\Departments\Interfaces
 */
interface DepartmentInterface extends BasicCrudInterface
{

    /**
     * @param  string      $departmentId
     * @param  string|null $employeeId
     * @return void
     */
    public function updateHod(string $departmentId, string|null $employeeId): void;
}
