<?php


namespace App\Modules\Departments\Mails;


use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\EmployeeView;
use Illuminate\Mail\Mailable;

class RevokeHodMail extends Mailable
{
    /**
     * HodAssignMail constructor.
     *
     * @param \App\System\Department\Database\Models\Department $department
     * @param \App\System\Employee\Database\Models\EmployeeView $hod
     */
    public function __construct(private Department $department, private EmployeeView $hod)
    {
    }

    /**
     * @return \App\Modules\Departments\Mails\RevokeHodMail
     */
    public function build(): RevokeHodMail
    {
        return $this->subject(trans('revoke_hod.subject'))
            ->view(nico_view('emails.revoke_hod', [], [], true))->with(
                [
                'receiver' => explode(' ', $this->hod->name)[0],
                'department' => $this->department->title,
                'title' => trans('revoke_hod.title'),
                ]
            );
    }
}
