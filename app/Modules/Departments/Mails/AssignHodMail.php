<?php

namespace App\Modules\Departments\Mails;

use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\EmployeeView;
use Illuminate\Mail\Mailable;

class AssignHodMail extends Mailable
{

    /**
     * HodAssignMail constructor.
     *
     * @param \App\System\Department\Database\Models\Department $department
     * @param \App\System\Employee\Database\Models\EmployeeView $hod
     */
    public function __construct(private Department $department, private EmployeeView $hod)
    {
    }

    /**
     * @return \App\Modules\Departments\Mails\AssignHodMail
     */
    public function build(): AssignHodMail
    {
        return $this->subject(trans('assign_hod.subject'))
            ->view(nico_view('emails.assign_hod', [], [], true))->with(
                [
                'receiver' => explode(' ', $this->hod->name)[0],
                'department' => $this->department->title,
                'title' => trans('assign_hod.title'),
                ]
            );
    }
}
