<?php
namespace App\Modules\Departments\Database\Models;

use App\System\Department\Database\Models\Department as BaseModel;

/**
 * Class Department
 *
 * @package App\Modules\Departments\Database\Models
 */
class Department extends BaseModel
{
    /**
     * @var string[]
     */
    protected $withCount = [
        'employees'
    ];
}
