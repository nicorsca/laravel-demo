@extends('emails.base_email')

@section('mail_body')
    {!! trans('revoke_hod.body', compact('department')) !!}
@stop
