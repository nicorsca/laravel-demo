@extends('emails.base_email')

@section('mail_body')
    {!! trans('assign_hod.body', compact('department')) !!}
@stop
