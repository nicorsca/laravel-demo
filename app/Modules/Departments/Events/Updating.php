<?php


namespace App\Modules\Departments\Events;


use App\Modules\Departments\Database\Models\Department;

class Updating
{
    /**
     * Deleting constructor.
     *
     * @param \App\Modules\Departments\Database\Models\Department $department
     */
    public function __construct(public Department $department)
    {
    }
}
