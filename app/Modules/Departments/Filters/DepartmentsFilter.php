<?php

namespace App\Modules\Departments\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class DepartmentsController
 *
 * @package App\Modules\Departments\Filters
 */
class DepartmentsFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

}
