<?php


namespace App\Modules\Employees\Requests;


use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class BankUpdateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        $bankId = $this->route('bank');

        return [
            'name' => 'required|string|max:100',
            'branch' => 'required|string|max:100',
            'account_number' => [
                'required',
                'alpha_dash',
                'min:9',
                'max:30',
                Rule::unique('banks', 'account_number')
                    ->where('bankable_type', 'Employee')
                    ->whereNull('deleted_at')
                    ->ignore($bankId)
            ],
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }

}
