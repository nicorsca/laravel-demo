<?php


namespace App\Modules\Employees\Requests;


use App\System\Common\Foundation\AddressType;
use App\System\Common\Foundation\ContactType;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class AdditionalInfoUpdateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'contacts.*.number.phone' => 'Please specify valid format phone number'
        ];
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'addresses' => 'nullable|array|max:2',
            'addresses.*.id' => [
                'nullable',
                'integer',
                Rule::exists('addresses', 'id')
                    ->where(
                        function ($query) {
                            $query->whereNull('deleted_at');
                            $query->where('addressable_type', 'Employee');
                        }
                    )
            ],
            'addresses.*.country' => 'required|string|max:60',
            'addresses.*.state' => 'required|string|max:60',
            'addresses.*.city' => 'required|string|max:85',
            'addresses.*.street' => 'required|string|max:85',
            'addresses.*.zip_code' => 'required|integer|digits:5',
            'addresses.*.type' => 'required|distinct|in:' . implode(',', AddressType::options()),

            'contacts' => 'nullable|array',
            'contacts.*.id' => [
                'nullable',
                'integer',
                Rule::exists('contacts', 'id')
                    ->where(
                        function ($query) {
                            $query->whereNull('deleted_at');
                            $query->where('contactable_type', 'Employee');
                        }
                    )
            ],
            'contacts.*.number' => 'required|string|distinct|phone',
            'contacts.*.type' => 'required|in:' . implode(',', ContactType::options()),
            'contacts.*.status' => 'nullable|in:' . implode(',', Status::options()),
        ];
    }
}
