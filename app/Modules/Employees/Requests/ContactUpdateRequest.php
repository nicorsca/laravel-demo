<?php


namespace App\Modules\Employees\Requests;


use App\System\Common\Foundation\ContactType;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;

class ContactUpdateRequest extends ContactCreateRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $contactId = $this->route('contact');

        return [
            'number' => [
                'required',
                'string',
                'phone',
                'min:9',
                'max:15',
                Rule::unique('contacts', 'number')
                    ->where('contactable_type', 'Employee')
                    ->whereNull('deleted_at')
                    ->ignore($contactId)
            ],
            'type' => 'required|in:' . implode(',', ContactType::options()),
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }
}
