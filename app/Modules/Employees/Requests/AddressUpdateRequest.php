<?php


namespace App\Modules\Employees\Requests;


class AddressUpdateRequest extends AddressCreateRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        unset($rules['type']);

        return $rules;
    }
}
