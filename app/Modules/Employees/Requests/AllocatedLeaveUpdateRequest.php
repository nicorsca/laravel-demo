<?php
namespace App\Modules\Employees\Requests;

/**
 * Class LeaveUpdateRequest
 *
 * @package App\Modules\Leaves\Requests
 */
class AllocatedLeaveUpdateRequest extends AllocatedLeaveCreateRequest
{

    public function rules(): array
    {
        $rules = parent::rules();
        unset($rules['current_fiscal_year']);

        return $rules;
    }

}
