<?php
namespace App\Modules\Employees\Requests;

use App\Modules\Employees\Database\Models\Setting;
use App\System\Setting\Foundation\SettingKey;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class LeaveCreateRequest
 *
 * @package App\Modules\Leaves\Requests
 */
class AllocatedLeaveCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $type = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        return [
            'title' => 'nullable|string|max:100',
            'days' => 'required|integer|min:1|max:30',
            'current_fiscal_year' => 'required|boolean',
            'type' => 'required|in:' . implode(',', $type?->value ?? []),
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }
}
