<?php

namespace App\Modules\Employees\Requests;

use NicoSystem\Foundation\Requests\NicoListRequest;

class EmployeeListRequest extends NicoListRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['department_id'] = 'nullable|integer';

        return $rules;
    }
}
