<?php
namespace App\Modules\Employees\Requests;

use App\System\Employee\Foundation\Gender;
use App\System\Employee\Foundation\MaritalStatus;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class EmployeeCreateRequest
 *
 * @package App\Modules\Employees\Requests
 */
class EmployeeCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'email' => 'required|email:rfc,dns|max:255|unique:users,email,NULL,id,deleted_at,NULL|unique:employees,personal_email,NULL,id,deleted_at,NULL',
            'personal_email' => 'required|email:rfc,dns|max:255|unique:users,email,NULL,id,deleted_at,NULL|unique:employees,personal_email,NULL,id,deleted_at,NULL',
            'position' => 'required|string|max:50',
            'joined_at' => 'required|date_format:Y-m-d|before_or_equal:now',
            'gender' => 'required|in:' . implode(',', Gender::options()),
            'dob' => 'required|date_format:Y-m-d|before:now',
            'marital_status' =>  'required|in:' . implode(',', MaritalStatus::options()),
            'pan_no' => 'nullable|integer|digits:9',
            'department_id' => [
                'required',
                'integer',
                Rule::exists('departments', 'id')
                    ->whereNull('deleted_at')
                    ->where('status', Status::STATUS_PUBLISHED)
            ],
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(
            function ($validator) {
                if ($this->get('email') == $this->get('personal_email')) {
                    $validator->errors()->add('email', trans('responses.employee.diff_email_and_personal_email'));
                }
            }
        );
    }
}
