<?php


namespace App\Modules\Employees\Requests;


use App\System\Common\Foundation\ContactType;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class ContactCreateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'number.phone' => 'Please enter valid phone number',
            'number.string' => 'The phone number must be string',
            'number.min' => 'The phone number must be at least 9 characters',
            'number.max' => 'The phone number must not be greater than 15 characters'
        ];
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'number' => [
                'required',
                'string',
                'phone',
                'min:9',
                'max:15',
                Rule::unique('contacts', 'number')
                    ->where('contactable_type', 'Employee')
                    ->whereNull('deleted_at')
            ],
            'type' => 'required|in:' . implode(',', ContactType::options()),
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }
}
