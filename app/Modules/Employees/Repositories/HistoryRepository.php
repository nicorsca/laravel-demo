<?php


namespace App\Modules\Employees\Repositories;


use App\Events\Employee\History\Activated;
use App\Events\Employee\History\Deactivated;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\History;
use App\Modules\Employees\Filters\HistoryFilter;
use App\Modules\Employees\Interfaces\HistoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Repositories\BaseRepository;

class HistoryRepository extends BaseRepository implements HistoryInterface
{
    /**
     * @var Employee
     */
    private Employee $globalEmployee;

    /**
     * HistoryRepository constructor.
     *
     * @param Employee $employee
     * @param History  $history
     */
    public function __construct(private Employee $employee, History $history)
    {
        parent::__construct($history);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\HistoryFilter
     */
    public function getFilter(Builder $builder): HistoryFilter
    {
        return new HistoryFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where('employee_id', $this->globalEmployee->id);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getHistoryList(array $inputs, string $employeeId): LengthAwarePaginator|Collection
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($inputs);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return bool
     */
    public function deactivate(array $inputs, string $employeeId): bool
    {
        $employee = $this->employee->findOrfail($employeeId);
        if (!is_null($employee->detached_at)) {
            throw new NicoBadRequestException(trans('responses.employee.history.deactivated'), 'err_employee_already_deactivated');
        }
        $inputs['status'] = Employee::STATUS_INACTIVE;
        $inputs['date'] = Carbon::now()->format('Y-m-d');
        $inputs['employee_id'] = $employee->id;
        try {
            DB::beginTransaction();
            $history = parent::create($inputs);
            if (Arr::has($inputs, 'url') && !empty($inputs['url'])) {
                $inputs['title'] = $employee->code . '_deactivate';
                $history->document()->create(Arr::only($inputs, ['title', 'url', 'thumbnail']));
            }
            event(new Deactivated($employee, $history));
            DB::commit();
        } catch (QueryException | \Swift_SwiftException | \Exception $e) {
            DB::rollBack();
            throw new NicoException($e->getMessage());
        }

        return true;
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return bool
     */
    public function activate(array $inputs, string $employeeId): bool
    {
        $employee = $this->employee->findOrfail($employeeId);
        if (is_null($employee->detached_at)) {
            throw new NicoBadRequestException(trans('responses.employee.history.activated'), 'err_employee_already_activated');
        }
        $inputs['status'] = Employee::STATUS_ACTIVE;
        $inputs['date'] = Carbon::now()->format('Y-m-d');
        $inputs['employee_id'] = $employee->id;
        try {
            DB::beginTransaction();
            $history = parent::create($inputs);
            if (Arr::has($inputs, 'url') && !empty($inputs['url'])) {
                $inputs['title'] = $employee->code . '_activate';
                $history->document()->create(Arr::only($inputs, ['title', 'url', 'thumbnail']));
            }
            event(new Activated($employee, $history));
            DB::commit();
        } catch (QueryException | \Swift_SwiftException | \Exception $e) {
            DB::rollBack();
            throw new NicoException($e->getMessage());
        }

        return true;
    }
}
