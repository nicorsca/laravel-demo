<?php


namespace App\Modules\Employees\Repositories;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\SocialSecurity;
use App\Modules\Employees\Filters\SocialSecurityFilter;
use App\Modules\Employees\Interfaces\SocialSecurityInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use NicoSystem\Repositories\BaseRepository;

class SocialSecurityRepository extends BaseRepository implements SocialSecurityInterface
{
    /**
     * @var Employee
     */
    private Employee $globalEmployee;

    /**
     * SocialSecurityRepository constructor.
     *
     * @param \App\Modules\Employees\Database\Models\Employee       $employee
     * @param \App\Modules\Employees\Database\Models\SocialSecurity $socialSecurity
     */
    public function __construct(private Employee $employee, SocialSecurity $socialSecurity)
    {
        parent::__construct($socialSecurity);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\SocialSecurityFilter
     */
    public function getFilter(Builder $builder): SocialSecurityFilter
    {
        return new SocialSecurityFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where('employee_id', $this->globalEmployee->id);
    }

    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return \App\Modules\Employees\Database\Models\SocialSecurity
     */
    public function getSocialSecurityById(string $employeeId, string $securityId): SocialSecurity
    {
        $this->employee->findOrfail($employeeId);

        return $this->model->where('employee_id', $employeeId)->findOrfail($securityId);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getSocialSecurityList(array $inputs, string $employeeId): LengthAwarePaginator|Collection
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($inputs);
    }

    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return bool
     */
    public function destroySocialSecurity(string $employeeId, string $securityId): bool
    {
        $socialSecurity = $this->getSocialSecurityById($employeeId, $securityId);

        return $socialSecurity->delete();
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \App\Modules\Employees\Database\Models\SocialSecurity
     */
    public function createSocialSecurity(array $inputs, string $employeeId): SocialSecurity
    {
        $employee = $this->employee->findOrfail($employeeId);
        $inputs['employee_id'] = $employee->id;

        return parent::create($inputs);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $securityId
     * @return \App\Modules\Employees\Database\Models\SocialSecurity
     */
    public function updateSocialSecurity(array $inputs, string $employeeId, string $securityId): SocialSecurity
    {
        $this->getSocialSecurityById($employeeId, $securityId);

        return parent::update($securityId, Arr::only($inputs, ['title', 'number', 'start_date', 'end_date', 'status']));
    }


    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return \App\Modules\Employees\Database\Models\SocialSecurity
     */
    public function toggleSocialSecurityStatus(string $employeeId, string $securityId): SocialSecurity
    {
        $this->getSocialSecurityById($employeeId, $securityId);

        return parent::toggleStatus($securityId);
    }
}
