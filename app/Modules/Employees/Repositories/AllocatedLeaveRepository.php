<?php

namespace App\Modules\Employees\Repositories;

use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\AllocatedLeave;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Filters\AllocatedLeavesFilter;
use App\Modules\Employees\Interfaces\AllocatedLeaveInterface;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Foundation\Status;
use NicoSystem\Repositories\BaseRepository;

/**
 * Class LeaveRepository
 *
 * @package App\Modules\Leaves\Repositories
 */
class AllocatedLeaveRepository extends BaseRepository implements AllocatedLeaveInterface
{
    use FiscalYearTrait;

    /**
     * @var \App\Modules\Employees\Database\Models\Employee
     */
    private Employee $globalEmployee;

    /**
     * LeaveRepository constructor.
     *
     * @param \App\Modules\Employees\Database\Models\Employee $employee
     * @param AllocatedLeave $leave
     * @param \App\Modules\Employees\Database\Models\Setting $setting
     */
    public function __construct(private Employee $employee, AllocatedLeave $leave, private Setting $setting)
    {
        parent::__construct($leave);
    }

    /**
     * @param Builder $builder
     * @return AllocatedLeavesFilter
     */
    public function getFilter(Builder $builder): AllocatedLeavesFilter
    {
        return new AllocatedLeavesFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where('employee_id', $this->globalEmployee->id);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getLeaveList(array $inputs, string $employeeId): LengthAwarePaginator|Collection
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($inputs);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function createLeave(array $inputs, string $employeeId): AllocatedLeave
    {
        $employee = $this->employee->findOrfail($employeeId);
        $inputs['employee_id'] = $employee->id;
        $fiscalDates = $inputs['current_fiscal_year'] ? $this->getStartAndEndFiscalDate() : $this->getStartAndEndFiscalDate(false);
        $inputs = array_merge($inputs, $fiscalDates);

        return parent::create($inputs);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @param string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function updateLeave(array $inputs, string $employeeId, string $leaveId): AllocatedLeave
    {
        $leave = $this->getLeaveById($employeeId, $leaveId);
        $this->canUpdateAllocatedLeave($leave);

        return parent::update($leaveId, $inputs);
    }

    /**
     * @param string $employeeId
     * @param string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function getLeaveById(string $employeeId, string $leaveId): AllocatedLeave
    {
        $this->employee->findOrfail($employeeId);

        return $this->model->where('employee_id', $employeeId)->findOrfail($leaveId);
    }

    /**
     * @param \App\Modules\Employees\Database\Models\AllocatedLeave $leave
     * @return bool
     */
    private function canUpdateAllocatedLeave(AllocatedLeave $leave): bool
    {
        $currentFiscalDates = $this->getStartAndEndFiscalDate();
        if ($leave->status == Status::STATUS_UNPUBLISHED || Carbon::parse($currentFiscalDates['start_date'])->lessThan($leave->start_date)) {
            return true;
        }

        throw new NicoBadRequestException(trans('responses.employee.allocated_leave.not_updated'), 'err_allocated_leave_not_updatable');
    }

    /**
     * @param string $employeeId
     * @param string $leaveId
     * @return bool
     */
    public function destroyLeave(string $employeeId, string $leaveId): bool
    {
        $leave = $this->getLeaveById($employeeId, $leaveId);
        $this->canUpdateAllocatedLeave($leave);

        return $leave->delete();
    }

    /**
     * @param string $employeeId
     * @param string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function toggleLeaveStatus(string $employeeId, string $leaveId): AllocatedLeave
    {
        $leave = $this->getLeaveById($employeeId, $leaveId);
        $this->canUpdateAllocatedLeave($leave);

        return parent::toggleStatus($leaveId);
    }

}
