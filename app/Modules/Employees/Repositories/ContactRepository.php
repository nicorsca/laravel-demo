<?php


namespace App\Modules\Employees\Repositories;


use App\Modules\Employees\Database\Models\Contact;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Filters\ContactsFilter;
use App\Modules\Employees\Interfaces\ContactInterface;
use App\System\Common\Repositories\BaseCommonRepository;
use App\System\Common\Database\Models\Contact as SystemContact;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ContactRepository extends BaseCommonRepository implements ContactInterface
{
    /**
     * @var \App\Modules\Employees\Database\Models\Employee
     */
    private Employee $globalEmployee;

    /**
     * ContactRepository constructor.
     *
     * @param Employee                                       $employee
     * @param \App\Modules\Employees\Database\Models\Contact $contact
     */
    public function __construct(private Employee $employee, Contact $contact)
    {
        parent::__construct($contact);
    }

    /**
     * @param string $employeeId
     * @param array  $inputs
     */
    public function updateContacts(string $employeeId, array $inputs): void
    {
        parent::updateRelations($this->employee, 'contacts', $employeeId, $inputs);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\ContactsFilter
     */
    public function getFilter(Builder $builder): ContactsFilter
    {
        return new ContactsFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where(
            [
            'contactable_id' => $this->globalEmployee->id,
            'contactable_type' => $this->employee->getMorphClass(),
            ]
        );
    }

    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function getContactList(array $params, string $employeeId): Collection|LengthAwarePaginator
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($params);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \App\Modules\Employees\Database\Models\Contact
     */
    public function createContact(array $inputs, string $employeeId): Contact
    {
        $this->employee->findOrfail($employeeId);
        $inputs['contactable_id'] = $employeeId;
        $inputs['contactable_type'] = $this->employee->getMorphClass();

        return parent::create($inputs);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $contactId
     * @return \App\Modules\Employees\Database\Models\Contact
     */
    public function updateContact(array $inputs, string $employeeId, string $contactId): Contact
    {
        $this->getContactById($employeeId, $contactId);

        return parent::update($contactId, $inputs);
    }

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return bool
     */
    public function destroyContact(string $employeeId, string $contactId): bool
    {
        $contact = $this->getContactById($employeeId, $contactId);

        return $contact->delete();
    }

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return SystemContact
     */
    public function getContactById(string $employeeId, string $contactId): SystemContact
    {
        $employee = $this->employee->findOrfail($employeeId);

        return $employee->contacts()->findOrfail($contactId);
    }

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return \App\Modules\Employees\Database\Models\Contact
     */
    public function toggleContactStatus(string $employeeId, string $contactId): Contact
    {
        $this->getContactById($employeeId, $contactId);

        return parent::toggleStatus($contactId);
    }
}
