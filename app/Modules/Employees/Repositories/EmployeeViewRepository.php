<?php


namespace App\Modules\Employees\Repositories;


use App\Modules\Employees\Database\Models\EmployeeView;
use App\Modules\Employees\Filters\EmployeesFilter;
use App\Modules\Employees\Interfaces\EmployeeViewInterface;
use Illuminate\Database\Eloquent\Builder;
use NicoSystem\Repositories\BaseRepository;

class EmployeeViewRepository extends BaseRepository implements EmployeeViewInterface
{

    /**
     * EmployeeViewRepository constructor.
     *
     * @param EmployeeView $employee
     */
    public function __construct(EmployeeView $employee)
    {
        parent::__construct($employee);
    }

    /**
     * @param Builder $builder
     *
     * @return EmployeesFilter
     */
    public function getFilter(Builder $builder): EmployeesFilter
    {
        return new EmployeesFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->select('id', 'name', 'email', 'status', 'avatar', 'code', 'position', 'joined_at', 'department',
            'department_id', 'created_at');
    }

    /**
     * @param string $id
     * @param array $attributes
     *
     * @return EmployeeView
     */
    public function getById(string $id, array $attributes = []): EmployeeView
    {
        return $this->model->with('addresses', 'contacts', 'bank')->findOrFail($id);
    }
}
