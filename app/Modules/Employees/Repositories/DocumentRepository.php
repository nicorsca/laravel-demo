<?php


namespace App\Modules\Employees\Repositories;


use App\Events\Employee\Document\Created;
use App\Events\Employee\Document\Updated;
use App\Modules\Employees\Database\Models\Document;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Filters\DocumentsFilter;
use App\Modules\Employees\Interfaces\DocumentInterface;
use App\System\Common\Database\Models\Document as SystemDocument;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Repositories\BaseRepository;

class DocumentRepository extends BaseRepository implements DocumentInterface
{
    /**
     * @var \App\Modules\Employees\Database\Models\Employee
     */
    private Employee $globalEmployee;

    /**
     * @var array|string[]
     */
    protected array $events = [
        'created' => Created::class,
        'updated' => Updated::class,
    ];

    /**
     * DocumentRepository constructor.
     *
     * @param Employee                                        $employee
     * @param \App\Modules\Employees\Database\Models\Document $document
     */
    public function __construct(
        protected Employee $employee,
        protected Document $document
    ) {
        parent::__construct($document);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\DocumentsFilter
     */
    public function getFilter(Builder $builder): DocumentsFilter
    {
        return new DocumentsFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where(
            [
            'documentary_id' => $this->globalEmployee->id,
            'documentary_type' => $this->employee->getMorphClass(),
            ]
        );
    }

    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function getDocumentList(array $params, string $employeeId): Collection|LengthAwarePaginator
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($params);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @return \App\Modules\Employees\Database\Models\Document
     */
    public function createDocument(array $inputs, string $employeeId): Document
    {
        $this->employee->findOrfail($employeeId);
        $inputs['documentary_id'] = $employeeId;
        $inputs['documentary_type'] = $this->employee->getMorphClass();

        return parent::create($inputs);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @param string $documentId
     * @return \App\Modules\Employees\Database\Models\Document
     */
    public function updateDocument(array $inputs, string $employeeId, string $documentId): Document
    {
        $this->getDocumentById($employeeId, $documentId);

        return parent::update($documentId, $inputs);
    }

    /**
     * @param  string $employeeId
     * @param  string $documentId
     * @return boolean
     */
    public function destroyDocument(string $employeeId, string $documentId): bool
    {
        $document = $this->getDocumentById($employeeId, $documentId);

        return $document->delete();
    }

    /**
     * @param  string $employeeId
     * @param  string $documentId
     * @return SystemDocument
     */
    public function getDocumentById(string $employeeId, string $documentId): SystemDocument
    {
        $employee = $this->employee->findOrfail($employeeId);

        return $employee->documents()->findOrfail($documentId);
    }

    /**
     * @param string $employeeId
     * @param string $documentId
     * @return \App\Modules\Employees\Database\Models\Document
     */
    public function toggleDocumentStatus(string $employeeId, string $documentId): Document
    {
        $this->getDocumentById($employeeId, $documentId);

        return parent::toggleStatus($documentId);
    }
}
