<?php

namespace App\Modules\Employees\Repositories;

use App\Modules\Employees\Database\Models\Manager;
use App\Modules\Employees\Filters\EmployeesFilter;
use App\Modules\Employees\Interfaces\ManagerInterface;
use Illuminate\Database\Eloquent\Builder;
use NicoSystem\Repositories\BaseRepository;

class ManagerRepository extends BaseRepository implements ManagerInterface
{
    /**
     * ManagerRepository constructor.
     * @param Manager $model
     */
    public function __construct(Manager $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return EmployeesFilter
     */
    public function getFilter(Builder $builder): EmployeesFilter
    {
        return new EmployeesFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->select('id', 'name', 'email', 'status', 'avatar', 'code', 'position', 'joined_at', 'department', 'created_at');
    }

}
