<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Interfaces\DocumentInterface;
use App\Modules\Employees\Requests\DocumentCreateRequest;
use App\Modules\Employees\Requests\DocumentUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class DocumentsController extends BaseController
{
    /**
     * DocumentsController constructor.
     *
     * @param DocumentInterface $repository
     */
    public function __construct(private DocumentInterface $repository)
    {
    }

    /**
     * @param  \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param  string                                          $employeeId
     * @return JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getDocumentList($request->all(), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\DocumentCreateRequest $request
     * @param  string                                                $employeeId
     * @return JsonResponse
     */
    public function store(DocumentCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createDocument($request->only('title', 'url', 'thumbnail', 'status','type'), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\DocumentUpdateRequest $request
     * @param  string                                                $employeeId
     * @param  string                                                $documentId
     * @return JsonResponse
     */
    public function update(DocumentUpdateRequest $request, string $employeeId, string $documentId): JsonResponse
    {
        return $this->responseOk($this->repository->updateDocument($request->only('title', 'url', 'thumbnail', 'status','type'), $employeeId, $documentId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $documentId
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $documentId): JsonResponse
    {
        return $this->responseOk($this->repository->destroyDocument($employeeId, $documentId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  $documentId
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, $documentId): JsonResponse
    {
        return $this->responseOk($this->repository->getDocumentById($employeeId, $documentId));
    }

    /**
     * @param NicoRequest $request
     * @param string $employeeId
     * @param string $documentId
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $employeeId, string $documentId): JsonResponse
    {
        return $this->responseOk($this->repository->toggleDocumentStatus($employeeId, $documentId));
    }
}
