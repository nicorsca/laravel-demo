<?php

namespace App\Modules\Employees\Controllers;

use App\Modules\Employees\Interfaces\ManagerInterface;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

class ManagersController extends BaseController
{
    /**
     * ManagersController constructor.
     * @param ManagerInterface $repository
     */
    public function __construct(private ManagerInterface $repository)
    {
    }

    /**
     * @param NicoRequest $request
     * @return JsonResponse
     */
    public function index(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }
}
