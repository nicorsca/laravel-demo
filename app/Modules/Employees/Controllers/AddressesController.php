<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Interfaces\AddressInterface;
use App\Modules\Employees\Requests\AddressCreateRequest;
use App\Modules\Employees\Requests\AddressUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class AddressesController extends BaseController
{

    /**
     * AddressesController constructor.
     *
     * @param \App\Modules\Employees\Interfaces\AddressInterface $repository
     */
    public function __construct(private AddressInterface $repository)
    {
    }

    /**
     * @param  \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param  string                                          $employeeId
     * @return JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getAddressList($request->all(), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\AddressCreateRequest $request
     * @param  string                                               $employeeId
     * @return JsonResponse
     */
    public function store(AddressCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createAddress($request->only(['country', 'state', 'city', 'street', 'zip_code', 'type']), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\AddressUpdateRequest $request
     * @param  string                                               $employeeId
     * @param  string                                               $addressId
     * @return JsonResponse
     */
    public function update(AddressUpdateRequest $request, string $employeeId, string $addressId): JsonResponse
    {
        return $this->responseOk($this->repository->updateAddress($request->only(['country', 'state', 'city', 'street', 'zip_code']), $employeeId, $addressId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $addressId
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $addressId): JsonResponse
    {
        return $this->responseOk($this->repository->destroyAddress($employeeId, $addressId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  $addressId
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, $addressId): JsonResponse
    {
        return $this->responseOk($this->repository->getAddressById($employeeId, $addressId));
    }
}
