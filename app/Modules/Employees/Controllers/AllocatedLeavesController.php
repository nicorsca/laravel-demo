<?php

namespace App\Modules\Employees\Controllers;

use App\Modules\Employees\Interfaces\AllocatedLeaveInterface;
use App\Modules\Employees\Requests\AllocatedLeaveCreateRequest;
use App\Modules\Employees\Requests\AllocatedLeaveUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

/**
 * Class LeavesController
 *
 * @package App\Modules\Leaves\Controllers
 */
class AllocatedLeavesController extends BaseController
{
    /**
     * LeavesController constructor.
     *
     * @param AllocatedLeaveInterface $repository
     */
    public function __construct(private AllocatedLeaveInterface $repository)
    {
    }

    /**
     * @param NicoListRequest $request
     * @param string $employeeId
     *
     * @return JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getLeaveList($request->all(), $employeeId));
    }

    /**
     * @param AllocatedLeaveCreateRequest $request
     * @param string $employeeId
     *
     * @return JsonResponse
     */
    public function store(AllocatedLeaveCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createLeave($request->only('title', 'days', 'type', 'status',
            'current_fiscal_year'), $employeeId));
    }

    /**
     * @param AllocatedLeaveUpdateRequest $request
     * @param string $employeeId
     * @param string $leaveId
     *
     * @return JsonResponse
     */
    public function update(AllocatedLeaveUpdateRequest $request, string $employeeId, string $leaveId): JsonResponse
    {
        return $this->responseOk($this->repository->updateLeave($request->only('title', 'days', 'type', 'status'),
            $employeeId, $leaveId));
    }

    /**
     * @param NicoRequest $request
     * @param string $employeeId
     * @param string $leaveId
     *
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $leaveId): JsonResponse
    {
        return $this->responseOk($this->repository->destroyLeave($employeeId, $leaveId));
    }

    /**
     * @param NicoRequest $request
     * @param string $employeeId
     * @param string $leaveId
     *
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, string $leaveId): JsonResponse
    {
        return $this->responseOk($this->repository->getLeaveById($employeeId, $leaveId));
    }

    /**
     * @param NicoRequest $request
     * @param string $employeeId
     * @param string $leaveId
     *
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $employeeId, string $leaveId): JsonResponse
    {
        return $this->responseOk($this->repository->toggleLeaveStatus($employeeId, $leaveId));
    }
}
