<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Exceptions\EmployeeDeactivateException;
use App\Modules\Employees\Interfaces\HistoryInterface;
use App\Modules\Employees\Requests\HistoryActivateRequest;
use App\Modules\Employees\Requests\HistoryDeactivateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;

class HistoryController extends BaseController
{

    /**
     * HistoryController constructor.
     *
     * @param HistoryInterface $repository
     */
    public function __construct(private HistoryInterface $repository)
    {
    }

    /**
     * @param \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param string $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getHistoryList($request->all(), $employeeId));
    }

    /**
     * @param \App\Modules\Employees\Requests\HistoryDeactivateRequest $request
     * @param string $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactivate(HistoryDeactivateRequest $request, string $employeeId): JsonResponse
    {
        $authUser = Auth::user();
        if ($authUser->employee?->id == $employeeId) {
            throw new EmployeeDeactivateException(trans('responses.employee.history.cannot_deactivate'));
        }
        return $this->responseOk($this->repository->deactivate($request->all(), $employeeId));
    }

    /**
     * @param \App\Modules\Employees\Requests\HistoryActivateRequest $request
     * @param string $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function activate(HistoryActivateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->activate($request->all(), $employeeId));
    }
}
