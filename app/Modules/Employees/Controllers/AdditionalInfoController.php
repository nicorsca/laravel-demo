<?php


namespace App\Modules\Employees\Controllers;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Interfaces\AddressInterface;
use App\Modules\Employees\Interfaces\ContactInterface;
use App\Modules\Employees\Interfaces\EmployeeViewInterface;
use App\Modules\Employees\Requests\AdditionalInfoUpdateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use NicoSystem\Controllers\BaseController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AdditionalInfoController extends BaseController
{
    public function __construct(
        private EmployeeViewInterface $employeeRepository,
        private ContactInterface $contactRepository,
        private AddressInterface $addressRepository,
    ) {
    }

    /**
     * @param  \App\Modules\Employees\Requests\AdditionalInfoUpdateRequest $request
     * @param  string                                                      $employeeId
     * @return JsonResponse
     */
    public function update(AdditionalInfoUpdateRequest $request, string $employeeId): JsonResponse
    {
        try {
            DB::beginTransaction();
            $this->contactRepository->updateContacts($employeeId, $request->contacts ?? []);
            $this->addressRepository->updateAddresses($employeeId, $request->addresses ?? []);
            DB::commit();
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            throw new ModelNotFoundException($exception->getMessage());
        } catch (ResourceExistsException $exception) {
            DB::rollBack();
            throw new ResourceExistsException($exception->getMessage());
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new BadRequestException();
        }
        return $this->responseOk($this->employeeRepository->getById($employeeId));
    }
}
