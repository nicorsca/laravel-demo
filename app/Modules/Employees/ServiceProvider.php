<?php
namespace App\Modules\Employees;

use App\Modules\Employees\Providers\EventServiceProvider;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * @package App\Modules\Employees
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Employees\Interfaces\EmployeeInterface', 'App\Modules\Employees\Repositories\EmployeeRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\EmployeeViewInterface', 'App\Modules\Employees\Repositories\EmployeeViewRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\AddressInterface', 'App\Modules\Employees\Repositories\AddressRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\ContactInterface', 'App\Modules\Employees\Repositories\ContactRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\BankInterface', 'App\Modules\Employees\Repositories\BankRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\DocumentInterface', 'App\Modules\Employees\Repositories\DocumentRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\HistoryInterface', 'App\Modules\Employees\Repositories\HistoryRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\SocialSecurityInterface', 'App\Modules\Employees\Repositories\SocialSecurityRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\AllocatedLeaveInterface', 'App\Modules\Employees\Repositories\AllocatedLeaveRepository');
        $this->app->bind('App\Modules\Employees\Interfaces\ManagerInterface', 'App\Modules\Employees\Repositories\ManagerRepository');
        $this->app->register(EventServiceProvider::class);
    }
}
