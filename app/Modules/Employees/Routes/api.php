<?php

use App\Modules\Employees\Controllers\AdditionalInfoController;
use App\Modules\Employees\Controllers\AllocatedLeavesController;
use App\Modules\Employees\Controllers\BanksController;
use App\Modules\Employees\Controllers\ContactsController;
use App\Modules\Employees\Controllers\DocumentsController;
use App\Modules\Employees\Controllers\HistoryController;
use App\Modules\Employees\Controllers\ManagersController;
use App\Modules\Employees\Controllers\SocialSecuritiesController;
use Illuminate\Support\Facades\Route;

Route::group(
    ['middleware' => ['authApi']], function () {
    Route::get('employees/managers', [ManagersController::class, 'index']);
    Route::resource('employees', 'EmployeesController')->only('index', 'store', 'show', 'update');
    Route::group(
        ['prefix' => 'employees/{employee}'], function () {
        Route::group(['middleware' => ['role:manager']], function () {
            Route::put('additional-info', [AdditionalInfoController::class, 'update']);
            Route::resource('addresses', 'AddressesController')->only('store', 'update', 'destroy');
            Route::put('contacts/{contact}/status', [ContactsController::class, 'toggleStatus']);
            Route::resource('contacts', 'ContactsController')->only('store', 'update', 'destroy');
            Route::put('banks/{bank}/status', [BanksController::class, 'toggleStatus']);
            Route::resource('banks', 'BanksController')->only('store', 'update', 'destroy');
            Route::put('documents/{document}/status', [DocumentsController::class, 'toggleStatus']);
            Route::resource('documents', 'DocumentsController')->only('store', 'update', 'destroy');
            Route::put('allocated-leaves/{allocated_leave}/status', [AllocatedLeavesController::class, 'toggleStatus']);
            Route::resource('allocated-leaves', 'AllocatedLeavesController')->only('store', 'update', 'destroy');
            Route::put('activate', [HistoryController::class, 'activate']);
            Route::put('deactivate', [HistoryController::class, 'deactivate']);
            Route::put('social-securities/{social_security}/status',
                [SocialSecuritiesController::class, 'toggleStatus']);
            Route::resource('social-securities', 'SocialSecuritiesController')->only('store', 'update', 'destroy');

        });
        Route::resource('addresses', 'AddressesController')->only('index', 'show');
        Route::resource('contacts', 'ContactsController')->only('index', 'show');
        Route::resource('banks', 'BanksController')->only('show', 'index');
        Route::resource('documents', 'DocumentsController')->only('index', 'show');
        Route::resource('allocated-leaves', 'AllocatedLeavesController')->only('index', 'show');
        Route::get('histories', [HistoryController::class, 'index']);
        Route::resource('social-securities', 'SocialSecuritiesController')->only('index', 'show');
    }
    );
}
);
