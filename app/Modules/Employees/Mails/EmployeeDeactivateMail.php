<?php


namespace App\Modules\Employees\Mails;


use App\System\Employee\Database\Models\History;
use App\System\User\Database\Models\User;
use Illuminate\Mail\Mailable;

class EmployeeDeactivateMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param User    $user
     * @param History $history
     */
    public function __construct(private User $user, private History $history)
    {
    }

    /**
     * @return \App\Modules\Employees\Mails\EmployeeDeactivateMail
     */
    public function build(): EmployeeDeactivateMail
    {
        return $this->subject(trans('employee_deactivate.subject'))
            ->view(nico_view('emails.employee_deactivate', [], [], true))->with(
                [
                'receiver' => $this->user->first_name,
                'username' => $this->user->email,
                'comment' => $this->history->comment,
                'title' => trans('employee_deactivate.title'),
                ]
            );
    }
}
