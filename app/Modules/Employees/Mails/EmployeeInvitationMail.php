<?php


namespace App\Modules\Employees\Mails;


use App\System\User\Database\Models\User;
use Illuminate\Mail\Mailable;

class EmployeeInvitationMail extends Mailable
{
    /**
     * EmployeeInvitationMail constructor.
     *
     * @param User   $user
     * @param string $password
     */
    public function __construct(private User $user, private string $password)
    {
    }

    /**
     * @return EmployeeInvitationMail
     */
    public function build(): EmployeeInvitationMail
    {
        return $this->subject(trans('employee_invitation.subject'))
            ->view(nico_view('emails.employee_invitation', [], [], true))->with(
                [
                'receiver' => $this->user->first_name,
                'username' => $this->user->email,
                'url' => get_front_app_url(url('/')),
                'loginUrl' => get_front_app_url(url('/login')),
                'password' => $this->password,
                'title' => trans('employee_invitation.title'),
                ]
            );
    }
}
