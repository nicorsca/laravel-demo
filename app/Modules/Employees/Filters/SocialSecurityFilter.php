<?php


namespace App\Modules\Employees\Filters;


use NicoSystem\Filters\BaseFilter;

class SocialSecurityFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

    /**
     * @param string $number
     */
    public function number(string $number = ''): void
    {
        if ($number !== '') {
            $this->builder->where('number', 'like', "%{$number}%");
        }
    }
}
