<?php

namespace App\Modules\Employees\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class LeavesController
 *
 * @package App\Modules\Leaves\Filters
 */
class AllocatedLeavesFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type != '' || $type != null) {
            $this->builder->where('type', $type);
        }
    }

}
