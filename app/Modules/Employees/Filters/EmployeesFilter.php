<?php

namespace App\Modules\Employees\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class EmployeesController
 *
 * @package App\Modules\Employees\Filters
 */
class EmployeesFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        $this->name($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        $this->name($title);
    }

    /**
     * @param string $name
     */
    public function name(string $name = ''): void
    {
        if ($name !== '') {
            $this->builder->where(
                function ($query) use ($name) {
                    $query->orWhere('name', 'like', '%' . $name . '%')
                        ->orWhere('email', 'like', '%'.$name.'%');
                }
            );
        }
    }

    /**
     * @param string $title
     */
    public function department(string $title = ''): void
    {
        if ($title !== '') {
            $this->builder->where('department', 'like', "%{$title}%");
        }
    }

    /**
     * @param int|null $departmentId
     */
    public function department_id(int $departmentId = null): void
    {
        if ($departmentId) {
            $this->builder->where('department_id', $departmentId);
        }
    }

    /**
     * @param string $code
     */
    public function code(string $code = ''): void
    {
        if ($code !== '') {
            $this->builder->where('code', 'like', "%{$code}%");
        }
    }
}
