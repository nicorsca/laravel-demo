<?php


namespace App\Modules\Employees\Interfaces;


use App\Modules\Employees\Database\Models\Bank;
use App\System\Common\Database\Models\Bank as SystemBank;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface BankInterface
{
    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return Bank
     */
    public function createBank(array $inputs, string $employeeId): Bank;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $bankId
     * @return Bank
     */
    public function updateBank(array $inputs, string $employeeId, string $bankId): Bank;

    /**
     * @param  string $employeeId
     * @param  string $bankId
     * @return boolean
     */
    public function destroyBank(string $employeeId, string $bankId): bool;

    /**
     * @param  string $employeeId
     * @param  string $bankId
     * @return SystemBank
     */
    public function getBankById(string $employeeId, string $bankId): SystemBank;

    /**
     * @param array $params
     * @param string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getBankList(array $params, string $employeeId): LengthAwarePaginator|Collection;

    /**
     * @param string $employeeId
     * @param string $bankId
     * @return \App\Modules\Employees\Database\Models\Bank
     */
    public function toggleBankStatus(string $employeeId, string $bankId): Bank;

}
