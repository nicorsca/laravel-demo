<?php


namespace App\Modules\Employees\Interfaces;


use App\Modules\Employees\Database\Models\Address;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface AddressInterface
{
    /**
     * @param string $employeeId
     * @param array  $inputs
     */
    public function updateAddresses(string $employeeId, array $inputs): void;

    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return Collection|LengthAwarePaginator
     */
    public function getAddressList(array $params, string $employeeId): Collection|LengthAwarePaginator;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return Address
     */
    public function createAddress(array $inputs, string $employeeId): Address;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $addressId
     * @return Address
     */
    public function updateAddress(array $inputs, string $employeeId, string $addressId): Address;

    /**
     * @param  string $employeeId
     * @param  string $addressId
     * @return mixed
     */
    public function destroyAddress(string $employeeId, string $addressId): bool;

    /**
     * @param  string $employeeId
     * @param  string $addressId
     * @return \App\System\Common\Database\Models\Address
     */
    public function getAddressById(string $employeeId, string $addressId): \App\System\Common\Database\Models\Address;

}
