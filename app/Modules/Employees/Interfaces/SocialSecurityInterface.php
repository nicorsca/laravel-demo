<?php


namespace App\Modules\Employees\Interfaces;


use App\Modules\Employees\Database\Models\SocialSecurity;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface SocialSecurityInterface
{
    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return \App\Modules\Employees\Database\Models\SocialSecurity
     */
    public function getSocialSecurityById(string $employeeId, string $securityId): SocialSecurity;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getSocialSecurityList(array $inputs, string $employeeId): LengthAwarePaginator|Collection;

    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return boolean
     */
    public function destroySocialSecurity(string $employeeId, string $securityId): bool;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return SocialSecurity
     */
    public function createSocialSecurity(array $inputs, string $employeeId): SocialSecurity;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $securityId
     * @return SocialSecurity
     */
    public function updateSocialSecurity(array $inputs, string $employeeId, string $securityId): SocialSecurity;

    /**
     * @param  string $employeeId
     * @param  string $securityId
     * @return SocialSecurity
     */
    public function toggleSocialSecurityStatus(string $employeeId, string $securityId): SocialSecurity;
}
