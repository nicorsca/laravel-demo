<?php


namespace App\Modules\Employees\Interfaces;


use NicoSystem\Interfaces\BasicCrudInterface;

interface EmployeeViewInterface extends BasicCrudInterface
{

}
