<?php


namespace App\Modules\Employees\Interfaces;


use App\Modules\Employees\Database\Models\Contact;
use App\System\Common\Database\Models\Contact as SystemContact;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface ContactInterface
{
    /**
     * @param string $employeeId
     * @param array  $inputs
     */
    public function updateContacts(string $employeeId, array $inputs): void;


    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return Collection|LengthAwarePaginator
     */
    public function getContactList(array $params, string $employeeId): Collection|LengthAwarePaginator;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return Contact
     */
    public function createContact(array $inputs, string $employeeId): Contact;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $contactId
     * @return Contact
     */
    public function updateContact(array $inputs, string $employeeId, string $contactId): Contact;

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return mixed
     */
    public function destroyContact(string $employeeId, string $contactId): bool;

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return SystemContact
     */
    public function getContactById(string $employeeId, string $contactId): SystemContact;

    /**
     * @param  string $employeeId
     * @param  string $contactId
     * @return Contact
     */
    public function toggleContactStatus(string $employeeId, string $contactId): Contact;
}
