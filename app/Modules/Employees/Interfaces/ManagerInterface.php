<?php

namespace App\Modules\Employees\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

interface ManagerInterface extends BasicCrudInterface
{

}
