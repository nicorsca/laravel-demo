@extends('emails.base_email')

@section('mail_body')
    {!! trans('employee_invitation.body',compact('url','username','password','loginUrl')) !!}
@stop
