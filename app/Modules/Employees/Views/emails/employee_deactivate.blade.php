@extends('emails.base_email')

@section('mail_body')
    {!! trans('employee_deactivate.body', compact('username','comment')) !!}
@stop
