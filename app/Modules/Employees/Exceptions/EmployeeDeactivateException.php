<?php

namespace App\Modules\Employees\Exceptions;

use NicoSystem\Exceptions\NicoException;

class EmployeeDeactivateException extends NicoException
{

    protected $code = 422;

    protected string $respCode = 'err_employee_deactivate_exception';
}
