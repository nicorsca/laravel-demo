<?php


namespace App\Modules\Employees\Listeners;


use App\Events\Employee\History\Deactivated;
use App\Modules\Employees\Mails\EmployeeDeactivateMail;
use Illuminate\Support\Facades\Mail;

class HistoryEventSubscriber
{

    /**
     * @param \App\Events\Employee\History\Deactivated $event
     */
    public function onEmployeeDeactivated(Deactivated $event): void
    {
        $user = $event->employee->user;
        Mail::to($user->email)->queue(new EmployeeDeactivateMail($user, $event->history));
    }


    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen(Deactivated::class, [HistoryEventSubscriber::class, 'onEmployeeDeactivated']);
    }

}
