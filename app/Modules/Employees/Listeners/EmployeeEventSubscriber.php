<?php


namespace App\Modules\Employees\Listeners;


use App\Events\Employee\Created as EmployeeCreated;
use App\Events\Employee\History\Activated as EmployeeActivated;
use App\Events\Employee\History\Deactivated as EmployeeDeactivated;
use App\Modules\Employees\Database\Models\User;
use App\Modules\Employees\Mails\EmployeeInvitationMail;
use App\System\User\Database\Models\User as SystemUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class EmployeeEventSubscriber
{
    /**
     * @param \App\Events\Employee\Created $event
     */
    public function onEmployeeCreated(EmployeeCreated $event): void
    {
        $employee = $event->employee;
        $employee->histories()->create(
            [
                'date' => $employee->joined_at,
                'status' => User::STATUS_ACTIVE,
                'comment' => trans('responses.employee.history.created_comment'),
            ]
        );
        $this->sendEmployeeInvitation($employee->user);
    }

    /**
     * @param SystemUser $user
     */
    private function sendEmployeeInvitation(SystemUser $user): void
    {
        $password = Str::random(10);
        $user->update(
            [
                'email_verified_at' => null,
                'status' => User::STATUS_ACTIVE,
                'password' => Hash::make($password),
                'is_locked' => User::STATUS_LOCKED,
            ]
        );
        Mail::to($user->email)->queue(new EmployeeInvitationMail($user, $password));
    }

    /**
     * @param \App\Events\Employee\History\Deactivated $event
     */
    public function onEmployeeDeactivated(EmployeeDeactivated $event): void
    {
        $employee = $event->employee;
        $employee->update(['detached_at' => Carbon::now()->format('Y-m-d')]);
        $employee->user->update(['status' => User::STATUS_INACTIVE]);
    }

    /**
     * @param \App\Events\Employee\History\Activated $event
     */
    public function onEmployeeActivated(EmployeeActivated $event): void
    {
        $employee = $event->employee;
        $employee->update(
            [
                'joined_at' => Carbon::now()->format('Y-m-d'),
                'detached_at' => null,
            ]
        );
        $this->sendEmployeeInvitation($employee->user);
    }

    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen(EmployeeCreated::class, [EmployeeEventSubscriber::class, 'onEmployeeCreated']);
        $events->listen(EmployeeDeactivated::class, [EmployeeEventSubscriber::class, 'onEmployeeDeactivated']);
        $events->listen(EmployeeActivated::class, [EmployeeEventSubscriber::class, 'onEmployeeActivated']);
    }
}
