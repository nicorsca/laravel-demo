<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi']], function () {
    Route::resource('user-preferences', 'UserPreferencesController')->only('index', 'show', 'update');
});
