<?php
namespace App\Modules\UserPreferences;

use App\Modules\UserPreferences\Providers\EventServiceProvider;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package App\Modules\UserPreferences
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\UserPreferences\Interfaces\UserPreferenceInterface','App\Modules\UserPreferences\Repositories\UserPreferenceRepository');
        $this->app->register(EventServiceProvider::class);
    }
}
