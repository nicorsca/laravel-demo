<?php
namespace App\Modules\UserPreferences\Requests;

use App\System\UserPreference\Database\Models\UserPreference;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use NicoSystem\Requests\NicoRequest;

/**
 * Class UserPreferenceUpdateRequest
 * @package App\Modules\UserPreferences\Requests
 */
class UserPreferenceUpdateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $id = $this->route('user_preference');
        $preference = UserPreference::find($id);

        return $this->getSettingValidationRules($preference?->key);
    }

    /**
     * @param string|null $key
     * @return string[]
     */
    private function getSettingValidationRules(string $key = null): array
    {
        return match ($key) {
            UserPreferenceKey::USER_LIST_VIEW => [
                'value' => 'required|string|in:' . implode(',', UserPreferenceValue::listViewOptions()),
            ],
            UserPreferenceKey::USER_THEME => [
                'value' => 'required|string|in:' . implode(',', UserPreferenceValue::themeOptions()),
            ],
            UserPreferenceKey::USER_TIMEZONE => [
                'value' => 'required|string|max:30',
            ],
            UserPreferenceKey::EMPLOYEE_WORK_TIME => [
                'value' => 'required|array',
                'value.start_work_time' => 'required|date_format:H:i',
                'value.end_work_time' => 'required|date_format:H:i|after:value.start_work_time',
            ],
            default => ['value' => 'required'],
        };
    }

}
