<?php

namespace App\Modules\UserPreferences\Listeners;

use App\Events\Employee\Created as EmployeeCreated;
use App\Modules\UserPreferences\Database\Models\Setting;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use Illuminate\Events\Dispatcher;

class UserPreferenceEventSubscriber
{
    /**
     * @param \App\Events\Employee\Created $event
     */
    public function onEmployeeCreated(EmployeeCreated $event): void
    {
        $employee = $event->employee;
        $user = $employee->user;
        $settings = $event->setting->whereIn('key', UserPreferenceKey::options())->get(['key', 'value', 'category'])->toArray();
        $user->preferences()->createMany($settings);
    }

    /**
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(EmployeeCreated::class, [UserPreferenceEventSubscriber::class, 'onEmployeeCreated']);
    }
}
