<?php

namespace App\Modules\Attendances\Repositories;

use App\Modules\Attendances\Database\Models\Holiday;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Interfaces\AttendanceExcelReportEmployeesSummaryInterface;
use App\System\Foundation\Excel\ExcelGenerateTrait;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use Exception;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AttendanceExcelReportEmployeesSummaryRepository implements AttendanceExcelReportEmployeesSummaryInterface
{
    use ExcelGenerateTrait;

    /**
     * @param Setting $setting
     * @param Holiday $holiday
     */
    public function __construct(private Setting $setting, private Holiday $holiday)
    {
    }

    /**
     * @param array $inputs
     * @param Collection $data
     * @param int $workingDays
     *
     * @return StreamedResponse
     * @throws Exception
     */
    public function exportEmployeesAttendanceSummaryReport(
        array $inputs,
        Collection $data,
        int $workingDays
    ): StreamedResponse {
        $timezone = $this->setting->where('key',
                UserPreferenceKey::USER_TIMEZONE)->first()?->value ?? UserPreferenceValue::USER_TIMEZONE_VALUE;

        $attendances = [];
        if ($data->count() > 0) {
            $attendances = $this->formatSummarizedData($data->toArray(), $timezone, $workingDays);
        }

        $columns = [
            'name'            => 'Employee name',
            'email'           => 'Email',
            'position'        => 'Position',
            'present_days'    => 'Present days',
            'absent_days'     => 'Absent days',
            'leave_days'      => 'Leave days',
            'avg_check_in'    => 'Average Check in',
            'avg_check_out'   => 'Average Check out',
            'avg_work_time'   => 'Average work time',
            'total_work_time' => 'Total work time',
        ];

        $widths = [
            'name'            => 20,
            'email'           => 20,
            'position'        => 20,
            'present_days'    => 20,
            'absent_days'     => 20,
            'leave_days'      => 20,
            'avg_check_in'    => 20,
            'avg_check_out'   => 20,
            'avg_work_time'   => 20,
            'total_work_time' => 20,
        ];

        $writer = $this->createExcelOrder($attendances, $columns, $widths);

        return response()->stream(
            function () use ($writer) {
                $writer->setOffice2003Compatibility(true);
                $writer->save('php://output');
            },
            200,
            [
                'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => sprintf('attachment;filename="%s"',
                    'attendance_employees_summary_' . $inputs['start_date'] . '_' . $inputs['end_date'] . '.xlsx'),
                'Cache-Control'       => 'max-age=0',
            ]
        );
    }

    /**
     * @param array $data
     * @param string $timezone
     * @param int $workDays
     *
     * @return array
     */
    private function formatSummarizedData(array $data, string $timezone, int $workDays): array
    {
        return array_reduce(
            $data,
            static function ($response, $item) use ($timezone, $workDays) {
                $absentDays = $workDays - $item['present_days'] - $item['leave_days'];
                $response[] = [
                    'name'            => $item['employee']['name'],
                    'email'           => $item['employee']['email'],
                    'position'        => $item['employee']['position'],
                    'present_days'    => $item['present_days'],
                    'absent_days'     => ($absentDays > 0) ? $absentDays : 0,
                    'leave_days'      => $item['leave_days'],
                    'avg_check_in'    => convert_date_time_to_timezone($item['avg_check_in'], $timezone, 'H:i'),
                    'avg_check_out'   => convert_date_time_to_timezone($item['avg_check_out'], $timezone, 'H:i'),
                    'avg_work_time'   => convert_seconds_to_hour_minute($item['avg_work_time']),
                    'total_work_time' => convert_seconds_to_hour_minute($item['total_work_time']),
                ];

                return $response;
            }
        );
    }
}
