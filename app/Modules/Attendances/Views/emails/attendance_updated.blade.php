<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
</head>

<body>
<p>
    <strong>{!! $employeeName !!}</strong> has updated the attendance log.
</p>
<p><strong>Type:</strong> {!! $type !!}</p>
<p><strong>Old attend at:</strong> {!! $oldAttendanceAttendAt !!}</p>
<p><strong>New attend at:</strong> {!! $newAttendanceAttendAt !!}</p>

<p><strong>Reason:</strong> {!! $reason !!}</p>

</body>
</html>
