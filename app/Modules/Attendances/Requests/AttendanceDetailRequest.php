<?php

namespace App\Modules\Attendances\Requests;

use App\System\AppBaseModel;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Requests\NicoListRequest;

class AttendanceDetailRequest extends NicoListRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules += [
            'date' => 'required|date_format:Y-m-d',
            'employee_id' => 'nullable|integer',
        ];

        return $rules;
    }
}
