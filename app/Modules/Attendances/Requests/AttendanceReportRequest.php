<?php

namespace App\Modules\Attendances\Requests;

use NicoSystem\Requests\NicoRequest;

class AttendanceReportRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'start_date' => 'required|date_format:Y-m-d|required_with:end_date',
            'end_date' => 'required|required_with:start_date|date_format:Y-m-d|after_or_equal:start_date',
        ];
    }

}
