<?php

namespace App\Modules\Attendances\Requests;

use NicoSystem\Requests\NicoRequest;

class AttendanceTodayRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'keyword' => 'nullable|string',
        ];
    }

}
