<?php

namespace App\Modules\Attendances\Requests;

use NicoSystem\Foundation\Requests\NicoListRequest;

class AttendanceListRequest extends NicoListRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules += [
            'start_date' => 'nullable|date_format:Y-m-d|required_with:end_date',
            'end_date' => 'nullable|required_with:start_date|date_format:Y-m-d|after_or_equal:start_date',
            'employee_id' => 'nullable|integer',
            'on_leave' => 'nullable|boolean',
        ];

        return $rules;
    }

}
