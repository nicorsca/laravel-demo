<?php

namespace App\Modules\Attendances\Controllers;

use App\Modules\Attendances\Interfaces\AttendanceExcelReportEmployeesSummaryInterface;
use App\Modules\Attendances\Interfaces\AttendanceReportInterface;
use App\Modules\Attendances\Requests\AttendanceReportRequest;
use NicoSystem\Controllers\BaseController;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AttendanceExcelReportController extends BaseController
{

    /**
     * @param AttendanceReportInterface $attendanceViewRepository
     * @param AttendanceExcelReportEmployeesSummaryInterface $repository
     */
    public function __construct(
        private AttendanceReportInterface $attendanceViewRepository,
        private AttendanceExcelReportEmployeesSummaryInterface $repository
    ) {
    }

    /**
     * @param AttendanceReportRequest $request
     *
     * @return StreamedResponse
     */
    public function getAttendanceExcelReportEmployeesSummary(AttendanceReportRequest $request): StreamedResponse
    {
        $data = $this->attendanceViewRepository->getEmployeesReport($request->all(), 'employee_id');
        $workingDays = $this->attendanceViewRepository->getWorkingDays($request->all());

        return $this->repository->exportEmployeesAttendanceSummaryReport($request->all(), $data, $workingDays);
    }
}
