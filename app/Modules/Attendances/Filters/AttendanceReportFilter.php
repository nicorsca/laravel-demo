<?php

namespace App\Modules\Attendances\Filters;

use NicoSystem\Filters\BaseFilter;

class AttendanceReportFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        $this->employee_id($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
    }

    /**
     * @param string $employeeId
     */
    public function employee_id(string $employeeId = ''): void
    {
        if ($employeeId) {
            $this->builder->where('employee_id', $employeeId);
        }
    }

    /**
     * @param string $startDate
     */
    public function start_date(string $startDate = ''): void
    {
        if ($startDate != '') {
            $this->builder->where('attend_date', '>=', $startDate);
        }
    }

    /**
     * @param string $endDate
     */
    public function end_date(string $endDate = ''): void
    {
        if ($endDate != '') {
            $this->builder->where('attend_date', '<=', $endDate);
        }
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {
    }

    /**
     * @param bool|null $onLeave
     */
    public function on_leave(bool $onLeave = null): void
    {
        if ($onLeave) {
            $this->builder->whereNotNull(['leave_in', 'leave_out']);
        }
    }
}
