<?php

namespace App\Modules\Attendances\Listeners;

use App\Events\Leave\ApproveLeaveRequest;
use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Events\Updated;
use App\Modules\Attendances\Mails\AttendanceUpdatedMail;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Leave\Database\Models\Leave;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;
use NicoSystem\Foundation\Status;

class AttendanceEventSubscriber
{

    /**
     * @param ApproveLeaveRequest $event
     * @throws Exception
     */
    public function onLeaveStatusUpdated(ApproveLeaveRequest $event): void
    {
        $leave = $event->leave;
        if ($leave->status == LeaveStatus::APPROVED) {
            $this->splitLeaveToAttendances($leave);
        }
    }

    /**
     * @param Leave $leave
     * @return void
     * @throws Exception
     */
    private function splitLeaveToAttendances(Leave $leave): void
    {
        $period = new \DatePeriod(
            new \DateTime(Carbon::parse($leave->start_at)->toDateString()),
            new \DateInterval('P1D'),
            new \DateTime($leave->end_at)
        );
        foreach ($period as $value) {
            $value = Carbon::parse($value)->toDateString();
            $leaveIn = Carbon::parse($leave->start_at)->toDateString() != $value ? $value . ' 00:00:00' : $leave->start_at;
            $leaveOut = Carbon::parse($leave->end_at)->toDateString() != $value ? Carbon::parse($leaveIn)->toDateString() . ' 23:59:59' : $leave->end_at;
            if ($leave->type != LeaveType::CANCELLATION_LEAVE) {
                $this->createLeaveAttendance($leave->employee_id, $leaveIn, $leaveOut);
            } else {
                $this->cancelLeaveAttendance($leave->employee_id, $leaveIn, $leaveOut);
            }
        }
    }

    /**
     * @param string $employeeId
     * @param string $startAt
     * @param string $endAt
     */
    private function createLeaveAttendance(string $employeeId, string $startAt, string $endAt): void
    {
        Attendance::create([
            'attend_at' => $startAt,
            'type' => AttendanceType::LEAVE_IN,
            'punch_count' => 1,
            'status' => Status::STATUS_PUBLISHED,
            'employee_id' => $employeeId,
        ]);

        Attendance::create([
            'attend_at' => $endAt,
            'type' => AttendanceType::LEAVE_OUT,
            'punch_count' => 1,
            'status' => Status::STATUS_PUBLISHED,
            'employee_id' => $employeeId,
        ]);
    }

    /**
     * @param string $employeeId
     * @param string $startAt
     * @param string $endAt
     */
    private function cancelLeaveAttendance(string $employeeId, string $startAt, string $endAt): void
    {
        $attendances = Attendance::where('employee_id', $employeeId)
            ->published()
            ->whereDate('attend_at', Carbon::parse($startAt)->toDateString())
            ->whereIn('type', [AttendanceType::LEAVE_IN, AttendanceType::LEAVE_OUT])
            ->get();
        $leaveIn = $attendances->firstWhere('type', AttendanceType::LEAVE_IN);
        $leaveOut = $attendances->firstWhere('type', AttendanceType::LEAVE_OUT);

        if ($leaveIn?->attend_at == $startAt && $leaveOut?->attend_at == $endAt) {
            Attendance::whereIn('id', $attendances->pluck('id'))->delete();
            return;
        }
        $newEndAt = $leaveIn?->attend_at != $startAt ? Carbon::parse($startAt)->subSecond() : $leaveOut?->attend_at;
        $newStartAt = $leaveOut?->attend_at != $endAt ? Carbon::parse($endAt)->addSecond() : $leaveIn?->attend_at;

        $leaveIn?->update(['attend_at' => $newStartAt]);
        $leaveOut?->update(['attend_at' => $newEndAt]);
    }

    /**
     * @param \App\Modules\Attendances\Events\Updated $event
     */
    public function onAttendanceUpdated(Updated $event): void
    {
        $emailGroups = Setting::where('key', SettingKey::DEFAULT_EMAIL_GROUPS)->first();
        if ($emailGroups && config('mail.mail_to.default_email_groups')) {
            Mail::to($emailGroups->value)->queue(new AttendanceUpdatedMail($event->oldAttendance, $event->newAttendance));
        }
    }

    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen(ApproveLeaveRequest::class, [AttendanceEventSubscriber::class, 'onLeaveStatusUpdated']);
        $events->listen(Updated::class, [AttendanceEventSubscriber::class, 'onAttendanceUpdated']);
    }
}
