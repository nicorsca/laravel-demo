<?php

use App\Modules\Attendances\Controllers\AttendanceExcelReportController;
use App\Modules\Attendances\Controllers\AttendanceReportController;
use App\Modules\Attendances\Controllers\AttendancesController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi']], function () {
    Route::get('attendances/details', [AttendancesController::class, 'getDetails']);
    Route::resource('attendances', 'AttendancesController')->only('store', 'update');
    Route::get('attendances', [AttendanceReportController::class, 'index']);

    //attendance reports
    Route::group(['prefix' => 'attendances/reports'], function () {
        Route::get('summary', [AttendanceReportController::class, 'getEmployeesAttendanceSummary']);
        Route::get('average', [AttendanceReportController::class, 'getEmployeesAttendanceAverage']);
        Route::get('calendar', [AttendanceReportController::class, 'getEmployeesAttendanceCalendar']);
        Route::get('employees/{employee}/summary', [AttendanceReportController::class, 'getEmployeeAttendanceSummary']);
        Route::get('employees/{employee}/calendar', [AttendanceReportController::class, 'getEmployeeAttendanceCalendar']);
        Route::get('today/summary', [AttendanceReportController::class, 'getTodaySummary']);
        Route::get('today/attendance', [AttendanceReportController::class, 'getTodayAttendance']);
        Route::get('summary/excel', [AttendanceExcelReportController::class, 'getAttendanceExcelReportEmployeesSummary']);
    });
});
