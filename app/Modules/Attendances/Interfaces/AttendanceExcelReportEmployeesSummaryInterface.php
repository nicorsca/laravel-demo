<?php

namespace App\Modules\Attendances\Interfaces;

use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\StreamedResponse;

interface AttendanceExcelReportEmployeesSummaryInterface
{

    /**
     * @param array $inputs
     * @param Collection $data
     * @param int $workingDays
     *
     * @return StreamedResponse
     */
    public function exportEmployeesAttendanceSummaryReport(
        array $inputs,
        Collection $data,
        int $workingDays
    ): StreamedResponse;
}
