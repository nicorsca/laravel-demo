<?php

namespace App\Modules\Attendances\Interfaces;

use App\Modules\Attendances\Database\Models\AttendanceView;
use Illuminate\Database\Eloquent\Collection;
use NicoSystem\Interfaces\BasicCrudInterface;

interface AttendanceReportInterface extends BasicCrudInterface
{
    /**
     * @param array $inputs
     * @param string $employeeId
     *
     * @return AttendanceView
     */
    public function getEmployeeAttendanceSummary(array $inputs, string $employeeId): AttendanceView;

    /**
     * @param array $inputs
     * @param string $employeeId
     *
     * @return array
     */
    public function getEmployeeAttendanceCalendar(array $inputs, string $employeeId): array;

    /**
     * @param array $inputs
     * @param string $groupBy
     *
     * @return Collection
     */
    public function getEmployeesReport(array $inputs, string $groupBy): Collection;

    /**
     * @param array $inputs
     *
     * @return int
     */
    public function getWorkingDays(array $inputs): int;

    /**
     * @param array $inputs
     *
     * @return Collection
     */
    public function getTodayAttendance(array $inputs): Collection;

    /**
     * @return array
     */
    public function getTodaySummary(): array;

    /**
     * @param array $inputs
     * @param Collection $attendances
     * @param array $attendResponseKeys
     *
     * @return array
     */
    public function getEachDatesReport(array $inputs, Collection $attendances, array $attendResponseKeys): array;

    /**
     * @param array $inputs
     *
     * @return Collection
     */
    public function getEmployeesAttendanceCalendar(array $inputs): Collection;

}
