<?php

namespace App\Modules\Attendances\Exceptions;

use NicoSystem\Exceptions\NicoException;

class DifferentAttendanceDateException extends NicoException
{

    /**
     * @var int
     */
    protected $code = 422;

    /**
     * @var string
     */
    protected string $respCode = 'err_different_attendance_date_exception';
}
