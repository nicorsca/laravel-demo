<?php

namespace App\Modules\Attendances\Exceptions;

use NicoSystem\Exceptions\NicoException;

class SameTimeAttendedException extends NicoException
{
    /**
     * @var int
     */
    protected $code = 422;

    /**
     * @var string
     */
    protected string $respCode = 'err_same_time_attended_exception';
}
