<?php

namespace App\Modules\Attendances\Exceptions;

use NicoSystem\Exceptions\NicoException;

class AttendanceIntersectException extends NicoException
{
    /**
     * @var int
     */
    protected $code = 422;

    /**
     * @var string
     */
    protected string $respCode = 'err_attendance_intersect_exception';

}
