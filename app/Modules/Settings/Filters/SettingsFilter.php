<?php

namespace App\Modules\Settings\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class SettingsController
 *
 * @package App\Modules\Settings\Filters
 */
class SettingsFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param $keyword
     */
    public function keyword($keyword = ''): void
    {
        $this->key($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        $this->key($title);
    }

    /**
     * @param string $key
     */
    public function key(string $key = ''): void
    {
        if ($key !== '') {
            $this->builder->where('key', 'like', "%{$key}%");
        }
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type !== '') {
            $this->builder->where('type', $type);
        }
    }

    /**
     * @param string $category
     */
    public function category(string $category = ''): void
    {
        if ($category !== '') {
            $this->builder->where('category', $category);
        }
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {
    }
}
