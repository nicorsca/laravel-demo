<?php

namespace App\Modules\Settings\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface SettingInterface
 *
 * @package App\Modules\Settings\Interfaces
 */
interface SettingInterface extends BasicCrudInterface
{

}
