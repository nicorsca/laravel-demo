<?php

namespace App\Modules\Settings\Controllers;

use App\Modules\Settings\Interfaces\SettingInterface;
use App\Modules\Settings\Requests\SettingUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class SettingsController
 *
 * @package App\Modules\Settings\Controllers
 */
class SettingsController extends BaseController
{
    /**
     * SettingsController constructor.
     *
     * @param SettingInterface $repository
     */
    public function __construct(private SettingInterface $repository)
    {
    }

    /**
     * @param  NicoRequest $request
     * @return JsonResponse
     */
    public function index(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param  SettingUpdateRequest $request
     * @param  string               $id
     * @return JsonResponse
     */
    public function update(SettingUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->only('value')));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }
}
