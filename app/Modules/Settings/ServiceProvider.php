<?php
namespace App\Modules\Settings;

use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * @package App\Modules\Settings
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Settings\Interfaces\SettingInterface', 'App\Modules\Settings\Repositories\SettingRepository');
    }
}
