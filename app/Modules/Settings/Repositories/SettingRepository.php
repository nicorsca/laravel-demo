<?php

namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Filters\SettingsFilter;
use App\Modules\Settings\Interfaces\SettingInterface;
use App\Modules\Settings\Database\Models\Setting;
use Illuminate\Support\Arr;
use NicoSystem\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SettingRepository
 *
 * @package App\Modules\Settings\Repositories
 */
class SettingRepository extends BaseRepository implements SettingInterface
{
    /**
     * SettingRepository constructor.
     *
     * @param Setting $model
     */
    public function __construct(Setting $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return SettingsFilter
     */
    public function getFilter(Builder $builder): SettingsFilter
    {
        return new SettingsFilter($builder);
    }

    /**
     * @param string $id
     * @param array $attributes
     * @return \App\Modules\Settings\Database\Models\Setting
     */
    public function update(string $id, array $attributes): Setting
    {
        if (is_array($attributes['value']) && Arr::has($attributes['value'], ['start_work_time', 'end_work_time'])) {
            $attributes['value'] = $attributes['value']['start_work_time'] . '-' . $attributes['value']['end_work_time'];
        }

        return parent::update($id, $attributes);
    }
}
