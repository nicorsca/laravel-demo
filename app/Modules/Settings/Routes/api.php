<?php

use App\Modules\Settings\Controllers\SettingsController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi']], function () {
    Route::resource('settings', 'SettingsController')->only('index', 'show');
    Route::put('settings/{setting}', [SettingsController::class, 'update'])->middleware('role:manager');
});
