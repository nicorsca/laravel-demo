<?php

namespace App\Modules\Projects\Interfaces;

interface MemberInterface
{

    /**
     * @param string $projectId
     * @param array $inputs
     * @return void
     */
    public function assignMembers(string $projectId, array $inputs): void;

    /**
     * @param string $projectId
     * @param int $employeeId
     */
    public function revokeMember(string $projectId, int $employeeId): void;
}
