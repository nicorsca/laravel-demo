<?php

namespace App\Modules\Projects\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface ProjectInterface
 * @package App\Modules\Projects\Interfaces
 */
interface ProjectInterface extends BasicCrudInterface
{

    /**
     * @param string $projectId
     * @return void
     */
    public function archive(string $projectId): void;
}
