<?php

namespace App\Modules\Projects\Listeners;

use App\Events\Employee\History\Deactivated as EmployeeDeactivated;
use App\Modules\Projects\Events\AssignMembers;
use App\Modules\Projects\Events\RevokeMember;
use App\Modules\Projects\Mails\ProjectAssignMemberMail;
use App\Modules\Projects\Mails\ProjectRevokeMemberMail;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class MemberEventSubscriber
{
    /**
     * @param \App\Modules\Projects\Events\AssignMembers $event
     */
    public function onAssignMembers(AssignMembers $event): void
    {
        $project = $event->project;

        $employees = $project->employees()->wherePivotIn('employee_id', $event->newMembersIds)->get();
        $employees->each(function ($employee) use ($project) {
            Mail::to($employee->email)->queue(new ProjectAssignMemberMail($project, $employee));
        });

    }

    /**
     * @param \App\Modules\Projects\Events\RevokeMember $event
     */
    public function onRevokeMember(RevokeMember $event): void
    {
        $employee = $event->employee;
        Mail::to($employee->email)->queue(new ProjectRevokeMemberMail($event->project, $employee));
    }

    /**
     * @param \App\Events\Employee\History\Deactivated $event
     */
    public function onEmployeeDeactivated(EmployeeDeactivated $event): void
    {
        $employee = $event->employee;
        $projects = $employee->projects()->wherePivot('licensed', true)->get();
        $projects->each(function ($project) use ($employee){
           $project->employees()->syncWithoutDetaching([$employee->id => ['licensed' => false]]);
        });
    }

    /**
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(AssignMembers::class, [MemberEventSubscriber::class, 'onAssignMembers']);
        $events->listen(RevokeMember::class, [MemberEventSubscriber::class, 'onRevokeMember']);
        $events->listen(EmployeeDeactivated::class, [MemberEventSubscriber::class, 'onEmployeeDeactivated']);
    }
}
