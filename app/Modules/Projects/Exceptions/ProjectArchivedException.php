<?php

namespace App\Modules\Projects\Exceptions;

use NicoSystem\Exceptions\NicoException;

class ProjectArchivedException extends NicoException
{

    protected $code = 422;

    protected string $respCode = 'err_project_archived_exception';

}
