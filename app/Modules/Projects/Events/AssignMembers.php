<?php

namespace App\Modules\Projects\Events;

use App\Modules\Projects\Database\Models\Project;
use Illuminate\Support\Collection;

class AssignMembers
{
    /**
     * @param \App\Modules\Projects\Database\Models\Project $project
     * @param \Illuminate\Support\Collection $newMembersIds
     */
    public function __construct(
        public Project    $project,
        public Collection $newMembersIds
    )
    {
    }
}
