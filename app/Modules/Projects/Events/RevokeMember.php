<?php

namespace App\Modules\Projects\Events;

use App\Modules\Projects\Database\Models\Project;
use App\System\Employee\Database\Models\EmployeeView;

class RevokeMember
{
    public function __construct(
        public Project  $project,
        public EmployeeView $employee
    )
    {
    }
}
