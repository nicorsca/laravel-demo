<?php

namespace App\Modules\Projects\Repositories;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Events\AssignMembers;
use App\Modules\Projects\Events\RevokeMember;
use App\Modules\Projects\Interfaces\MemberInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use NicoSystem\Repositories\BaseRepository;

class MemberRepository extends BaseRepository implements MemberInterface
{
    /**
     * ProjectRepository constructor.
     * @param Project $model
     */
    public function __construct(Project $model)
    {
        parent::__construct($model);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function getFilter(Builder $builder): void
    {
    }

    /**
     * @param string $projectId
     * @param array $inputs
     */
    public function assignMembers(string $projectId, array $inputs): void
    {
        $project = parent::getById($projectId);
        $members = $project->employees()
            ->wherePivot('licensed', $inputs['licensed'])
            ->wherePivotIn('employee_id', $inputs['employee_ids'])
            ->get()->pluck('id');

        $newMembers = collect($inputs['employee_ids'])->diff($members);
        $data = $this->mapWithPivotValues($inputs);
        $project->employees()->syncWithoutDetaching($data);
        event(new AssignMembers($project, $newMembers));
    }

    /**
     * @param array $inputs
     * @return array
     */
    private function mapWithPivotValues(array $inputs): array
    {
        return collect($inputs['employee_ids'])
            ->filter(function ($id) {
                return $id;
            })
            ->reduce(function ($assoc, $id) use ($inputs) {
                $assoc[$id] =  Arr::only($inputs, ['role_id', 'licensed']);
                return $assoc;
            }, []);
    }

    /**
     * @param string $projectId
     * @param int $employeeId
     */
    public function revokeMember(string $projectId, int $employeeId): void
    {
        $project = parent::getById($projectId);
        $member = $project->employees()
            ->wherePivot('licensed', true)
            ->wherePivot('employee_id', $employeeId)
            ->firstOrFail();
        $project->employees()->syncWithoutDetaching([$employeeId => ['licensed' => false]]);
        event(new RevokeMember($project, $member));
    }
}
