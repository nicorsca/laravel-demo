<?php

namespace App\Modules\Projects\Repositories;

use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Filters\RolesFilter;
use App\Modules\Projects\Interfaces\RoleInterface;
use Illuminate\Database\Eloquent\Builder;
use NicoSystem\Repositories\BaseRepository;

class RoleRepository extends BaseRepository implements RoleInterface
{
    /**
     * ProjectRepository constructor.
     * @param \App\Modules\Projects\Database\Models\ProjectRole $model
     */
    public function __construct(ProjectRole $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return \App\Modules\Projects\Filters\RolesFilter
     */
    public function getFilter(Builder $builder): RolesFilter
    {
        return new RolesFilter($builder);
    }
}
