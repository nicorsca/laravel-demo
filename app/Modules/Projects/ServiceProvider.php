<?php
namespace App\Modules\Projects;

use App\Modules\Projects\Interfaces\MemberInterface;
use App\Modules\Projects\Interfaces\RoleInterface;
use App\Modules\Projects\Providers\EventServiceProvider;
use App\Modules\Projects\Repositories\MemberRepository;
use App\Modules\Projects\Repositories\RoleRepository;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;
use App\Modules\Projects\Interfaces\ProjectInterface;
use App\Modules\Projects\Repositories\ProjectRepository;
use App\Modules\Projects\Interfaces\DocumentInterface;
use App\Modules\Projects\Repositories\DocumentRepository;

/**
 * Class ServiceProvider
 * @package App\Modules\Projects
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected bool $defer = true;

    /**
     * Register your binding
     */
    public function register(): void
    {
        $this->app->bind(ProjectInterface::class, ProjectRepository::class);
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(DocumentInterface::class, DocumentRepository::class);
        $this->app->bind(MemberInterface::class, MemberRepository::class);
        $this->app->register(EventServiceProvider::class);
    }
}
