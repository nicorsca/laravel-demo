<?php

namespace App\Modules\Projects\Controllers;

use App\Modules\Projects\Interfaces\ProjectInterface;
use App\Modules\Projects\Requests\ProjectCreateRequest;
use App\Modules\Projects\Requests\ProjectUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class ProjectsController
 * @package App\Modules\Projects\Controllers
 */
class ProjectsController extends BaseController
{
    /**
     * ProjectsController constructor.
     * @param ProjectInterface $repository
     */
    public function __construct(private ProjectInterface $repository)
    {
    }

    /**
     * @param NicoRequest $request
     * @return JsonResponse
     */
    public function index(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param ProjectCreateRequest $request
     * @return JsonResponse
     */
    public function store(ProjectCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->only('title', 'description', 'code', 'type', 'avatar', 'start_date', 'client_id')));
    }

    /**
     * @param ProjectUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(ProjectUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->only('title', 'description', 'type', 'avatar', 'start_date', 'client_id')));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

    /**
     * @param \NicoSystem\Requests\NicoRequest $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function archive(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->archive($id));
    }
}
