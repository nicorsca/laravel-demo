<?php

namespace App\Modules\Projects\Middlewares;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Exceptions\ProjectArchivedException;
use App\System\Project\Foundation\ProjectStatus;
use Closure;
use Illuminate\Http\Request;

class ArchivedProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($request->getMethod() !== 'GET' && $request->route()?->hasParameter('project')) {
            $project = Project::findOrFail($request->route('project'));
            if ($project->status == ProjectStatus::STATUS_ARCHIVE) {
                throw new ProjectArchivedException(trans('responses.project.archived'));
            }
        }
        return $next($request);
    }
}
