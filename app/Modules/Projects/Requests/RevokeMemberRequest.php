<?php

namespace App\Modules\Projects\Requests;

use App\System\AppBaseModel;
use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

class RevokeMemberRequest extends NicoRequest
{

    public function rules(): array
    {
        return [
            'employee_id' => [
                'required',
                'integer',
                Rule::exists('vw_employees', 'id')
                    ->where('status', AppBaseModel::STATUS_ACTIVE)
                    ->whereNull('deleted_at')
            ],
        ];
    }

}
