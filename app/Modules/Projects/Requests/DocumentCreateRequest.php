<?php

namespace App\Modules\Projects\Requests;

use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class DocumentCreateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|max:255',
            'url' => 'required|url|ends_with:jpg,png,jpeg,gif,svg,pdf,doc,docx,xls,xlsx|starts_with:' . config('fileupload.whitelist_urls'),
            'thumbnail' => 'nullable|url|ends_with:jpg,png,jpeg,gif,svg',
            'status' => 'nullable|in:' . implode(',', Status::options()),
            'type' => 'required|string|max:130',
        ];
    }
}
