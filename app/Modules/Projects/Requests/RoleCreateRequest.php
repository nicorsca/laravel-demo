<?php

namespace App\Modules\Projects\Requests;

use NicoSystem\Requests\NicoRequest;

class RoleCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:100|unique:project_roles,title,NULL,id,deleted_at,NULL',
        ];
    }
}
