@extends('emails.base_email')

@section('mail_body')
    {!! trans('project_member_assign.body', compact('title')) !!}
@stop
