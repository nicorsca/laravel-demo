<?php

namespace App\Modules\Projects\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class ProjectsController
 * @package App\Modules\Projects\Filters
 */
class ProjectsFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
       parent::title($keyword);
    }

    /**
     * @param string $code
     */
    public function code(string $code = ''): void
    {
        if ($code !== '') {
            $this->builder->where('code', 'like', "%{$code}%");
        }
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type !== '') {
            $this->builder->where('type', 'like', "%{$type}%");
        }
    }

    /**
     * @param int|null $clientId
     */
    public function client_id(int $clientId = null): void
    {
        if ($clientId) {
            $this->builder->where('client_id', $clientId);
        }
    }

}
