<?php

namespace App\Modules\Projects\Filters;

use NicoSystem\Filters\BaseFilter;

class RolesFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {
    }
}
