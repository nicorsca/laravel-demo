<?php


namespace App\Modules\Projects\Filters;


use NicoSystem\Filters\BaseFilter;

class DocumentsFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }
}
