<?php

namespace App\Modules\Holidays\Requests;

use NicoSystem\Requests\NicoRequest;

class HolidayMultipleRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'holidays' => 'required|array',
            'holidays.*.title' => 'required|string|max:50',
            'holidays.*.date' => 'required|date_format:Y-m-d|after:today|distinct|unique:holidays,date,NULL,id,deleted_at,NULL',
            'holidays.*.remarks' => 'nullable|string|max:250',
        ];
    }
}
