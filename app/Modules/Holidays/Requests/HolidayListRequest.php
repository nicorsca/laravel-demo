<?php

namespace App\Modules\Holidays\Requests;

use NicoSystem\Foundation\Requests\NicoListRequest;

class HolidayListRequest extends NicoListRequest
{

    public function rules(): array
    {
        $rules = parent::rules();

        $rules += [
            'start_date' => 'nullable|date_format:Y-m-d',
            'end_date' => 'nullable|required_with:start_date|date_format:Y-m-d|after_or_equal:start_date'
        ];

        return $rules;
    }

}
