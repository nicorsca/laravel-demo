<?php

namespace App\Modules\Holidays;

use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package App\Modules\Holidays
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Holidays\Interfaces\HolidayInterface', 'App\Modules\Holidays\Repositories\HolidayRepository');
    }
}
