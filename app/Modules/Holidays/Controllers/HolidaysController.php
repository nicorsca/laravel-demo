<?php

namespace App\Modules\Holidays\Controllers;

use App\Modules\Holidays\Interfaces\HolidayInterface;
use App\Modules\Holidays\Requests\HolidayCreateRequest;
use App\Modules\Holidays\Requests\HolidayListRequest;
use App\Modules\Holidays\Requests\HolidayMultipleRequest;
use App\Modules\Holidays\Requests\HolidayUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class HolidaysController
 * @package App\Modules\Holidays\Controllers
 */
class HolidaysController extends BaseController
{
    /**
     * HolidaysController constructor.
     * @param HolidayInterface $repository
     */
    public function __construct(private HolidayInterface $repository)
    {
    }

    /**
     * @param \App\Modules\Holidays\Requests\HolidayListRequest $request
     * @return JsonResponse
     */
    public function index(HolidayListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param HolidayCreateRequest $request
     * @return JsonResponse
     */
    public function store(HolidayCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->only('title', 'date', 'remarks')));
    }

    /**
     * @param \App\Modules\Holidays\Requests\HolidayMultipleRequest $request
     * @return JsonResponse
     */
    public function storeMultiple(HolidayMultipleRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->createMultiple($request->only('holidays')));
    }

    /**
     * @param HolidayUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(HolidayUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id,$request->only('title', 'date', 'remarks')));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show (NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

}
