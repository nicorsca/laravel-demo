<?php

namespace App\Modules\Holidays\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class HolidaysController
 * @package App\Modules\Holidays\Filters
 */
class HolidaysFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
       parent::title($keyword);
    }

    /**
     * @param string $startDate
     */
    public function start_date(string $startDate = ''): void
    {
        if ($startDate != '' || $startDate != null) {
            $this->builder->where('date', '>=', $startDate);
        }
    }

    /**
     * @param string $endDate
     */
    public function end_date(string $endDate = ''): void
    {
        if ($endDate != '' || $endDate != null) {
            $this->builder->where('date', '<=', $endDate);
        }
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {
    }

}
