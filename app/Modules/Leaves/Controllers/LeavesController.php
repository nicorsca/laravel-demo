<?php

namespace App\Modules\Leaves\Controllers;

use App\Events\Leave\ApproveLeaveRequest;
use App\Exceptions\ForbiddenException;
use App\Exceptions\ResourceExistsException;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Exceptions\InvalidLeaveStatusException;
use App\Modules\Leaves\Interfaces\LeaveInterface;
use App\Modules\Leaves\Requests\LeaveCancellationRequest;
use App\Modules\Leaves\Requests\LeaveCancelRequest;
use App\Modules\Leaves\Requests\LeaveCreateEmergencyRequest;
use App\Modules\Leaves\Requests\LeaveCreateRequest;
use App\Modules\Leaves\Requests\LeaveListRequest;
use App\Modules\Leaves\Requests\LeaveSummaryRequest;
use App\Modules\Leaves\Requests\LeaveUpdateRequest;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Requests\NicoRequest;

/**
 * Class LeavesController
 *
 * @package App\Modules\Leaves\Controllers
 */
class LeavesController extends BaseController
{
    /**
     * LeavesController constructor.
     *
     * @param LeaveInterface $repository
     */
    public function __construct(private LeaveInterface $repository)
    {
    }

    /**
     * @param LeaveListRequest $request
     * @return JsonResponse
     */
    public function index(LeaveListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param LeaveCreateRequest $request
     * @return JsonResponse
     */
    public function store(LeaveCreateRequest $request): JsonResponse
    {
        $inputs = $request->all();
        $inputs['employee_id'] = Auth::user()->employee?->id;
        $inputs['status'] = LeaveStatus::PENDING;
        if ($inputs['requested_to'] == $inputs['employee_id']) {
            throw new NicoBadRequestException(trans('responses.employee.cannot_request_self'), 'err_cannot_leave_request_self');
        }
        return $this->responseOk($this->createOrUpdateLeaveRequest($inputs));
    }

    /**
     * @param array $inputs
     * @param int|null $leaveId
     * @return array|Leave
     */
    private function createOrUpdateLeaveRequest(array $inputs, int $leaveId = null): array|Leave
    {
        $leave = $this->repository->getLeaveLiesBetweenStartAndEndDate($inputs, $leaveId);
        if ($leave) {
            throw new ResourceExistsException(trans('responses.leave.requested_already'), 'leave_exists_exception');
        }
        if ($inputs['type'] == LeaveType::CANCELLATION_LEAVE) {
            return $this->repository->saveCancellationLeave($inputs, $leaveId);
        }
        $fiscalDate = $this->repository->getStartAndEndFiscalDate();
        $isStartAtInBetweenFiscalYear = check_date_is_between_date_range($inputs['start_at'], $fiscalDate['start_date'], $fiscalDate['end_date']);
        $isEndAtInBetweenFiscalYear = check_date_is_between_date_range($inputs['end_at'], $fiscalDate['start_date'], $fiscalDate['end_date']);
        if ($isStartAtInBetweenFiscalYear && $isEndAtInBetweenFiscalYear) {
            return $this->repository->saveLeaveRequest($inputs, $fiscalDate['start_date'], $fiscalDate['end_date'], $leaveId);
        } elseif (!$isStartAtInBetweenFiscalYear && !$isEndAtInBetweenFiscalYear) {
            $fiscalDateStart = Carbon::parse($fiscalDate['start_date'])->addYear()->toDateString();
            $fiscalDateEnd = Carbon::parse($fiscalDate['end_date'])->addYear()->toDateString();

            return $this->repository->saveLeaveRequest($inputs, $fiscalDateStart, $fiscalDateEnd, $leaveId);
        } elseif ($isStartAtInBetweenFiscalYear && !$isEndAtInBetweenFiscalYear) {
            return $this->repository->saveSplitLeaveRequest($inputs, $fiscalDate['start_date'], $fiscalDate['end_date'], $leaveId);
        }
    }

    /**
     * @param LeaveUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(LeaveUpdateRequest $request, string $id): JsonResponse
    {
        $leaveRequest = $this->repository->getById($id);
        $inputs = $request->only('title', 'description', 'start_at', 'end_at', 'type', 'is_emergency');
        $authEmployeeId = Auth::user()->employee?->id;
        $inputs['status'] = LeaveStatus::PENDING;
        $inputs['employee_id'] = $authEmployeeId;
        $inputs['requested_to'] = $leaveRequest->requested_to;
        if ($leaveRequest->employee_id != $authEmployeeId) {
            throw new ForbiddenException(trans('auth.forbidden'));
        }
        if ($leaveRequest->status != LeaveStatus::PENDING) {
            throw new InvalidLeaveStatusException(trans('responses.leave.status_not_pending'));
        }

        return $this->responseOk($this->createOrUpdateLeaveRequest($inputs, $leaveRequest->id));
    }

    /**
     * @param LeaveCreateEmergencyRequest $request
     * @return JsonResponse
     */
    public function storeEmergencyLeaveRequest(LeaveCreateEmergencyRequest $request): JsonResponse
    {
        $inputs = $request->all();
        $inputs['requested_to'] = Auth::user()->employee?->id;
        $inputs['status'] = LeaveStatus::APPROVED;
        $inputs['is_emergency'] = true;
        if ($inputs['requested_to'] == $inputs['employee_id']) {
            throw new NicoBadRequestException(trans('responses.employee.cannot_request_self'), 'err_cannot_leave_request_self');
        }
        $result = $this->createOrUpdateLeaveRequest($inputs);

        if (!is_array($result)) {
            event(new ApproveLeaveRequest($result));
        } else {
            foreach ($result as $r) event(new ApproveLeaveRequest($r));
        }

        return $this->responseOk($result);
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

    /**
     * @param LeaveCancelRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function cancelLeaveRequest(LeaveCancelRequest $request, string $id): JsonResponse
    {
        $authUser = Auth::user();
        $inputs = $request->only('reason');

        return $this->responseOk($this->repository->cancelLeaveRequest($inputs, $id, $authUser));
    }

    /**
     * @param \App\Modules\Leaves\Requests\LeaveCancelRequest $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function declineLeaveRequest(LeaveCancelRequest $request, string $id): JsonResponse
    {
        $authUser = Auth::user();
        $inputs = $request->only('reason');
        $inputs['status'] = LeaveStatus::DECLINED;

        return $this->responseOk($this->repository->declineLeaveRequest($inputs, $id, $authUser));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function approveLeaveRequest(NicoRequest $request, string $id): JsonResponse
    {
        $authUser = Auth::user();

        return $this->responseOk($this->repository->approveLeaveRequest($id, $authUser));
    }

    /**
     * @param LeaveCancellationRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function storeRequestCancellationLeave(LeaveCancellationRequest $request, string $id): JsonResponse
    {
        $employee = Auth::user()->employee;
        $inputs = $request->only('title', 'description', 'start_at', 'end_at');
        $inputs['employee_id'] = $employee->id;
        $inputs['type'] = LeaveType::CANCELLATION_LEAVE;
        $leave = $this->repository->getLeaveLiesBetweenStartAndEndDate($inputs);
        if ($leave) {
            throw new ResourceExistsException(trans('responses.leave.requested_already'), 'leave_exists_exception');
        }
        return $this->responseOk($this->repository->requestCancellationLeave($inputs, $id));
    }

    /**
     * @param \App\Modules\Leaves\Requests\LeaveSummaryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSummary(LeaveSummaryRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getSummary($request->employee_id));
    }
}
