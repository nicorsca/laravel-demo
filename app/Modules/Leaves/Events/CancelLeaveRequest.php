<?php

namespace App\Modules\Leaves\Events;

use App\Modules\Leaves\Database\Models\Leave;
use App\System\User\Database\Models\User;

class CancelLeaveRequest
{
    /**
     * @param \App\Modules\Leaves\Database\Models\Leave $leave
     * @param \App\System\User\Database\Models\User $authUser
     */
    public function __construct(public Leave $leave, public User $authUser)
    {
    }
}
