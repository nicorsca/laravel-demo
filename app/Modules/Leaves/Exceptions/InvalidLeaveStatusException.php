<?php


namespace App\Modules\Leaves\Exceptions;


use NicoSystem\Exceptions\NicoException;

class InvalidLeaveStatusException extends NicoException
{

    protected $code = 422;

    protected string $respCode = 'err_invalid_leave_status_exception';
}
