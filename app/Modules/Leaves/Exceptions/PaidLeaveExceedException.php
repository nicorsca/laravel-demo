<?php


namespace App\Modules\Leaves\Exceptions;


use NicoSystem\Exceptions\NicoException;

class PaidLeaveExceedException extends NicoException
{
    /**
     * @var int
     */
    protected $code = 400;

    /**
     * @var string
     */
    protected string $respCode = 'paid_leave_exceed_exception';
}
