<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
</head>

<body>
{!! trans('base_email.salutation',['receiverName' => $receiver]) !!}

<p>
    {!! $sender !!} has approved your leave request.
</p>

<p><strong>Type:</strong> {!! $type !!}</p>
<p><strong>Time period:</strong> {!! $start_at !!} - {!! $end_at !!}</p>
<p><strong>Days:</strong> {!! $days !!}</p>
<p><strong>Is emergency:</strong> {!! $is_emergency ? "true" : "false" !!}</p>

</body>
</html>



