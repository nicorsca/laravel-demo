<?php

namespace App\Modules\Leaves\Interfaces;

use App\Modules\Leaves\Database\Models\Leave;
use App\System\User\Database\Models\User;
use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface LeaveInterface
 *
 * @package App\Modules\Leaves\Interfaces
 */
interface LeaveInterface extends BasicCrudInterface
{
    /**
     * @param array $inputs
     * @param string $fiscalDateStart
     * @param string $fiscalDateEnd
     * @param string|null $leaveId
     *
     * @return Leave|array
     */
    public function saveSplitLeaveRequest(
        array $inputs,
        string $fiscalDateStart,
        string $fiscalDateEnd,
        string $leaveId = null
    ): Leave|array;

    /**
     * @param array $inputs
     * @param string $fiscalDateStart
     * @param string $fiscalDateEnd
     * @param string|null $id
     *
     * @return Leave
     */
    public function saveLeaveRequest(
        array $inputs,
        string $fiscalDateStart,
        string $fiscalDateEnd,
        string $id = null
    ): Leave;

    /**
     * @param array $inputs
     * @param string|null $leaveId
     *
     * @return Leave
     */
    public function saveCancellationLeave(array $inputs, string $leaveId = null): Leave;

    /**
     * @return array
     */
    public function getStartAndEndFiscalDate(): array;

    /**
     * @param array $inputs
     * @param int|null $exceptLeaveId
     *
     * @return Leave|null
     */
    public function getLeaveLiesBetweenStartAndEndDate(array $inputs, int $exceptLeaveId = null): Leave|null;

    /**
     * @param array $inputs
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function cancelLeaveRequest(array $inputs, string $leaveId, User $authUser): Leave;

    /**
     * @param array $inputs
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function declineLeaveRequest(array $inputs, string $leaveId, User $authUser): Leave;

    /**
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function approveLeaveRequest(string $leaveId, User $authUser): Leave;

    /**
     * @param array $inputs
     * @param string $leaveId
     *
     * @return Leave
     */
    public function requestCancellationLeave(array $inputs, string $leaveId): Leave;

    /**
     * @param string $employeeId
     *
     * @return array
     */
    public function getSummary(string $employeeId): array;
}
