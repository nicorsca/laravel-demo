<?php

namespace App\Modules\Leaves\Mails;

use App\Modules\Leaves\Database\Models\Leave;
use Illuminate\Mail\Mailable;

class LeaveRequestMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param Leave $leave
     */
    public function __construct(private Leave $leave)
    {
    }

    /**
     * @return LeaveRequestMail
     */
    public function build(): LeaveRequestMail
    {
        $employeeView = $this->leave->employee;
        $requestedView = $this->leave->requested;
        $title = 'Leave request: ' . $this->leave->title;
        return $this->subject($title)
            ->view(nico_view('emails.leave_request', [], [], true))->with(
                [
                    'receiver' => $requestedView->first_name,
                    'sender' => $employeeView->name,
                    'title' => $title,
                    'description' => $this->leave->description,
                    'start_at' => $this->leave->start_at,
                    'end_at' => $this->leave->end_at,
                    'days' => $this->leave->days,
                    'type' => $this->leave->type,
                    'is_emergency' => $this->leave->is_emergency,
                ]
            );
    }
}
