<?php

namespace App\Modules\Leaves\Mails;

use App\System\Leave\Database\Models\Leave;
use Illuminate\Mail\Mailable;

class ApproveLeaveRequestMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param Leave $leave
     */
    public function __construct(private Leave $leave)
    {
    }

    /**
     * @return ApproveLeaveRequestMail
     */
    public function build(): ApproveLeaveRequestMail
    {
        $employeeView = $this->leave->employee;
        $requestedView = $this->leave->requested;

        return $this->subject('Leave request approved: ' . $this->leave->title)
            ->view(nico_view('emails.approve_leave_request', [], [], true))->with(
                [
                    'receiver' => $employeeView->first_name,
                    'sender' => $requestedView->name,
                    'title' => 'Leave request approved: ' . $this->leave->title,
                    'start_at' => $this->leave->start_at,
                    'end_at' => $this->leave->end_at,
                    'days' => $this->leave->days,
                    'type' => $this->leave->type,
                    'is_emergency' => $this->leave->is_emergency,
                ]
            );
    }
}
