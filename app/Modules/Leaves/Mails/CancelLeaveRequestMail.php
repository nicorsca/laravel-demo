<?php

namespace App\Modules\Leaves\Mails;

use App\Modules\Leaves\Database\Models\Leave;
use App\System\Employee\Database\Models\EmployeeView;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\User\Database\Models\User;
use Illuminate\Mail\Mailable;

class CancelLeaveRequestMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param Leave $leave
     * @param User $user
     */
    public function __construct(private Leave $leave, private EmployeeView $receiver, private EmployeeView $sender)
    {
    }

    /**
     * @return CancelLeaveRequestMail
     */
    public function build(): CancelLeaveRequestMail
    {
        $status = $this->leave->status == LeaveStatus::DECLINED ? 'declined: ' : 'cancelled: ';
        $title = 'Leave request ' . $status . $this->leave->title;
        return $this->subject($title)
            ->view(nico_view('emails.cancel_leave_request', [], [], true))->with(
                [
                    'receiver' => $this->receiver->first_name,
                    'sender' => $this->sender->name,
                    'title' => $title,
                    'start_at' => $this->leave->start_at,
                    'end_at' => $this->leave->end_at,
                    'days' => $this->leave->days,
                    'type' => $this->leave->type,
                    'is_emergency' => $this->leave->is_emergency,
                    'reason' => $this->leave->reason
                ]
            );
    }
}
