<?php


namespace App\Modules\Leaves\Requests;


use App\Modules\Leaves\Database\Models\Employee;
use App\Modules\Leaves\Database\Models\Setting;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class LeaveCreateEmergencyRequest extends LeaveCreateRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $type = Setting::where('key', SettingKey::LEAVES_TYPE)->first();
        $type = array_merge([LeaveType::UNPAID_LEAVE], $type?->value ?? []);
        $afterDate = Carbon::today()->subWeeks(2)->toDateTimeString();

        return [
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'start_at' => 'required|date_format:Y-m-d H:i:s|after_or_equal:' . $afterDate,
            'end_at' => 'required|date_format:Y-m-d H:i:s|after:start_at',
            'type' => 'required|in:' . implode(',', $type),
            'employee_id' => [
                'required',
                'integer',
                Rule::exists('vw_employees', 'id')
                    ->where('status', Employee::STATUS_ACTIVE)
                    ->whereNull('deleted_at')
            ]
        ];
    }
}
