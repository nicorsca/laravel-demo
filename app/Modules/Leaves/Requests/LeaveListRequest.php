<?php

namespace App\Modules\Leaves\Requests;

use App\System\Leave\Foundation\LeaveStatus;
use NicoSystem\Foundation\Requests\NicoListRequest;

class LeaveListRequest extends NicoListRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules += [
            'start_at' => 'nullable|date_format:Y-m-d H:i:s|required_with:end_at',
            'end_at' => 'nullable|required_with:start_at|date_format:Y-m-d H:i:s|after_or_equal:start_at',
            'employee_id' => 'nullable|integer',
            'requested_to' => 'nullable|integer',
            'is_emergency' => 'nullable|boolean',
            'status' => 'nullable|in:' . implode(',', LeaveStatus::options()),
        ];

        return $rules;
    }

}
