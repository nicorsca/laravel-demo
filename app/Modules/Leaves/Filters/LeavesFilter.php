<?php

namespace App\Modules\Leaves\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class LeavesController
 *
 * @package App\Modules\Leaves\Filters
 */
class LeavesFilter extends BaseFilter
{
    /**
     * To do filter here
     *
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

    /**
     * @param string $startAt
     */
    public function start_at(string $startAt = ''): void
    {
        if ($startAt != '' || $startAt != null) {
            $this->builder->where('start_at', '>=', $startAt);
        }
    }

    /**
     * @param string $endAt
     */
    public function end_at(string $endAt = ''): void
    {
        if ($endAt != '' || $endAt != null) {
            $this->builder->where('end_at', '<=', $endAt);
        }
    }

    /**
     * @param string $employeeId
     */
    public function employee_id(string $employeeId = ''): void
    {
        if ($employeeId != '' || $employeeId != null) {
            $this->builder->where('employee_id', $employeeId);
        }
    }

    /**
     * @param string $requestedTo
     */
    public function requested_to(string $requestedTo = ''): void
    {
        if ($requestedTo != '' || $requestedTo != null) {
            $this->builder->where('requested_to', $requestedTo);
        }
    }

    /**
     * @param string $isEmergency
     */
    public function is_emergency(string $isEmergency = ''): void
    {
        if ($isEmergency != '' || $isEmergency != null) {
            $this->builder->where('is_emergency', $isEmergency);
        }
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type !== '') {
            $this->builder->where('type', $type);
        }
    }
}
