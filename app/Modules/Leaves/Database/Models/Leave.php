<?php

namespace App\Modules\Leaves\Database\Models;

use App\System\Leave\Database\Models\Leave as BaseModel;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Leave
 *
 * @package App\Modules\Leaves\Database\Models
 */
class Leave extends BaseModel
{

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $startAt
     * @param  string                                $endAt
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereDateBetween(Builder $query, string $startAt, string $endAt): Builder
    {
        return $query->where('start_at', '>=', $startAt)
            ->where('end_at', '<=', $endAt);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $startAt
     * @param  string                                $endAt
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereDateLiesIn(Builder $query, string $startAt, string $endAt): Builder
    {
        return $query->whereBetween('start_at', [$startAt, $endAt])
            ->orWhereBetween('end_at', [$startAt, $endAt]);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $startAt
     * @param  string                                $endAt
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereExistsInBetween(Builder $query, string $startAt, string $endAt): Builder
    {
        return $query->where('start_at', '<=', $startAt)
            ->where('end_at', '>=', $endAt);
    }
}
