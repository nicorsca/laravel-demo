<?php

namespace App\Modules\ActivityLogs;

use App\Modules\ActivityLogs\Providers\EventServiceProvider;
use App\System\Attendance\Database\Models\Attendance;
use App\System\Client\Database\Models\Client;
use App\System\Common\Database\Models\Address;
use App\System\Common\Database\Models\Bank;
use App\System\Common\Database\Models\Contact;
use App\System\Common\Database\Models\Document;
use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\AllocatedLeave;
use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Database\Models\History;
use App\System\Employee\Database\Models\SocialSecurity;
use App\System\Holiday\Database\Models\Holiday;
use App\System\Leave\Database\Models\Leave;
use App\System\Project\Database\Models\Project;
use App\System\Project\Database\Models\ProjectRole;
use App\System\Setting\Database\Models\Setting;
use App\System\User\Database\Models\User;
use App\System\UserPreference\Database\Models\UserPreference;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package App\Modules\ActivityLogs
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\ActivityLogs\Interfaces\ActivityLogInterface', 'App\Modules\ActivityLogs\Repositories\ActivityLogRepository');
        $this->app->register(EventServiceProvider::class);
    }

    public function boot()
    {
        Relation::morphMap([
            'User' => User::class,
            'Department' => Department::class,
            'Employee' => Employee::class,
            'History' => History::class,
            'Address' => Address::class,
            'Contact' => Contact::class,
            'Bank' => Bank::class,
            'Document' => Document::class,
            'AllocatedLeave' => AllocatedLeave::class,
            'SocialSecurity' => SocialSecurity::class,
            'Leave' => Leave::class,
            'Setting' => Setting::class,
            'UserPreference' => UserPreference::class,
            'Attendance' => Attendance::class,
            'Project' => Project::class,
            'ProjectRole' => ProjectRole::class,
            'Client' => Client::class,
            'Holiday' => Holiday::class,
            'Provider' => Project::class,
        ]);
    }
}
