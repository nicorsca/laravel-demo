<?php

namespace App\Modules\ActivityLogs\Requests;

use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Requests\NicoListRequest;

class ActivityLogListRequest extends NicoListRequest
{
    /**
     * @return array[]
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules += [
            'user_id' => [
                'nullable',
                'integer',
                Rule::exists('users', 'id')
                    ->whereNull('deleted_at')
            ]
        ];

        return $rules;
    }

}
