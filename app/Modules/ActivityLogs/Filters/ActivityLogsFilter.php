<?php

namespace App\Modules\ActivityLogs\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class ActivityLogsController
 * @package App\Modules\ActivityLogs\Filters
 */
class ActivityLogsFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        parent::title($keyword);
    }

    /**
     * @param int|null $userId
     */
    public function user_id(int $userId = null)
    {
        if ($userId) {
            $this->builder->where('user_id', $userId);
        }
    }

}
