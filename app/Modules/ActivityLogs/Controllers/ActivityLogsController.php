<?php

namespace App\Modules\ActivityLogs\Controllers;

use App\Modules\ActivityLogs\Interfaces\ActivityLogInterface;
use App\Modules\ActivityLogs\Requests\ActivityLogListRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class ActivityLogsController
 * @package App\Modules\ActivityLogs\Controllers
 */
class ActivityLogsController extends BaseController
{
    /**
     * ActivityLogsController constructor.
     * @param ActivityLogInterface $repository
     */
    public function __construct(private ActivityLogInterface $repository)
    {
    }

    /**
     * @param \App\Modules\ActivityLogs\Requests\ActivityLogListRequest $request
     * @return JsonResponse
     */
    public function index(ActivityLogListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }
}
