<?php
use Illuminate\Support\Facades\Route;

Route::resource('activity-logs','ActivityLogsController')->only('index', 'show')->middleware('role:manager');
