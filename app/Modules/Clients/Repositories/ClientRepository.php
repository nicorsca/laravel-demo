<?php

namespace App\Modules\Clients\Repositories;

use App\Events\Client\Created;
use App\Events\Client\Updated;
use App\Modules\Clients\Events\Deleting;
use App\Modules\Clients\Filters\ClientsFilter;
use App\Modules\Clients\Interfaces\ClientInterface;
use App\Modules\Clients\Database\Models\Client;
use NicoSystem\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ClientRepository
 * @package App\Modules\Clients\Repositories
 */
class ClientRepository extends BaseRepository implements ClientInterface
{
    /**
     * @var array|string[]
     */
    protected array $events = [
        'created' => Created::class,
        'updated' => Updated::class,
        'deleting' => Deleting::class
    ];

    /**
     * ClientRepository constructor.
     * @param Client $model
     */
    public function __construct(Client $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return ClientsFilter
     */
    public function getFilter(Builder $builder): ClientsFilter
    {
        return new ClientsFilter($builder);
    }

}
