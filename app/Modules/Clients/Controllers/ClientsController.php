<?php

namespace App\Modules\Clients\Controllers;

use App\Modules\Clients\Interfaces\ClientInterface;
use App\Modules\Clients\Requests\ClientCreateRequest;
use App\Modules\Clients\Requests\ClientUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

/**
 * Class ClientsController
 * @package App\Modules\Clients\Controllers
 */
class ClientsController extends BaseController
{
    /**
     * ClientsController constructor.
     * @param ClientInterface $repository
     */
    public function __construct(private ClientInterface $repository)
    {
    }

    /**
     * @param \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @return JsonResponse
     */
    public function index(NicoListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param ClientCreateRequest $request
     * @return JsonResponse
     */
    public function store(ClientCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->all()));
    }

    /**
     * @param ClientUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(ClientUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id,$request->all()));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show (NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }
}
