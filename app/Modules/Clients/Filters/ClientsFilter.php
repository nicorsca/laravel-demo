<?php

namespace App\Modules\Clients\Filters;

use NicoSystem\Filters\BaseFilter;

/**
 * Class ClientsController
 * @package App\Modules\Clients\Filters
 */
class ClientsFilter extends BaseFilter
{
    /**
     * To do filter here
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
       $this->name($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        $this->name($title);
    }

    /**
     * @param string $name
     */
    public function name(string $name = ''): void
    {
        if ($name !== '') {
            $this->builder->where('name', 'like', "%{$name}%");
        }
    }

    /**
     * @param string $status
     */
    public function status(string $status = ''): void
    {

    }
}
