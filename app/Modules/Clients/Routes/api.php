<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['authApi', 'role:manager']], function () {
    Route::apiResource('clients','ClientsController');
});

