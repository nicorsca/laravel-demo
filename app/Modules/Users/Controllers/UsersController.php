<?php

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Interfaces\UserInterface;
use App\Modules\Users\Requests\UserCreateRequest;
use App\Modules\Users\Requests\UserUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

/**
 * Class UsersController
 *
 * @package App\Modules\Users\Controllers
 */
class UsersController extends BaseController
{
    /**
     * UsersController constructor.
     *
     * @param UserInterface $repository
     */
    public function __construct(UserInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  NicoRequest $request
     * @return JsonResponse
     */
    public function index(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param  UserCreateRequest $request
     * @return JsonResponse
     */
    public function store(UserCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->all()));
    }

    /**
     * @param  UserUpdateRequest $request
     * @param  int               $id
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request, int $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->all()));
    }

    /**
     * @param  NicoRequest $request
     * @param  int         $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, int $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param  NicoRequest $request
     * @param  int         $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, int $id): JsonResponse
    {
        return $this->responseOk($this->repository->getById($id));
    }

}
