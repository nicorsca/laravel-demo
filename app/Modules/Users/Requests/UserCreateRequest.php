<?php
namespace App\Modules\Users\Requests;

use NicoSystem\Requests\NicoRequest;

/**
 * Class UserCreateRequest
 *
 * @package App\Modules\Users\Requests
 */
class UserCreateRequest extends NicoRequest
{

}
