<?php

namespace App\Listeners;

use App\Events\Employee\History\Deactivated as EmployeeDeactivated;
use App\Events\User\UserSocialiteLogin;
use App\System\User\Database\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthEventSubscriber
{
    /**
     * @param UserSocialiteLogin $event
     * @return mixed
     */
    public function onSocialiteLogin(UserSocialiteLogin $event): User
    {
        $authUser = $event->authUser;
        $user = User::where('email', $authUser->getEmail())->where('status', User::STATUS_ACTIVE)->firstOrFail();

        if (!$user->email_verified_at) {
            $user->update([
                'password' => Hash::make(Str::random(16)),
                'email_verified_at' => now(),
                'is_locked' => User::STATUS_UNLOCKED,
            ]);
        }

        $user->providers()->updateOrCreate(
            [
                'provider' => $event->provider,
                'provider_id' => $authUser->getId(),
            ],
            [
                'avatar' => is_string($authUser->getAvatar()) ? $authUser->getAvatar() : null,
                'extra' => $authUser->getRaw(),
            ]
        );

        return $user;
    }

    /**
     * @param \App\Events\Employee\History\Deactivated $event
     */
    public function onEmployeeDeactivated(EmployeeDeactivated $event): void
    {
        foreach ($event->employee->user->tokens as $token) {
            $token->revoke();
        }
    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(UserSocialiteLogin::class, [AuthEventSubscriber::class, 'onSocialiteLogin']);
        $events->listen(EmployeeDeactivated::class, [AuthEventSubscriber::class, 'onEmployeeDeactivated']);
    }
}
