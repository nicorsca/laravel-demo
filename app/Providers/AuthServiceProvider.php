<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        if (! $this->app->routesAreCached()) {
            Passport::routes();
        }

        Passport::tokensExpireIn(now()->addSeconds(config('auth.passport_access_token_expire_in_seconds')));
        Passport::refreshTokensExpireIn(now()->addSeconds(config('auth.passport_refresh_token_expire_in_seconds')));
        Passport::personalAccessTokensExpireIn(now()->addSeconds(config('auth.passport_access_token_expire_in_seconds')));
    }
}
