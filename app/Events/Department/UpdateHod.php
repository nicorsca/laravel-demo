<?php


namespace App\Events\Department;


use App\System\Department\Database\Models\Department;

class UpdateHod
{
    /**
     * Deleting constructor.
     * @param Department $department
     * @param int|null $oldHodId
     * @param int|null $newHodId
     */
    public function __construct(public Department $department, public int|null $oldHodId, public int|null $newHodId)
    {
    }
}
