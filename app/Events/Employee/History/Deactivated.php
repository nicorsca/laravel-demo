<?php


namespace App\Events\Employee\History;


use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Database\Models\History;

class Deactivated
{

    /**
     * Deactivated constructor.
     * @param \App\System\Employee\Database\Models\Employee $employee
     * @param \App\System\Employee\Database\Models\History $history
     */
    public function __construct(
        public Employee $employee,
        public History $history
    )
    {
    }

}
