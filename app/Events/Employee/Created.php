<?php


namespace App\Events\Employee;


use App\System\Employee\Database\Models\Employee;
use App\System\Setting\Database\Models\Setting;

class Created
{
    /**
     * Created constructor.
     * @param \App\System\Employee\Database\Models\Employee $employee
     * @param \App\System\Setting\Database\Models\Setting $setting
     */
    public function __construct(public Employee $employee, public Setting $setting)
    {
    }
}
