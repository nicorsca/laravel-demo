<?php

namespace App\Events\Leave;

use App\System\Leave\Database\Models\Leave;

class ApproveLeaveRequest
{
    /**
     * @param Leave $leave
     */
    public function __construct(public Leave $leave)
    {
    }
}
