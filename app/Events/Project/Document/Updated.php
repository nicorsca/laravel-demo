<?php

namespace App\Events\Project\Document;

use App\System\Common\Database\Models\Document;

class Updated
{
    /**
     * @param \App\System\Common\Database\Models\Document $document
     */
    public function __construct(public Document $document)
    {
    }
}
