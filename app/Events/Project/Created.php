<?php

namespace App\Events\Project;

use App\System\Project\Database\Models\Project;

class Created
{

    /**
     * @param \App\System\Project\Database\Models\Project $project
     */
    public function __construct(public Project $project)
    {
    }
}
