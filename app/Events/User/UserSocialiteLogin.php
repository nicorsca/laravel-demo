<?php

namespace App\Events\User;

use Laravel\Socialite\Two\User as SocialiteUser;

class UserSocialiteLogin
{
    /**
     * Create a new event instance.
     *
     * @param String $provider
     * @param SocialiteUser $authUser
     */
    public function __construct(
        public string $provider,
        public SocialiteUser $authUser
    )
    {
    }

}
