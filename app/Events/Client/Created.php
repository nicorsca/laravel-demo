<?php

namespace App\Events\Client;

use App\System\Client\Database\Models\Client;

class Created
{

    /**
     * @param \App\System\Client\Database\Models\Client $client
     */
    public function __construct(public Client $client)
    {
    }

}
