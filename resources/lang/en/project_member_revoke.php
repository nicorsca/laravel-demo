<?php
return [
    "subject" => config("app.name") . ": Access revoked",
    "title" => "Project access revoked",
    "body" => "<p>Your access to the :title project is revoked</p>",
];
