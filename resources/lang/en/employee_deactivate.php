<?php


return [
    "subject" => config("app.name") . ": Access revoked",
    "title" => "Access revoked",
    "body" => "<p>Your email :username has been deactivated from ENSUE HR.</p>
                <p><b>Reason: </b> :comment</p>
                <p>Thank you for being part of ENSUE Nepal. </p>",
];
