<?php

return [
    "subject" => config("app.name") . ": HOD access granted",
    "title" => "Access granted",
    "body" => "<p>You have been assigned as Head of Department of :department Department.</p>
                <p>Welcome to department as our new Head of Department.</p>",
];
