<?php

return [
    'users' => [
        'login' => ':user login',
        'logout' => ':user logout',
        'sso' => [
            'google' => ':user google login',
            'microsoft' => ':user microsoft login'
        ],
        'created' => ':user created user',
        'updated' => ':user updated user',

    ],
    'departments' => [
        'created' => ':user created department',
        'updated' => ':user updated department',
        'deleted' => ':user deleted department',
    ],
    'employees' => [
        'created' => ':user created employee',
        'updated' => ':user updated employee',
    ],
    'histories' => [
        'created' => ':user created history',
    ],
    'socialsecurities' => [
        'created' => ':user created social securities',
        'updated' => ':user updated social securities',
        'deleted' => ':user deleted social securities',
    ],
    'addresses' => [
        'created' => ':user created address',
        'updated' => ':user updated address',
        'deleted' => ':user deleted address',
    ],
    'contacts' => [
        'created' => ':user created contact',
        'updated' => ':user updated contact',
        'deleted' => ':user deleted contact',
    ],
    'banks' => [
        'created' => ':user created bank',
        'updated' => ':user updated bank',
        'deleted' => ':user deleted bank',
    ],
    'documents' => [
        'created' => ':user created document',
        'updated' => ':user updated document',
        'deleted' => ':user deleted document',
    ],
    'allocatedleaves' => [
        'created' => ':user created allocated leave',
        'updated' => ':user updated allocated leave',
        'deleted' => ':user deleted allocated leave',
    ],
    'leaves' => [
        'created' => ':user created leave',
        'updated' => ':user updated leave',
        'deleted' => ':user deleted leave',
    ],
    'settings' => [
        'created' => ':user created settings',
        'updated' => ':user updated settings',
    ],
];
