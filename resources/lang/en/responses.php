<?php

return [
    'department' => [
        'employee_already_hod' => 'Employee cannot be assigned HOD in multiple departments',
        'hod_not_exists' => 'Please assign HOD to your department first.',
        'employees_exists' => 'Failed to perform operation, employees exists in this department',
    ],
    'employee' => [
        'diff_email_and_personal_email' => 'Email and personal email should not be same.',
        'history' => [
            'created_comment' => 'Employee contract is activated.',
            'deactivated' => 'Employee is already deactivated.',
            'activated' => 'Employee is already activated.',
            'cannot_deactivate' => 'Employee cannot deactivate self.',
        ],
        'bank' => [
            'registered' => 'Employee bank is already registered.',
        ],
        'address' => [
            'already_exists' => 'Address of this type already exists for this employee..',
        ],
        'allocated_leave' => [
            'not_updated' => 'To perform action: allocated should be of next fiscal year or current fiscal year allocated leave having status unpublished.',
        ],
    ],
    'user' => [
        'deactivated' => 'User is deactivated. Please contact administration to have access to the system.',
        'is_locked' => 'User should update password inorder to proceed.',
    ],
    'leave' => [
        'requested_already' => 'Leave is requested already in the requested date range.',
        'paid_leave_exceed' => 'Allowed leave days is less than zero.',
        'empty_allocated_leave' => 'Allocated leaves is not setup for requested fiscal year.',
        'cannot_request_self' => 'Employee cannot request leave application to self.',
        'status_not_pending' => 'Leave status should be pending.',
        'cannot_change_status' => 'Employee is not authorized to change other leave request status.',
        'leave_expired' => 'Emergency leave is false and leave start date has passed current date.',
    ],
    'attendance' => [
        'on_leave' => 'Employee is on leave today',
        'invalid_type' => 'Attendance type should not be :type',
        'already_checked_in' => 'Employee has already checked in today.',
        'employee_update_disable' => 'Employee is restricted to update attendance.',
        'intersect' => 'Attendance attend_at should be between :previous and :next.',
        'invalid_employee' => 'Employee id does not match with the attendance employee id',
        'update_time_exceed' => 'Time period has exceeded for employee to update the attendance.',
        'same_time_attend_at' => 'Requested attend_at and existing attendance attend_at is same.',
        'different_date' => 'Original attendance date differs from requested attended date.',
        'max_updated' => 'Attendance is updated more than 2 times.',
    ],
    'holiday' => [
        'passed' => 'Past holiday cannot be modified.',
    ],
    'client' => [
        'has_project' => 'Client has projects.',
    ],
    'project' => [
        'archived' => 'Project is archived.',
    ]
];
