<?php
/**
 * Created by PhpStorm.
 * User: Rocco
 * Date: 11/20/2018
 * Time: 9:58 AM
 */


return [
    "subject" => config("app.name") . ": Access granted",
    "title" =>"Access granted",
    "salutation" => "<p> Hey there :receiverName,</p>",
    "body" => "<p>
                    You have been granted access to <a href=':url'>:url</a>.
                    Please use the following credentials to login to the system.
                </p>
                <p>Username: :username <br> Password: :password</p>
                <p>You will be asked to change your password after you login.</p>
                <p class='text-center'><a href=':loginUrl'>Login</a> </p>
                ",
];
