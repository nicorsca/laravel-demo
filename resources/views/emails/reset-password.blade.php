@extends('emails.base_email')

@section('mail_body')
    {!! trans("reset_password_email.body",['url'=>$url]) !!}
@stop
