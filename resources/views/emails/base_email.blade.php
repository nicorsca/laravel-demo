<!DOCTYPE html>
<html>
<head>
    <title>{{trans('base_email.title',['title'=>$title])}}</title>
</head>
<body>
    {!! trans('base_email.salutation',['receiverName'=>$receiver ]) !!}

    @yield('mail_body')

    {!! trans('base_email.regards') !!}
</body>
</html>
